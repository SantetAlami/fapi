<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateViewPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('view_permissions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->string('staff', 191)->notNullable();
            $table->string('title', 191)->notNullable();
            $table->string('access', 191)->notNullable();
            $table->string('manage', 191)->nullable();
            $table->string('create', 191)->nullable();
            $table->string('read', 191)->nullable();
            $table->string('readById', 191)->nullable()->comment('can only read those created by that user');
            $table->string('update', 191)->nullable();
            $table->string('updateById', 191)->nullable()->comment('can only update those created by that user');;
            $table->string('delete', 191)->nullable();
            $table->string('deleteById', 191)->nullable()->comment('can only delete those created by that user');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_permissions');
    }
}
