<?php

/**
 * Search like array value.
 *
 * @param  Array  $request
 * @param  String  $string
 * like mysql where like sintax

 * "string%" or "%string" or "%string%"
 * @return \Illuminate\Http\Response
 */
if (!function_exists('array_search_like')) {
    function array_search_like($array, $string)
    {
        $src = str_replace('%', '', $string);
        $res = [];
        foreach ($array as $key => $value) {

            $first = $string[0];
            $last = substr($string, -1);

            $val = explode($src, $value, 3);

            if ($first === '%' && $last === '%') {
                // if ($val[1] === '') {
                array_push($res, $value);
                // }
            } elseif ($last === '%') {
                if ($val[0] === '') {
                    array_push($res, $value);
                }
            } elseif ($first === '%') {
                // dd($val);
                if ($val[count($val) - 1] === '') {
                    array_push($res, $value);
                }
            }

        }

        return $res;
    }
}

/**
 * Cuting array value.
 *
 * @param  Array  $request
 * @param  String  $string
 * like mysql where like sintax

 * "string%" or "%string" or "%string%"
 * @return \Illuminate\Http\Response
 */
if (!function_exists('array_cut_value')) {
    function array_cut_value($array, $string)
    {
        $res = [];
        foreach ($array as $key => $value) {
            $res[] = str_replace($string, "", $value);
        }

        return $res;
    }
}

if (!function_exists('in_object')) {
/**
 * Update method.
 *
 * @param  Array  $array
 * @param  String  $key
 * @param  String  $val
 * @return Bool
 */
    function in_object($array, $key, $val)
    {
        foreach ($array as $item) {
            if (isset($item->$key) && $item->$key == $val) {
                return true;
            }
        }

        return false;
    }
}
