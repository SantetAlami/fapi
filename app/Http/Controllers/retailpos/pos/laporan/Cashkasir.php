<?php

namespace App\Http\Controllers\retailpos\pos\transaksi\toko;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\retailpos\users;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

// use App\User;

use Carbon\Carbon;

class Cashkasir extends Controller
{
    public static function run($db, $data)
    {
        $cash_toko = "cash_toko";

        $store_id = users::get($db, 'store_id');
        $table = DB::table($db . '.' . $cash_toko)->where(["store_id" => $store_id]);

        $requestData = ["data" => $data];

        return FapiController::read($requestData, $table);

    }

}
