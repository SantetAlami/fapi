<?php

namespace App\Http\Controllers\retailpos\pos;

use App\Http\Controllers\Controller;
use App\Http\Controllers\UserController;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\retailpos\users;
use App\User;

// use Carbon\Carbon;

class Register extends Controller
{

    public static function run($db, $data)
    {
        try {
            $user = new User;
            $user->name = $data['name'];
            $user->username = $data['username'];
            $user->email = $data['email'];
            $plainPassword = $data['password'];
            $user->password = app('hash')->make($plainPassword);
            $user->assignRole('guest'); //public register save as common user as default role
            $user->save();

            $credentials = ["username" => $data['username'], "password" => app('hash')->make($plainPassword)];

            if ($token = Auth::attempt($credentials)) {
                $res = Auth::user();
                $res = UserController::profile(["retailpos"], true);
                $res->token = $token;
            }

            DB::table($db . '.users')->insertOrIgnore(["id" => $res->id, "is_owner" => 1]);

            //return successful response
            // return response()->json(['user' => $user, 'message' => 'User Registration'], 201);
            return FapiController::response('success', 'the user has successfully registered', $res, 201);

        } catch (\Throwable $e) {
            //return error message
            // return response()->json(['message' => 'User Registration Failed!'], 409);
            return FapiController::response('error', 'user registration failed ' . $e->getMessage(), null, 409);
        }

    }

}
