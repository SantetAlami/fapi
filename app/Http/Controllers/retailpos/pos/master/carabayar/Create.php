<?php

namespace App\Http\Controllers\retailpos\pos\master\carabayar;

use App\Http\Controllers\Controller;
// use Carbon\Carbon;
// use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\retailpos\users;
use Illuminate\Support\Facades\DB;

class Create extends Controller
{

    public static function run($db, $data)
    {
        $carabayar = "carabayar";

        $store_id = users::get($db, 'store_id');
        $table = DB::table($db . '.' . $carabayar);

        $data["store_id"] = $store_id;
        $requestData = ["data" => $data];

        return FapiController::insert($requestData, $table);

    }

}
