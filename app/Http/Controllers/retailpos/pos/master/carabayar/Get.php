<?php

namespace App\Http\Controllers\retailpos\pos\master\carabayar;

use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\retailpos\users;

// use Carbon\Carbon;

class Get extends Controller
{

    public static function run($db, $data)
    {
        $carabayar = "carabayar";

        $store_id = users::get($db, 'store_id');
        $table = DB::table($db . '.' . $carabayar)->where(["store_id" => $store_id]);

        $requestData = ["data"=> $data];

        return FapiController::read($requestData, $table);

    }

}
