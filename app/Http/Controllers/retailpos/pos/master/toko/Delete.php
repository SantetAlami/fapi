<?php

namespace App\Http\Controllers\retailpos\pos\master\toko;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\FapiController;
// use App\Http\Controllers\retailpos\users;

// use Carbon\Carbon;

class Delete extends Controller
{

    public static function run($db, $data)
    {
        $store = "store";

        $user_id = Auth::user()->id;
        $table = DB::table($db . '.' . $store)->where(["user_id" => $user_id]);

        $requestData = ["data"=> $data, "id" => $data["id"]];

        return FapiController::delete($requestData, $table);

    }

}
