<?php

namespace App\Http\Controllers\retailpos\pos\master\kategori;

use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\retailpos\users;

// use Carbon\Carbon;

class Edit extends Controller
{

    public static function run($db, $data)
    {
        $kategori = "kategori";

        $store_id = users::get($db, 'store_id');
        $table = DB::table($db . '.' . $kategori)->where(["store_id" => $store_id]);

        $requestData = ["data"=> $data, "id" => $data["id"]];

        return FapiController::update($requestData, $table);

    }

}
