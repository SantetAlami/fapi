<?php

namespace App\Http\Controllers\retailpos\pos\master\barang;

use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\retailpos\users;
use Illuminate\Support\Facades\DB;

// use Carbon\Carbon;

class Get extends Controller
{

    public static function run($db, $data)
    {
        $barang = "v_barang";

        // $store = "store";

        // $user_id = Auth::user()->id;
        $store_id = users::get($db, 'store_id');
        $table = DB::table($db . '.' . $barang)->where(["store_id" => $store_id])->orderBy('kategori')->orderBy('nama');

        $requestData = ["data" => $data];

        return FapiController::read($requestData, $table);

    }

}
