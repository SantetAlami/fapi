<?php

namespace App\Http\Controllers\retailpos\pos\master\barang;

use App\Http\Controllers\Controller;
use App\Http\Controllers\retailpos\users;
use Carbon\Carbon;
// use App\Http\Controllers\FapiController;
use Illuminate\Support\Facades\DB;

class Edit extends Controller
{

    public static function run($db, $data)
    {
        $kategori = "kategori";
        $barang = "barang";
        $stok = "stok";

        // $store = "store";

        // $user_id = Auth::user()->id;

        $store_id = users::get($db, 'store_id');

        DB::table($db . '.' . $kategori)->insertOrIgnore(["nama" => $data['kategori'], "store_id" => $store_id]);
        $kategori_id = DB::table($db . '.' . $kategori)->where(["nama" => $data['kategori'], "store_id" => $store_id])->first()->id;

        DB::table($db . '.' . $barang)->where('id', $data['id'])->update(
            [
                "barcode" => $data['barcode'], 
                "store_id" => $store_id,
                "kategori_id" => $kategori_id,
                "nama" => $data['nama'],
                "harga_beli" => $data['harga_beli'],
                "harga_jual" => $data['harga_jual'],
                "status" => $data['status'],
                "gambar" => $data['gambar'],
                "detail" => $data['detail'],
            ]
        );

        $barang_id = DB::table($db . '.' . $barang)->where(["barcode" => $data['barcode'], "store_id" => $store_id])->first()->id;

        $pFaktur = "BM";
        $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

        if (isset($data['stok'])) {
            $sum = DB::table($db . '.' . $stok)->selectRaw("(sum(masuk) - sum(keluar)) as tot")->where(["barang_id" => $barang_id])->first()->tot;
            DB::table($db . '.' . $stok)->insert(["tgl" => date("Y-m-d", Carbon::now()->timestamp), "barang_id" => $barang_id, "faktur" => $faktur, "masuk" => $data['stok'] - $sum, "keluar" => 0]);
        }

        return ['status' => 'success', 'message' => 'Data barang telah di perbaharui', 'data' => []];

    }

}
