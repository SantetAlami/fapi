<?php

namespace App\Http\Controllers\retailpos\pos\master\barang;

use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\retailpos\users;

use Carbon\Carbon;

class Delete extends Controller
{

    public static function run($db, $data)
    {
        $barang = "barang";

        // $store = "store";

        // $user_id = Auth::user()->id;
        $store_id = users::get($db, 'store_id');
        $table = DB::table($db . '.' . $barang)->where(["store_id" => $store_id]);

        $data["store_id"] = $store_id;
        $requestData = ["data"=> ['deleted_at' => date("Y-m-d", Carbon::now()->timestamp)], "id" => $data["id"]];

        return FapiController::update($requestData, $table);

    }

}
