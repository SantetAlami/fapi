<?php

namespace App\Http\Controllers\retailpos\pos\transaksi\toko;

use App\Http\Controllers\Controller;
// use App\Http\Controllers\FapiController;
use App\Http\Controllers\retailpos\users;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

// use App\User;

use Carbon\Carbon;

class Buka extends Controller
{
    public static function run($db, $data)
    {
        $cash_toko = "cash_toko";

        $user_id = Auth::user()->id;

        $store_id = users::get($db, 'store_id');
        
        DB::table($db . '.' . $cash_toko)->insert([
            "user_id" => $user_id,
            "tgl" => date("Y-m-d H-i-s", Carbon::now()->timestamp),
            "nominal" => $data['nominal'],
            "status" => 1,
            "store_id" => $store_id,
        ]);

        return ['status' => 'success', 'message' => 'Data buka toko telah tersimpan.', 'data' => []];
    }

}
