<?php

namespace App\Http\Controllers\retailpos\pos\transaksi;

use App\Http\Controllers\Controller;
use App\Http\Controllers\retailpos\users;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Set extends Controller
{

    public static function run($db, $data)
    {
        $pelanggan = "pelanggan";
        $transaksi_order = "transaksi_order";
        $detail_order = "detail_order";
        // $project_has_order = "project_has_order";
        // $stok = "stok";

        $barang = "barang";
        // $project = "project";

        $user_id = Auth::user()->id;
        $store_id = users::get($db, 'store_id');

        $pFaktur = "TO";

        // {
        //     "pelanggan_nama": "nyan",
        //     "pelanggan_no_tlpn": "0891273",
        //     "pelanggan_barcode": "999993148137",
        //     "carabayar_id": 1,
        //     "tgl": "2020-11-12",
        //     "project": ["Pasang CCTV", "Waaaaa"],
        //     "items": [
        //         {
        //             "id": 1,
        //             "jumlah":3
        //         },
        //         {
        //             "id":2,
        //             "jumlah":2
        //         }
        //     ],
        //     "catatan": "",
        // },

        $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

        $total_harga = 0;
        $jumlah_barang = 0;

        foreach ($data["items"] as $item) {
            try {
                $barangs = DB::table($db . '.' . $barang)->where(["id" => $item["id"], "store_id" => $store_id])->first();

                $total_harga = $total_harga + ($barangs->harga_order * $item["jumlah"]);
                $jumlah_barang++;
            } catch (\Throwable $th) {
                return ['status' => 'error', 'message' => 'Tidak ada data barang', 'data' => []];
            }
        }

        if (!isset($data["pelanggan_id"])) {
            DB::table($db . '.' . $pelanggan)->updateOrInsert(["nama" => $data["pelanggan_nama"], "no_telp" => $data["pelanggan_no_tlpn"], "barcode" => $data["pelanggan_barcode"], "store_id" => $store_id], []);
            $pelanggan_id = DB::table($db . '.' . $pelanggan)->where(["nama" => $data["pelanggan_nama"], "no_telp" => $data["pelanggan_no_tlpn"], "barcode" => $data["pelanggan_barcode"], "store_id" => $store_id])->first()->id;
        } else {
            $pelanggan_id = DB::table($db . '.' . $pelanggan)->where(["id" => $data["pelanggan_id"], "store_id" => $store_id])->first()->id;
        }

        // insert transaksi order
        $trx_id = DB::table($db . '.' . $transaksi_order)->insertGetId([
            "pelanggan_id" => $pelanggan_id,
            "carabayar_id" => $data["carabayar_id"],
            "tgl" => $data["tgl"],
            "faktur" => $faktur,
            "status" => 0, //lunas atau belum

            "total" => $total_harga,
            "jumlah_item" => $jumlah_barang,

            "bayar" => 0,
            "kembali" => 0,
            // "bayar" => $data["bayar"],
            // "kembali" => $data["bayar"] - $total_harga,

            "user_id" => $user_id,
            "store_id" => $store_id,
        ]);

        // insert detail trx
        foreach ($data["items"] as $item) {
            $barang_res = DB::table($db . '.' . $barang)->where(["id" => $item["id"], "store_id" => $store_id])->first();

            DB::table($db . '.' . $detail_order)->insert([
                "transaksi_id" => $trx_id,
                "barang_id" => $item["id"],
                "faktur" => $faktur,
                "harga_beli" => $barang_res->harga_beli,
                "harga_order" => $barang_res->harga_order,
                "jumlah_barang" => $item["jumlah"],
                "total" => $barang_res->harga_order * $item["jumlah"],
            ]);

            // update stok

            // DB::table($db . '.' . $stok)->insert(["tgl" => $data["tgl"], "barang_id" => $item["id"], "faktur" => $faktur, "masuk" => 0, "keluar" => $item["jumlah"]]);

            // insert project
            // foreach ($data["project"] as $item) {
            //     DB::table($db . '.' . $project)->updateOrInsert([
            //         "nama" => $item,
            //         "store_id" => $store_id,
            //     ], []);

            //     $project_id = DB::table($db . '.' . $project)->where([
            //         "nama" => $item,
            //         "store_id" => $store_id,
            //     ])->first()->id;

            //     DB::table($db . '.' . $project_has_order)->insert(["project_id" => $project_id, "penorderan_id" => $trx_id]);
            // }

        }
        return ['status' => 'success', 'message' => 'Transaksi baru telah disinkronkan', 'data' => []];
    }
}
