<?php

namespace App\Http\Controllers\retailpos\pos\transaksi;

use App\Http\Controllers\Controller;
use App\Http\Controllers\retailpos\users;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Beli extends Controller
{

    public static function run($db, $data)
    {
        $distributor = "distributor";
        $transaksi_beli = "transaksi_beli";
        $detail_beli = "detail_beli";
        // $project_has_beli = "project_has_beli";
        $stok = "stok";

        $barang = "barang";
        // $project = "project";

        $user_id = Auth::user()->id;
        $store_id = users::get($db, 'store_id');

        $pFaktur = "TB";

        // {
        //     "distributor_id": 3,
        //     "distributor_nama": "nyan",
        //     "distributor_no_tlpn": "0891273",
        //     "distributor_barcode": "999993148137",
        //     "faktur_offline": "OFSA1235676",
        //     "carabayar_id": 1,
        //     "tgl": "2020-11-12",
        //     "project": ["Pasang CCTV", "Waaaaa"],
        //     "items": [
        //         {
        //             "id": 1,
        //             "harga_beli": "3000",
        //             "jumlah":3
        //         },
        //         {
        //             "id":2,
        //             "jumlah":2
        //         }
        //     ],
        //     "catatan": "",
        //     "bayar": 20000
        // },

        $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

        $total_harga = 0;
        $jumlah_barang = 0;

        foreach ($data["items"] as $item) {
            try {
                $barangs = DB::table($db . '.' . $barang)->where(["id" => $item["id"], "store_id" => $store_id])->first();

                $total_harga = $total_harga + (($item['harga_beli'] ?? $barangs->harga_beli) * $item["jumlah"]);
                $jumlah_barang++;
            } catch (\Throwable $th) {
                return ['status' => 'error', 'message' => 'Tidak ada data barang', 'data' => []];
            }
        }

        if (!isset($data["distributor_id"])) {
            DB::table($db . '.' . $distributor)->updateOrInsert(["nama" => $data["distributor_nama"], "no_telp" => $data["distributor_no_tlpn"], "barcode" => $data["distributor_barcode"], "store_id" => $store_id], []);
            $distributor_id = DB::table($db . '.' . $distributor)->where(["nama" => $data["distributor_nama"], "no_telp" => $data["distributor_no_tlpn"], "barcode" => $data["distributor_barcode"], "store_id" => $store_id])->first()->id;
        } else {
            $distributor_id = DB::table($db . '.' . $distributor)->where(["id" => $data["distributor_id"], "store_id" => $store_id])->first()->id;
        }

        // insert transaksi beli
        $trx_id = DB::table($db . '.' . $transaksi_beli)->insertGetId([
            "distributor_id" => $distributor_id,
            "carabayar_id" => $data["carabayar_id"],
            "tgl" => $data["tgl"],
            "faktur" => $faktur,
            "status" => (($data["bayar"] - $total_harga) >= 0) ? 1 : 0, //lunas atau belum

            "total" => $total_harga,
            "jumlah_item" => $jumlah_barang,

            "bayar" => $data["bayar"],
            "kembali" => $data["bayar"] - $total_harga,

            "user_id" => $user_id,
            "store_id" => $store_id,
        ]);

        // insert detail trx
        foreach ($data["items"] as $item) {
            $barang_res = DB::table($db . '.' . $barang)->where(["id" => $item["id"], "store_id" => $store_id])->first();

            DB::table($db . '.' . $detail_beli)->insert([
                "transaksi_id" => $trx_id,
                "barang_id" => $item["id"],
                "faktur" => $faktur,
                "harga_beli" => $item['harga_beli'] ?? $barang_res->harga_beli,
                "harga_jual" => $barang_res->harga_jual,
                "jumlah_barang" => $item["jumlah"],
                "total" => $barang_res->harga_beli * $item["jumlah"],
            ]);

            if (isset($item['harga_beli'])) {
                DB::table($db . '.' . $barang)->where(["id" => $item["id"], "store_id" => $store_id])->update(["harga_beli" => $item['harga_beli']]);
            }

            // update stok

            DB::table($db . '.' . $stok)->insert(["tgl" => $data["tgl"], "barang_id" => $item["id"], "faktur" => $faktur, "masuk" => $item["jumlah"], "keluar" => 0]);
        }

        // insert project
        // foreach ($data["project"] as $item) {
        //     DB::table($db . '.' . $project)->updateOrInsert([
        //         "nama" => $item,
        //         "store_id" => $store_id,
        //     ], []);

        //     $project_id = DB::table($db . '.' . $project)->where([
        //         "nama" => $item,
        //         "store_id" => $store_id,
        //     ])->first()->id;

        //     DB::table($db . '.' . $project_has_beli)->insert(["project_id" => $project_id, "penbelian_id" => $trx_id]);
        // }
        return ['status' => 'success', 'message' => 'Transaksi baru telah di tambahkan', 'data' => []];
    }
}
