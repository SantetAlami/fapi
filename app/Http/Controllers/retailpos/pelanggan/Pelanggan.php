<?php

namespace App\Http\Controllers\retailpos\pelanggan;

use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\retailpos\users;

// use Carbon\Carbon;

class Pelanggan extends Controller
{

    public static function run($db, $data)
    {
        $pelanggan = "pelanggan";

        // $store = "store";

        // $user_id = Auth::user()->id;
        $store_id = users::get($db, 'store_id');
        // try {
        //     $store_id = DB::table($db . '.' . $store)->where(["id" => $data['store_id'], "user_id" => $user_id])->first()->id;
        // } catch (\Throwable $th) {
        //     return ['status' => 'error', 'message' => 'Tidak ada data toko', 'data' => []];
        // }

        $res = DB::table($db . "." . $pelanggan)->where(["store_id" => $store_id])->where("nama", "LIKE", "%" . ($data["nama"] ?? "") . "%")->where("barcode", "LIKE", "%" . ($data["barcode"] ?? "") . "%")->get();

        return ['status' => 'success', 'message' => 'Get data pelanggan', 'data' => $res];

    }

}
