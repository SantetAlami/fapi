<?php

namespace App\Http\Controllers\retailpos\laporan;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Jual extends Controller
{

    public static function run($db, $data)
    {
        $res = DB::select('CALL ' . $db . '.' . htmlentities($data['gate']) . "('" . htmlentities(implode("','", $data['data'])) . "')");

        $sum = 0;
        foreach ($res as $key => $value) {
            if (isset($value->bayar)) {
                $sum += $value->bayar - ($value->kembali < 0 ? 0 : $value->kembali);
            }

        }

        foreach ($res as $key => $item) {
            $res[$key]->key = Str::uuid();
            $res[$key]->children = DB::select('CALL ' . $db . '.sp_detail_jual' . "('" . htmlentities($item->id) . "')");
            $res[$key]->grand_total = $sum;

            foreach ($res[$key]->children as $i => $value) {
                $res[$key]->children[$i]->key = Str::uuid();
            }
        }

        return ['status' => 'success', 'message' => 'Get all data', 'data' => $res];

    }

}
