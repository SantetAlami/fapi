<?php

namespace App\Http\Controllers\retailpos\toko;

use App\Http\Controllers\Controller;
use App\Http\Controllers\retailpos\users;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

// use Carbon\Carbon;

class ReadToko extends Controller
{

    public static function run($db, $data)
    {
        $store = "store";
        // $store_has_user = "store_has_user";

        // $user_id = Auth::user()->id;
        $store_id = users::get($db, "store_id");
        $res = DB::table($db . '.' . $store)->where(["id" => $store_id])->first();

        return ['status' => 'success', 'message' => 'Get data toko', 'data' => $res];

    }

}
