<?php

namespace App\Http\Controllers\retailpos\toko;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Toko extends Controller
{

    public static function run($db, $data)
    {
        $store = "store";
        $store_has_user = "store_has_user";

        $user_id = Auth::user()->id;
        $store_id = DB::table($db . '.' . $store)->updateOrInsert(
            ["id" => $data['id'] ?? null, "user_id" => $user_id],
            [
                "name" => $data['name'],
                "address" => $data['address'],
                "villages_id" => $data['villages_id'],
                "email" => $data['email'],
            ]);

        $store_id = DB::table($db . '.' . $store)->where([
            "name" => $data['name'],
            "address" => $data['address'],
            "villages_id" => $data['villages_id'],
            "email" => $data['email'],
            "user_id" => $user_id,
        ])->first()->id;
        
        DB::table($db . '.' . $store_has_user)->insertOrIgnore(["store_id" => $store_id, "user_id" => $user_id]);

        return ['status' => 'success', 'message' => 'Toko baru telah di tambahkan', 'data' => []];

    }

}
