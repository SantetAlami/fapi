<?php

namespace App\Http\Controllers\retailpos\jurnal\laporan;

use App\Http\Controllers\Controller;
use App\Http\Controllers\retailpos\users;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Jurnalumum extends Controller
{

    public static function run($db, $data)
    {
        $sp_jurnal_umum = "sp_jurnal_umum";

        $i_mulai = $data["mulai"];
        $i_hingga = $data["hingga"];

        // $user_id = Auth::user()->id;
        $store_id = users::get($db, 'store_id');

        $res = DB::select('CALL ' . $db . '.' . $sp_jurnal_umum . '(' . $store_id . ',\'' . htmlentities($i_mulai) . '\', \'' . htmlentities($i_hingga) . '\');');

        return ['status' => 'success', 'message' => 'Get Jurnal Umum', 'data' => $res];
    }

}
