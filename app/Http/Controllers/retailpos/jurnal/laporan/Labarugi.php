<?php

namespace App\Http\Controllers\retailpos\jurnal\laporan;

use App\Http\Controllers\Controller;
use App\Http\Controllers\retailpos\users;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Labarugi extends Controller
{

    public static function run($db, $data)
    {
        function buildTree(array $flatList)
        {
            $grouped = [];
            foreach ($flatList as $node) {
                $grouped[$node['induk']][] = $node;
                if ($node['jenis'] == 'induk' && ($node['dari'] != 0 || $node['sampai'] != 0)) {
                    $grouped[$node['kode']][] = [
                        "kode" => "",
                        "nama" => "Lain2",
                        "dari" => $node['dari'],
                        "debit" => $node['debit'],
                        "kredit" => $node['kredit'],
                        "sampai" => $node['sampai'],
                        "jenis" => "detail",
                        "induk" => $node['kode'],
                    ];
                }
            }
            // dd($grouped);

            $fnBuilder = function ($siblings) use (&$fnBuilder, $grouped) {
                foreach ($siblings as $k => $sibling) {
                    if ($sibling['jenis'] == "induk") {
                        $sibling['dari'] = 0;
                        $sibling['debit'] = 0;
                        $sibling['kredit'] = 0;
                        $sibling['sampai'] = 0;
                    }
                    $id = $sibling['kode'];
                    if (isset($grouped[$id])) {
                        $sibling['children'] = $fnBuilder($grouped[$id]);
                    }
                    $siblings[$k] = $sibling;
                }
                if ($siblings[0]['kode'] == "") {
                    $last = $siblings[count($siblings) - 1];
                    $siblings[count($siblings) - 1] = $siblings[0];
                    $siblings[0] = $last;
                }
                return $siblings;
            };

            $tree = [];
            for ($i = 1; $i <= 5; $i++) {
                if (isset($grouped[$i])) {
                    $tree[$i] = [];
                    foreach ($fnBuilder($grouped[$i]) as $key => $item) {
                        $tree[$i][] = $item;
                    }
                }
            }

            return $tree;
        }

        function array_move($arr, $old_index, $new_index)
        {
            if ($new_index >= count($arr)) {
                $k = $new_index - count($arr) + 1;
                while ($k--) {
                    $arr[] = null;
                }
            }
            array_splice($arr, $new_index, 0, array_splice($arr, $old_index, 1)[0]);
            return $arr; // for testing
        };

        $sp_jurnal_labarugi = "sp_jurnal_labarugi";

        $i_mulai = $data["mulai"];
        $i_hingga = $data["hingga"];

        // $user_id = Auth::user()->id;
        $store_id = users::get($db, 'store_id');

        // $res = DB::table($db . '.' . $rekening)->selectRaw("kode,nama,0 as dari, 0 as debit, 0 as kredit, 0 as sampai")->where(["store_id" => $store_id, "jenis" => "induk"])->whereRaw("LENGTH(kode) = 5 and SUBSTRING(kode,1,1) IN ('1','5')")->get();
        $bb = DB::select('CALL ' . $db . '.' . $sp_jurnal_labarugi . '(' . $store_id . ',\'' . htmlentities($i_mulai) . '\', \'' . htmlentities($i_hingga) . '\');');

        $new = [];
        foreach ($bb as $key => $value) {
            foreach ($value as $colkey => $col) {
                $new[$key][$colkey] = $col;
            }
        }
        // dd($new);
        $tree = buildTree($new, 'induk', 'kode');

        return ['status' => 'success', 'message' => 'Get Laba Rugi', 'data' => $tree];
    }
}
