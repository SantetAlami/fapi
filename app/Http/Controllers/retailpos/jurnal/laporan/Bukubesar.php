<?php

namespace App\Http\Controllers\retailpos\jurnal\laporan;

use App\Http\Controllers\Controller;
use App\Http\Controllers\retailpos\users;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BukuBesar extends Controller
{

    public static function run($db, $data)
    {
        $rekening = "rekening";
        $sp_jurnal_bukubesar = "sp_jurnal_bukubesar";

        $i_dari = $data["dari"];
        $i_sampai = $data["sampai"];
        $i_mulai = $data["mulai"];
        $i_hingga = $data["hingga"];

        // $user_id = Auth::user()->id;
        $store_id = users::get($db, 'store_id');

        $rekenings = DB::table($db . "." . $rekening)->where(["store_id" => $store_id])->whereBetween("kode", [$i_dari, $i_sampai])->get();
        $res = [];
        foreach ($rekenings as $key => $item) {
            $jurnal = DB::select('CALL ' . $db . '.' . $sp_jurnal_bukubesar . '(' . $item->id . ',' . $store_id . ',\'' . htmlentities($i_mulai) . '\', \'' . htmlentities($i_hingga) . '\');');
            if (count($jurnal) > 0) {
                array_push($res, [
                    "kode" => $item->kode,
                    "nama" => $item->nama,
                    "children" => $jurnal
                ]);
            }
        }

        return ['status' => 'success', 'message' => 'Get Buku Besar', 'data' => $res];
    }

}
