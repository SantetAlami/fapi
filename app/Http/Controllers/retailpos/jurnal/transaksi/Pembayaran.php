<?php

namespace App\Http\Controllers\retailpos\jurnal\transaksi;

use App\Http\Controllers\Controller;
use App\Http\Controllers\retailpos\users;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Pembayaran extends Controller
{

    public static function run($db, $data)
    {
        $produk = "produk";
        $produk_has_jurnal = "produk_has_jurnal";
        $bukubesar = "bukubesar";
        $rekening = "rekening";

        $i_produk = $data["produk_id"];
        $i_nominal = $data["nominal"];
        $i_tgl = $data["tgl"];

        $pFaktur = "JP";
        $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

        $user_id = Auth::user()->id;
        $store_id = users::get($db, 'store_id');

        $reqproduk = DB::table($db . "." . $produk)->where(["id" => $i_produk])->first();
        $reqrekening = DB::table($db . "." . $produk_has_jurnal)->where(["produk_id" => $reqproduk->id])->get();
        $save = [];
        foreach ($reqrekening as $key => $item) {
            if (DB::table($db . "." . $rekening)->where(["id" => $item->rekening_id, "store_id" => $store_id])->exists()) {
                $save[$key] = [];
                $save[$key]['tgl'] = $i_tgl;
                $save[$key]['rekening_id'] = $item->rekening_id;
                $save[$key]['faktur'] = $faktur;
                $save[$key]['keterangan'] = "Transaksi Pembayaran " . $reqproduk->nama;
                $save[$key]['debit'] = ($item->masuk == "debit") ? $i_nominal : 0;
                $save[$key]['kredit'] = ($item->masuk == "kredit") ? $i_nominal : 0;
                $save[$key]['store_id'] = $store_id;
                $save[$key]['user_id'] = $user_id;
            } else {
                return ['status' => 'error', 'message' => 'Tidak ada data jurnal', 'data' => []];
            }
        }
        // dd($reqrekening);

        DB::table($db . "." . $bukubesar)->insert($save);

        return ['status' => 'success', 'message' => 'Transaksi berhasil', 'data' => []];
    }

}
