<?php

namespace App\Http\Controllers\retailpos\jurnal\transaksi;

use App\Http\Controllers\Controller;
use App\Http\Controllers\retailpos\users;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

class Jurnallainlain extends Controller
{

    public static function run($db, $data)
    {
        $bukubesar = "bukubesar";
        $rekening = "rekening";

        $i_rekening = $data["rekening"];
        $i_tgl = $data["tgl"];

        $pFaktur = "JL";
        $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

        $user_id = Auth::user()->id;
        $store_id = users::get($db, 'store_id');

        foreach ($i_rekening as $key => $item) {
            if (DB::table($db . "." . $rekening)->where(["id" => $item["id"], "store_id" => $store_id])->exists()) {
                $i_rekening[$key]["tgl"] = $i_tgl;
                $i_rekening[$key]["rekening_id"] = $item["id"];
                $i_rekening[$key]["faktur"] = $faktur;
                $i_rekening[$key]["user_id"] = $user_id;
                $i_rekening[$key]["store_id"] = $store_id;

                unset($i_rekening[$key]["id"]);
            } else {
                return ['status' => 'error', 'message' => 'Tidak ada data jurnal', 'data' => []];
            }
        }

        DB::table($db . "." . $bukubesar)->insert($i_rekening);

        return ['status' => 'success', 'message' => 'Transaksi berhasil', 'data' => []];
    }

}
