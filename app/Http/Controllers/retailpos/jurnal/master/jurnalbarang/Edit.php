<?php

namespace App\Http\Controllers\retailpos\jurnal\master\jurnalbarang;

use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\retailpos\users;
use Illuminate\Support\Facades\DB;

// use Carbon\Carbon;

class Edit extends Controller
{

    public static function run($db, $data)
    {
        $rekening = "rekening";
        $barang = "barang";
        $barang_has_jurnal = "barang_has_jurnal";

        $store_id = users::get($db, 'store_id');

        $i_jurnal = $data["jurnal"];
        $i_barang = $data["barang_id"];

        foreach ($i_barang as $item) {
            if (DB::table($db . '.' . $barang)->where(["store_id" => $store_id, "id" => $item])->exists()) {
                $arr_jurnal = [];

                foreach ($i_jurnal as $value) {
                    if (DB::table($db . '.' . $rekening)->where(["store_id" => $store_id, "id" => $value["id"], "jenis" => "detail"])->exists()) {
                        array_push($arr_jurnal, [
                            "barang_id" => $item,
                            "rekening_id" => $value["id"],
                            "masuk" => $value["masuk"],
                        ]);
                    } else {
                        return ['status' => 'error', 'message' => 'tidak ada data rekening / data rekening bukan rekening detail', 'data' => []];
                    }
                }
                DB::table($db . "." . $barang_has_jurnal)->where(["barang_id" => $item])->delete();
                DB::table($db . "." . $barang_has_jurnal)->insert($arr_jurnal);
            } else {
                return ['status' => 'error', 'message' => 'tidak ada data barang', 'data' => $item];
            }
        }

        return ['status' => 'success', 'message' => 'jurnal barang telah di perbaharui', 'data' => []];

    }

}
