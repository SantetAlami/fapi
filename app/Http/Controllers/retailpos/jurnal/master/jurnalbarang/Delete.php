<?php

namespace App\Http\Controllers\retailpos\jurnal\master\jurnalbarang;

use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\retailpos\users;
use Illuminate\Support\Facades\DB;

// use Carbon\Carbon;

class Delete extends Controller
{

    public static function run($db, $data)
    {
        $rekening = "rekening";
        $barang = "barang";
        $barang_has_jurnal = "barang_has_jurnal";

        $store_id = users::get($db, 'store_id');

        $i_barang = $data["barang_id"];

        foreach ($i_barang as $item) {
            if (DB::table($db . '.' . $barang)->where(["store_id" => $store_id, "id" => $item])->exists()) {

                DB::table($db . "." . $barang_has_jurnal)->where(["barang_id" => $item])->delete();

            } else {
                return ['status' => 'error', 'message' => 'tidak ada data barang', 'data' => $item];
            }
        }

        return ['status' => 'success', 'message' => 'jurnal barang telah di hapus', 'data' => []];

    }

}
