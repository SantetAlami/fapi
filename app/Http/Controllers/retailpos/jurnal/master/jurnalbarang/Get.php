<?php

namespace App\Http\Controllers\retailpos\jurnal\master\jurnalbarang;

use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\retailpos\users;
use Illuminate\Support\Facades\DB;

// use Carbon\Carbon;

class Get extends Controller
{

    public static function run($db, $data)
    {
        $barang = "barang";

        $barang_id = $data["barang_id"];

        $store_id = users::get($db, 'store_id');
        if (DB::table($db . '.' . $barang)->where(["id" => $barang_id, "store_id" => $store_id])->exists()) {
            $requestData = ["gate" => "sp_jurnal_barang", "data" => [$barang_id]];

            return FapiController::sp($db, $requestData);
        }

        return ['status' => 'error', 'message' => 'Tidak ada data barang', 'data' => []];
    }

}
