<?php

namespace App\Http\Controllers\retailpos\jurnal\master\rekening;

use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\retailpos\users;

// use Carbon\Carbon;

class Get extends Controller
{

    public static function run($db, $data)
    {
        $rekening = "rekening";

        // $store = "store";

        // $user_id = Auth::user()->id;
        $store_id = users::get($db, 'store_id');
        $table = DB::table($db . '.' . $rekening)->where(["store_id" => $store_id]);

        $requestData = ["data"=> $data];

        return FapiController::read($requestData, $table);

    }

}
