<?php

namespace App\Http\Controllers\retailpos\jurnal\master\produk;

use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\retailpos\users;
use Illuminate\Support\Facades\DB;

// use Carbon\Carbon;

class Get extends Controller
{

    public static function run($db, $data)
    {
        $produk = "produk";

        $store_id = users::get($db, 'store_id');
        // if (DB::table($db . '.' . $produk)->where(["id" => $data["id"], "store_id" => $store_id])->exists()) {
        $requestData = ["data" => $data];

        // dd($requestData);
        $table = DB::table($db . '.produk')->where(["store_id" => $store_id]);
        return FapiController::read($requestData, $table);
        // }

        // return ['status' => 'error', 'message' => 'Tidak ada data produk', 'data' => []];
    }

}
