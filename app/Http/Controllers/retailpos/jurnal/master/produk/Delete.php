<?php

namespace App\Http\Controllers\retailpos\jurnal\master\produk;

use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\retailpos\users;
use Illuminate\Support\Facades\DB;

// use Carbon\Carbon;

class Delete extends Controller
{

    public static function run($db, $data)
    {
        $produk = "produk";
        $produk_has_jurnal = "produk_has_jurnal";

        $store_id = users::get($db, 'store_id');

        $i_id = $data["id"];

        if (DB::table($db . '.' . $produk)->where(["store_id" => $store_id, "id" => $i_id])->exists()) {

            DB::table($db . "." . $produk)->where(["store_id" => $store_id, "id" => $i_id])->delete();
            DB::table($db . "." . $produk_has_jurnal)->where(["produk_id" => $i_id])->delete();

        } else {
            return ['status' => 'error', 'message' => 'tidak ada data produk', 'data' => $i_id];
        }

        return ['status' => 'success', 'message' => 'jurnal produk telah di hapus', 'data' => []];

    }

}
