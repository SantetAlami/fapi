<?php

namespace App\Http\Controllers\retailpos\jurnal\master\produk;

use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\retailpos\users;
use Illuminate\Support\Facades\DB;

// use Carbon\Carbon;

class Edit extends Controller
{

    public static function run($db, $data)
    {
        $rekening = "rekening";
        $produk = "produk";
        $produk_has_jurnal = "produk_has_jurnal";

        $store_id = users::get($db, 'store_id');

        $i_id = $data["id"];
        $i_nama = $data["nama"];
        $i_keterangan = $data["keterangan"];
        $i_jurnal = $data["jurnal"];

        $produkId = $i_id;
        DB::table($db . '.' . $produk)->where(["id" => $i_id, "store_id" => $store_id])->update(["nama" => $i_nama, "keterangan" => $i_keterangan]);

        if (DB::table($db . '.' . $produk)->where(["store_id" => $store_id, "id" => $produkId])->exists()) {
            $arr_jurnal = [];

            foreach ($i_jurnal as $value) {
                if (DB::table($db . '.' . $rekening)->where(["store_id" => $store_id, "id" => $value["id"]])->exists()) { //, "jenis" => "detail"
                    array_push($arr_jurnal, [
                        "produk_id" => $produkId,
                        "rekening_id" => $value["id"],
                        "masuk" => $value["masuk"],
                    ]);
                } else {
                    return ['status' => 'error', 'message' => 'tidak ada data rekening / data rekening bukan rekening detail', 'data' => []];
                }
            }
            DB::table($db . "." . $produk_has_jurnal)->where(["produk_id" => $produkId])->delete();
            DB::table($db . "." . $produk_has_jurnal)->insert($arr_jurnal);
        } else {
            return ['status' => 'error', 'message' => 'tidak ada data produk', 'data' => $produkId];
        }

        return ['status' => 'success', 'message' => 'jurnal produk telah di perbaharui', 'data' => []];

    }

}
