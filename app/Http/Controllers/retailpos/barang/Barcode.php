<?php

namespace App\Http\Controllers\retailpos\barang;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

// use Carbon\Carbon;

class Barcode extends Controller
{

    public static function run($db, $data)
    {
        $sp_barcode = "sp_barcode";

        $store = "store";

        $user_id = Auth::user()->id;
        try {
            $store_id = DB::table($db . '.' . $store)->where(["id" => $data['store_id'], "user_id" => $user_id])->first()->id;
        } catch (\Throwable $th) {
            return ['status' => 'error', 'message' => 'Tidak ada data toko', 'data' => []];
        }

        $res = DB::select("CALL " . $db . "." . $sp_barcode . "('" . htmlentities($data["barcode"]) . "', " . htmlentities($store_id) . ")");

        return ['status' => 'success', 'message' => 'Get data barang', 'data' => $res[0]];

    }

}
