<?php

namespace App\Http\Controllers\retailpos\barang;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DelBarang extends Controller
{

    public static function run($db, $data)
    {
        $barang = "barang";

        $store = "store";

        $user_id = Auth::user()->id;
        try {
            $store_id = DB::table($db . '.' . $store)->where(["id" => $data['store_id'], "user_id" => $user_id])->first()->id;
        } catch (\Throwable $th) {
            return ['status' => 'error', 'message' => 'Tidak ada data toko', 'data' => []];
        }

        $res = DB::table($db . "." . $barang)->where(["id" => $data['id']])->update(["deleted_at" => date("Ymd", Carbon::now()->timestamp)]);

        return ['status' => 'success', 'message' => 'Data barang telah di hapus', 'data' => $res];

    }

}
