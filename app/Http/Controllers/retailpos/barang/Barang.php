<?php

namespace App\Http\Controllers\retailpos\barang;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Barang extends Controller
{

    public static function run($db, $data)
    {
        $kategori = "kategori";
        $barang = "barang";
        $stok = "stok";

        $store = "store";

        $user_id = Auth::user()->id;

        try {
            //hanya owner yg bisa tambah barang
            $store_id = DB::table($db . '.' . $store)->where(["id" => $data['store_id'], "user_id" => $user_id])->first()->id;
        } catch (\Throwable $th) {
            return ['status' => 'error', 'message' => 'Tidak ada data toko', 'data' => []];
        }

        DB::table($db . '.' . $kategori)->insertOrIgnore(["nama" => $data['kategori'], "store_id" => $store_id]);
        $kategori_id = DB::table($db . '.' . $kategori)->where(["nama" => $data['kategori'], "store_id" => $store_id])->first()->id;

        DB::table($db . '.' . $barang)->updateOrInsert(
            ["barcode" => $data['barcode'], "store_id" => $store_id],
            [
                "kategori_id" => $kategori_id,
                "nama" => $data['nama'],
                "harga_beli" => $data['harga_beli'],
                "harga_jual" => $data['harga_jual'],
                "status" => $data['status'],
                "gambar" => $data['gambar'],
                "detail" => $data['detail'],
            ]
        );

        $barang_id = DB::table($db . '.' . $barang)->where(["barcode" => $data['barcode'], "store_id" => $store_id])->first()->id;

        if (isset($data['stok'])) {
            $sum = DB::table($db . '.' . $stok)->selectRaw("(sum(masuk) - sum(keluar)) as tot")->where(["barang_id" => $barang_id])->first()->tot;
            DB::table($db . '.' . $stok)->insert(["tgl" => date("Ymd", Carbon::now()->timestamp), "barang_id" => $barang_id, "faktur" => "BM", "masuk" => $data['stok'] - $sum, "keluar" => 0]);
        }

        return ['status' => 'success', 'message' => 'Data barang telah di perbaharui', 'data' => []];

    }

}
