<?php

namespace App\Http\Controllers\retailpos;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\retailpos\barang\Barang;
use App\Http\Controllers\retailpos\barang\Barcode;
use App\Http\Controllers\retailpos\barang\CariBarang;
use App\Http\Controllers\retailpos\barang\DelBarang;
use App\Http\Controllers\retailpos\kategori\Kategori;
use App\Http\Controllers\retailpos\pelanggan\Pelanggan;
use App\Http\Controllers\retailpos\toko\ReadToko;
use App\Http\Controllers\retailpos\toko\Toko;
use App\Http\Controllers\retailpos\transaksi\Jual;
use App\Http\Controllers\retailpos\laporan\Jual as LapJual;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GateController extends Controller
{

    public function operation($operation, $data, $db)
    {

        $store_has_user = "store_has_user";

        $user_id = Auth::user()->id;
        $store_id = users::get($db, 'store_id');
        if (isset($data['store_id'])) {
            try {
                $store_id = DB::table($db . '.' . $store_has_user)->where(["store_id" => $data['store_id'], "user_id" => $user_id])->first()->store_id;
            } catch (\Throwable $th) {
                return FapiController::response('error', 'Tidak ada data toko', [], 409);
                // $store_id = users::get($db, 'store_id');
            }
        }

        $requestData = [];
        $requestData["data"] = $data;
        switch ($operation) {
            // toko
            case 'readtoko':
                $res = ReadToko::run($db, $data);
                break;
            case 'listtoko':
                $table = DB::table($db . '.store');
                return FapiController::readById($requestData, $table);
                break;
            case 'toko':
                $res = Toko::run($db, $data);
                break;
            case 'deletetoko':
                $requestData["id"] = $data["id"];

                $table = DB::table($db . '.store');
                return FapiController::deleteById($requestData, $table);
                break;
            // kategori
            case 'addkategori':
                if ($store_id == 0) {
                    return FapiController::response('error', "Tidak ada data toko", [], 409);
                }
                $requestData["data"]["store_id"] = $store_id;

                $table = DB::table($db . '.kategori')->where(["store_id" => $store_id]);
                return FapiController::insert($requestData, $table);
                break;
            case 'kategori':
                $res = Kategori::run($db, $data);
                break;
            case 'listkategori':
                if ($store_id == 0) {
                    return FapiController::response('error', "Tidak ada data toko", [], 409);
                }

                $table = DB::table($db . '.kategori')->where(["store_id" => $store_id]);
                return FapiController::read($requestData, $table);
                break;
            case 'updatekategori':
                if ($store_id == 0) {
                    return FapiController::response('error', "Tidak ada data toko", [], 409);
                }
                $requestData["id"] = $data["id"];

                $table = DB::table($db . '.kategori')->where(["store_id" => $store_id]);
                return FapiController::update($requestData, $table);
                break;
            case 'deletekategori':
                if ($store_id == 0) {
                    return FapiController::response('error', "Tidak ada data toko", [], 409);
                }
                $requestData["id"] = $data["id"];

                $table = DB::table($db . '.kategori')->where(["store_id" => $store_id]);
                return FapiController::delete($requestData, $table);
                break;
            // pelanggan
            case 'addpelanggan':
                if ($store_id == 0) {
                    return FapiController::response('error', "Tidak ada data toko", [], 409);
                }
                $requestData["data"]["store_id"] = $store_id;

                $table = DB::table($db . '.pelanggan')->where(["store_id" => $store_id]);
                return FapiController::insert($requestData, $table);
                break;
            case 'pelanggan':
                $res = Pelanggan::run($db, $data);
                break;
            case 'listpelanggan':
                if ($store_id == 0) {
                    return FapiController::response('error', "Tidak ada data toko", [], 409);
                }

                $table = DB::table($db . '.pelanggan')->where(["store_id" => $store_id]);
                return FapiController::read($requestData, $table);
                break;
            case 'updatepelanggan':
                if ($store_id == 0) {
                    return FapiController::response('error', "Tidak ada data toko", [], 409);
                }
                $requestData["id"] = $data["id"];

                $table = DB::table($db . '.pelanggan')->where(["store_id" => $store_id]);
                return FapiController::update($requestData, $table);
                break;
            case 'deletepelanggan':
                if ($store_id == 0) {
                    return FapiController::response('error', "Tidak ada data toko", [], 409);
                }
                $requestData["id"] = $data["id"];

                $table = DB::table($db . '.pelanggan')->where(["store_id" => $store_id]);
                return FapiController::delete($requestData, $table);
                break;
            // project
            case 'addproject':
                if ($store_id == 0) {
                    return FapiController::response('error', "Tidak ada data toko", [], 409);
                }
                $requestData["data"]["store_id"] = $store_id;

                $table = DB::table($db . '.project')->where(["store_id" => $store_id]);
                return FapiController::insert($requestData, $table);
                break;
            case 'readproject':
                if ($store_id == 0) {
                    return FapiController::response('error', "Tidak ada data toko", [], 409);
                }

                $table = DB::table($db . '.project')->where(["store_id" => $store_id]);
                return FapiController::read($requestData, $table);
                break;
            case 'updateproject':
                if ($store_id == 0) {
                    return FapiController::response('error', "Tidak ada data toko", [], 409);
                }
                $requestData["id"] = $data["id"];

                $table = DB::table($db . '.project')->where(["store_id" => $store_id]);
                return FapiController::update($requestData, $table);
                break;
            case 'deleteproject':
                if ($store_id == 0) {
                    return FapiController::response('error', "Tidak ada data toko", [], 409);
                }
                $requestData["id"] = $data["id"];

                $table = DB::table($db . '.project')->where(["store_id" => $store_id]);
                return FapiController::delete($requestData, $table);
                break; // barang
            case 'listbarang':
                if ($store_id == 0) {
                    return FapiController::response('error', "Tidak ada data toko", [], 409);
                }

                $table = DB::table($db . '.v_barang')->where(["store_id" => $store_id]);
                return FapiController::read($requestData, $table);
                break;
            case 'barang':
                $data["store_id"] = $store_id;
                $res = Barang::run($db, $data);
                break;
            case 'barcode':
                $data["store_id"] = $store_id;
                $res = Barcode::run($db, $data);
                break;
            case 'caribarang':
                $data["store_id"] = $store_id;
                $res = CariBarang::run($db, $data);
                break;
            case 'delbarang':
                $data["store_id"] = $store_id;
                $res = DelBarang::run($db, $data);
                break;
            // cara bayar
            case 'addcarabayar':
                if ($store_id == 0) {
                    return FapiController::response('error', "Tidak ada data toko", [], 409);
                }
                $requestData["data"]["store_id"] = $store_id;

                $table = DB::table($db . '.carabayar')->where(["store_id" => $store_id]);
                return FapiController::insert($requestData, $table);
                break;
            case 'readcarabayar':
                if ($store_id == 0) {
                    return FapiController::response('error', "Tidak ada data toko", [], 409);
                }

                $table = DB::table($db . '.carabayar')->where(["store_id" => $store_id]);
                return FapiController::read($requestData, $table);
                break;
            case 'updatecarabayar':
                if ($store_id == 0) {
                    return FapiController::response('error', "Tidak ada data toko", [], 409);
                }
                $requestData["id"] = $data["id"];

                $table = DB::table($db . '.carabayar')->where(["store_id" => $store_id]);
                return FapiController::update($requestData, $table);
                break;
            case 'deletecarabayar':
                if ($store_id == 0) {
                    return FapiController::response('error', "Tidak ada data toko", [], 409);
                }
                $requestData["id"] = $data["id"];

                $table = DB::table($db . '.carabayar')->where(["store_id" => $store_id]);
                return FapiController::delete($requestData, $table);
                break;
            // trx
            case 'jual':
                $data["store_id"] = $store_id;
                $res = Jual::run($db, $data);
                break;
            // auth
            case 'getusers':
                $data["gate"] = "sp_users";
                $data["data"] = [$user_id];
                return FapiController::sp($db, $data);
                break;
            // laporan
            case 'laptrxjual':
                if ($store_id == 0) {
                    return FapiController::response('error', "Tidak ada data toko", [], 409);
                }

                $send = [];
                $send["gate"] = "sp_laporan_transaksi_jual";
                $send["data"][0] = $requestData["data"]["awal"];
                $send["data"][1] = $requestData["data"]["akhir"];
                $send["data"][2] = $store_id;
                $res = LapJual::run($db, $send);
                break;
            case 'detailtrxjual':
                if ($store_id == 0) {
                    return FapiController::response('error', "Tidak ada data toko", [], 409);
                }

                $send = [];
                $send["gate"] = "sp_detail_jual";
                $send["data"][0] = $requestData["data"]["trx_id"];
                return FapiController::sp($db, $send);
                break;

            default:
                try {
                    ucfirst($operation)::run($db, $data);
                } catch (\Throwable $th) {
                    return FapiController::response('error', "operation $operation not found", [], 400);
                }
                break;
        }

        return FapiController::response($res['status'], $res['message'], $res['data'], isset($res['error']) ? 409 : 200);
    }
}
