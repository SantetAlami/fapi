<?php

namespace App\Http\Controllers\retailpos\kategori;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

// use Carbon\Carbon;

class Kategori extends Controller
{

    public static function run($db, $data)
    {
        $kategori = "kategori";

        $store = "store";

        $user_id = Auth::user()->id;
        try {
            $store_id = DB::table($db . '.' . $store)->where(["id" => $data['store_id'], "user_id" => $user_id])->first()->id;
        } catch (\Throwable $th) {
            return ['status' => 'error', 'message' => 'Tidak ada data toko', 'data' => []];
        }

        $res = DB::table($db . "." . $kategori)->where(["store_id" => $store_id])->get();

        return ['status' => 'success', 'message' => 'Get data kategori', 'data' => $res];

    }

}
