<?php

namespace App\Http\Controllers\retailpos\transaksi;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Jual extends Controller
{

    public static function run($db, $data)
    {
        $transaksi_jual = "transaksi_jual";
        $detail_jual = "detail_jual";
        $project_has_jual = "project_has_jual";
        $stok = "stok";

        $store_has_user = "store_has_user";
        $barang = "barang";
        $project = "project";

        $user_id = Auth::user()->id;
        //cek akses store
        $store = DB::table($db . '.' . $store_has_user)->where(["store_id" => $data["store_id"], "user_id" => $user_id])->get();

        if (count($store) > 0) {
            $pFaktur = "TJ";

            $total_harga = 0;
            $jumlah_barang = 0;

            $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

            foreach ($data["items"] as $item) {
                try {
                    $barangs = DB::table($db . '.' . $barang)->where(["id" => $item["id"], "store_id" => $data["store_id"]])->first();

                    $total_harga = $total_harga + ($barangs->harga_jual * $item["jumlah"]);
                    $jumlah_barang++;
                } catch (\Throwable $th) {
                    return ['status' => 'error', 'message' => 'Tidak ada data barang', 'data' => []];
                }
            }

            // insert transaksi jual
            $trx_id = DB::table($db . '.' . $transaksi_jual)->insertGetId([
                "pelanggan_id" => $data["pelanggan_id"],
                "carabayar_id" => $data["carabayar_id"],
                "tgl" => $data["tgl"],
                "faktur" => $faktur,
                "status" => (($data["bayar"] - $total_harga) >= 0) ? 1 : 0, //lunas atau belum

                "total" => $total_harga,
                "jumlah_item" => $jumlah_barang,

                "bayar" => $data["bayar"],
                "kembali" => $data["bayar"] - $total_harga,

                "user_id" => $user_id,
                "store_id" => $data["store_id"],
            ]);

            // insert detail trx
            foreach ($data["items"] as $item) {
                $barang_res = DB::table($db . '.' . $barang)->where(["id" => $item["id"], "store_id" => $data["store_id"]])->first();

                DB::table($db . '.' . $detail_jual)->insert([
                    "transaksi_id" => $trx_id,
                    "barang_id" => $item["id"],
                    "faktur" => $faktur,
                    "harga_beli" => $barang_res->harga_beli,
                    "harga_jual" => $barang_res->harga_jual,
                    "jumlah_barang" => $item["jumlah"],
                    "total" => $barang_res->harga_jual * $item["jumlah"],
                ]);

                // update stok

                DB::table($db . '.' . $stok)->insert(["tgl" => $data["tgl"], "barang_id" => $item["id"], "faktur" => $faktur, "masuk" => 0, "keluar" => $item["jumlah"]]);
            }

            // insert project
            if(isset($data["project"])){
                foreach ($data["project"] as $item) {
                    DB::table($db . '.' . $project)->updateOrInsert([
                        "nama" => $item,
                        "store_id" => $data["store_id"],
                    ], []);
    
                    $project_id = DB::table($db . '.' . $project)->where([
                        "nama" => $item,
                        "store_id" => $data["store_id"],
                    ])->first()->id;
    
                    DB::table($db . '.' . $project_has_jual)->insert(["project_id" => $project_id, "penjualan_id" => $trx_id]);
                }    
            }

            return ['status' => 'success', 'message' => 'Transaksi baru telah di tambahkan', 'data' => []];
        }

        return ['status' => 'error', 'message' => 'Tidak ada data toko', 'data' => []];

    }

}
