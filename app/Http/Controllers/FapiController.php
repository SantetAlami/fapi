<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Events\MessageSent;
use App\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class FapiController extends Controller
{

    /**
     * Insert method.
     *
     * @param  Array  $requestData
     * @param  DB::class  $table
     * @return Response
     */

    public static function insert($requestData, $table)
    {
        try {
            $sg = explode(" ", str_replace("`", "", $table->toSql()));
            $sg = explode(".", end($sg));

            $children = [];
            foreach ($requestData['data'] as $key => $data) {
                try {
                    if (is_array($data)) {
                        $children[$key] = $data;
                        unset($requestData['data'][$key]);
                    }

                    if ($data->getClientOriginalName() != "") {
                        $data->move("$sg[0]/$sg[1]", (string) Str::uuid() . $data->getClientOriginalName());
                        $requestData['data'][$key] = "/$sg[0]/$sg[1]/" . (string) Str::uuid() . $data->getClientOriginalName();
                    }
                } catch (\Throwable $th) {}
            }

            $id = $table->insertGetId($requestData['data']);
            $res = $table->where('id', $id)->first();

            foreach ($children as $key => $child) {
                $item = $child;
                foreach ($child as $idx => $value) {
                    $item[$idx][$sg[1] . "_id"] = $id;
                }
                DB::table($sg[0] . '.' . $key)->insert($item);
                $res->$key = $child;
            }

            return FapiController::response('success', 'data has successfully created', $res, 201);

        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    public static function where($request, $table)
    {
        //where
        if (isset($request['orWhere'])) {
            foreach ($request['orWhere'] as $key => $value) {
                $table->orWhere($key, '=', $value);
            }
        }
        if (isset($request['orWhereLike'])) {
            foreach ($request['orWhere'] as $key => $value) {
                $table->orWhere($key, 'LIKE', $value);
            }
        }
        if (isset($request['where'])) {
            foreach ($request['where'] as $key => $value) {
                $table->where($key, $value);
            }
        }
        if (isset($request['whereLike'])) {
            foreach ($request['whereLike'] as $key => $value) {
                $table->where($key, 'LIKE', $value);
            }
        }
        if (isset($request['whereNot'])) {
            foreach ($request['whereNot'] as $key => $value) {
                $table->where($key, '<>', $value);
            }
        }
        if (isset($request['whereNull'])) {
            foreach ($request['whereNull'] as $key => $value) {
                $table->whereNull($value);
            }
        }
        if (isset($request['whereNotNull'])) {
            foreach ($request['whereNotNull'] as $key => $value) {
                $table->whereNotNull($value);
            }
        }
        if (isset($request['whereLess'])) {
            foreach ($request['whereLess'] as $key => $value) {
                $table->where($key, '<', $value);
            }
        }
        if (isset($request['whereMore'])) {
            foreach ($request['whereMore'] as $key => $value) {
                $table->where($key, '>', $value);
            }
        }
        if (isset($request['whereLessEq'])) {
            foreach ($request['whereLessEq'] as $key => $value) {
                $table->where($key, '<=', $value);
            }
        }
        if (isset($request['whereMoreEq'])) {
            foreach ($request['whereMoreEq'] as $key => $value) {
                $table->where($key, '>=', $value);
            }
        }
        if (isset($request['whereIn'])) {
            foreach ($request['whereIn'] as $key => $value) {
                $table->whereIn($key, $value);
            }
        }
        if (isset($request['whereNull'])) {
            foreach ($request['whereNull'] as $key => $value) {
                $table->whereNull($value);
            }
        }
        if (isset($request['whereBetween'])) {
            foreach ($request['whereBetween'] as $key => $value) {
                $table->whereBetween($key, $value);
            }
        }
        if (isset($request['whereNotBetween'])) {
            foreach ($request['whereNotBetween'] as $key => $value) {
                $table->whereNotBetween($key, $value);
            }
        }

        if (isset($request['search'])) {
            $table->where(function ($query) use ($request) {
                foreach ($request['search'] as $key => $value) {
                    $query->orWhere($key, 'LIKE', "%" . $value . "%");
                }
            });
        }

        //limit take skip
        if (isset($request['offset'])) {
            $table->offset($request['offset']);
        }
        if (isset($request['limit'])) {
            $table->limit($request['limit']);
        }
        if (isset($request['skip'])) {
            $table->skip($request['skip']);
        }
        if (isset($request['take'])) {
            $table->take($request['take']);
        }

        //orderby groupby
        if (isset($request['groupBy'])) {
            $table->groupBy($request['groupBy']);
        }
        if (isset($request['orderBy'])) {
            $table->orderBy($request['orderBy']);
        }
        if (isset($request['orderByDesc'])) {
            $table->orderByDesc($request['orderByDesc']);
        }

        return $table;
    }

    /**
     * Read by id method.
     *
     * @param  Array  $requestData
     * @param  DB::class  $table
     * @return Response
     */

    public static function readById($requestData, $table)
    {
        try {
            $sg = explode(" ", str_replace("`", "", $table->toSql()));
            $sg = explode(".", end($sg));

            if (isset($requestData['data']['select'])) {
                if (is_array($requestData['data']['select'])) {
                    $select = $requestData['data']['select'];
                } else {
                    $select = [];
                    foreach (explode(',', $requestData['data']['select']) as $key => $value) {
                        $select[] = ltrim(rtrim($value));
                    }
                }

                $table->select($select);
            }

            $table = FapiController::where($requestData['data'], $table);

            if (isset($requestData['data']['and'])) {
                $and = $requestData['data']['and'];
                $table->where(function ($q) use ($and) {
                    $q = FapiController::where($and, $q);
                });
            }

            if (isset($requestData['data']['or'])) {
                $or = $requestData['data']['or'];
                $table->orWhere(function ($q) use ($or) {
                    $q = FapiController::where($or, $q);
                });
            }

            $res = $table->where('user_id', Auth::user()->id)->get();

            if ($res->isEmpty() && isset($requestData['data']['show_column'])) {
                $cols = DB::select("SELECT `COLUMN_NAME`
                    FROM `INFORMATION_SCHEMA`.`COLUMNS`
                    WHERE `TABLE_SCHEMA`='$sg[0]'
                    AND `TABLE_NAME`='$sg[1]';");

                $res = [[]];
                foreach ($cols as $key => $col) {
                    $res[0][$col->COLUMN_NAME] = "";
                }
            }

            if (count($res) == 0) {
                return FapiController::response('error', 'data not found', $res, 404);
            }

            return FapiController::response('success', 'showing all data', $res, 200);

        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    /**
     * Read method.
     *
     * @param  Array  $requestData
     * @param  DB::class  $table
     * @return Response
     */

    public static function read($requestData, $table)
    {
        try {
            $sg = explode(" ", str_replace("`", "", $table->toSql()));
            $sg = explode(".", end($sg));
            if (isset($requestData['id'])) {
                $table->where('id', $requestData['id']);
            }

            if (isset($requestData['data']['select'])) {
                if (is_array($requestData['data']['select'])) {
                    $select = $requestData['data']['select'];
                } else {
                    $select = [];
                    foreach (explode(',', $requestData['data']['select']) as $key => $value) {
                        $select[] = ltrim(rtrim($value));
                    }
                }

                $table->select($select);
            }

            $table = FapiController::where($requestData['data'], $table);

            if (isset($requestData['data']['and'])) {
                $and = $requestData['data']['and'];
                $table->where(function ($q) use ($and) {
                    $q = FapiController::where($and, $q);
                });
            }

            if (isset($requestData['data']['or'])) {
                $or = $requestData['data']['or'];
                $table->orWhere(function ($q) use ($or) {
                    $q = FapiController::where($or, $q);
                });
            }

            $res = $table->get();

            if ($res->isEmpty() && isset($requestData['data']['show_column'])) {
                $cols = DB::select("SELECT `COLUMN_NAME`
                    FROM `INFORMATION_SCHEMA`.`COLUMNS`
                    WHERE `TABLE_SCHEMA`='$sg[0]'
                    AND `TABLE_NAME`='$sg[1]';");

                $res = [[]];
                foreach ($cols as $key => $col) {
                    $res[0][$col->COLUMN_NAME] = "";
                }
            }
            if (count($res) == 0) {
                return FapiController::response('success', 'data not found', $res, 200);
            }

            return FapiController::response('success', 'showing all data', $res, 200);

        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    /**
     * Update by id method.
     *
     * @param  Array  $requestData
     * @param  DB::class  $table
     * @return Response
     */

    public static function updateById($requestData, $table)
    {
        try {
            $sg = explode(" ", str_replace("`", "", $table->toSql()));
            $sg = explode(".", end($sg));

            $cek = $table->where('id', $requestData['id'])->where('user_id', Auth::user()->id)->first();
            if (empty($cek)) {
                return FapiController::response('error', 'data not found, so it cannot be updated', null, 400);
            }

            $children = [];
            foreach ($requestData['data'] as $key => $data) {
                try {
                    if (is_array($data)) {
                        $children[$key] = $data;
                        unset($requestData['data'][$key]);
                    }

                    if ($data->getClientOriginalName() != "") {
                        if (File::exists("$sg[0]/$sg[1]/$cek->$key")) {
                            File::delete("$sg[0]/$sg[1]/$cek->$key");
                        }
                        $data->move("$sg[0]/$sg[1]", (string) Str::uuid() . $data->getClientOriginalName());
                        $requestData['data'][$key] = "/$sg[0]/$sg[1]/" . (string) Str::uuid() . $data->getClientOriginalName();
                    }
                } catch (\Throwable $th) {}
            }

            $table = FapiController::where($requestData['data'], $table);

            unset($requestData['data']["where"]);

            $table->update($requestData['data']);

            $res = $table->where('id', $requestData['id'])->where('user_id', Auth::user()->id)->first();

            foreach ($children as $key => $child) {
                $item = $child;
                foreach ($child as $idx => $value) {
                    $item[$idx][$sg[1] . "_id"] = $requestData['id'];
                }
                DB::table($sg[0] . '.' . $key)->insert($item);
                $res->$key = $child;
            }

            return FapiController::response('success', 'data has successfully updated', $res, 200);

        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    /**
     * Update method.
     *
     * @param  Array  $requestData
     * @param  DB::class  $table
     * @return Response
     */

    public static function update($requestData, $table)
    {
        try {
            $sg = explode(" ", str_replace("`", "", $table->toSql()));
            $sg = explode(".", end($sg));

            $cek = $table->where('id', $requestData['id'])->first();
            if (empty($cek)) {
                return FapiController::response('error', 'data not found, so it cannot be updated', null, 400);
            }

            $children = [];
            foreach ($requestData['data'] as $key => $data) {
                try {
                    if (is_array($data)) {
                        $children[$key] = $data;
                        unset($requestData['data'][$key]);
                    }

                    if ($data->getClientOriginalName() != "") {
                        if (File::exists("$sg[0]/$sg[1]/$cek->$key")) {
                            File::delete("$sg[0]/$sg[1]/$cek->$key");
                        }
                        $data->move("$sg[0]/$sg[1]", (string) Str::uuid() . $data->getClientOriginalName());
                        $requestData['data'][$key] = "/$sg[0]/$sg[1]/" . (string) Str::uuid() . $data->getClientOriginalName();
                    }
                } catch (\Throwable $th) {}
            }

            $table = FapiController::where($requestData['data'], $table);

            unset($requestData['data']["where"]);

            $table->update($requestData['data']);

            $res = $table->where('id', $requestData['id'])->first();

            try {
                foreach ($children as $key => $child) {
                    $item = $child;
                    foreach ($child as $idx => $value) {
                        $item[$idx][$sg[1] . "_id"] = $requestData['id'];
                    }
                    DB::table($sg[0] . '.' . $key)->insert($item);
                    $res->$key = $child;
                }
            } catch (\Throwable $th) {
                //throw $th;
            }

            return FapiController::response('success', 'data has successfully updated', $res, 200);

        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    /**
     * Delete by id method.
     *
     * @param  Array  $requestData
     * @param  DB::class  $table
     * @return Response
     */

    public static function deleteById($requestData, $table)
    {
        try {
            $res = $table->where('id', $requestData['id'])->where('user_id', Auth::user()->id)->delete();
            if (empty($res)) {
                return FapiController::response('error', 'data not found, so it cannot be deleted', null, 400);
            }

            return FapiController::response('success', 'data has successfully deleted', null, 200);

        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    /**
     * Delete method.
     *
     * @param  Array  $requestData
     * @param  DB::class  $table
     * @return Response
     */

    public static function delete($requestData, $table)
    {
        try {
            // if (isset($requestData['id'])) {
            $res = $table->where('id', $requestData['id'])->delete();
            // } else {
            //     $table = FapiController::where($requestData['data'], $table);
            //     $res = $table->delete();
            // }

            if (empty($res)) {
                return FapiController::response('error', 'data not found, so it cannot be deleted', null, 400);
            }

            return FapiController::response('success', 'data has successfully deleted', null, 200);

        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    /**
     * Delete method.
     *
     * @param  Array  $requestData
     * @param  DB::class  $table
     * @return Response
     */

    public static function sp($db, $requestData)
    {
        try {
            $res = DB::select('CALL ' . $db . '.' . htmlentities($requestData['gate']) . "('" . htmlentities(implode("','", $requestData['data'])) . "')");

            return FapiController::response('success', 'showing all data', $res, 200);

        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    public function test($requestData)
    {
        try {
            $channel_id = $requestData['channel_id'];
            $message = $requestData['message'];
            $reply_id = $requestData['reply_id'] ?? null;

            $channel = Channel::where('id', $channel_id)->first();

            $send = [
                'channel_id' => $channel->id,
                'user_id' => Auth::user()->id,
                'message' => $message,
                'message_id' => $reply_id,
                'author_username' => Auth::user()->name,
            ];
            $message = Message::create($send);

            event(new MessageSent('Channel-' . $channel->id, $send));
            return FapiController::response('success', 'get data', $message, 200);

        } catch (\Throwable $e) {
            // dd($e);
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    public function test2()
    {
        try {
            $res = DB::table('sewa_ban.ban')->where('vendor', 'Jack William')->orWhere('vendor', 'Louis Ergi Al-xavieri Yuwanta')->get();
            return FapiController::response('success', 'get data', $res, 200);

        } catch (\Throwable $e) {
            // dd($e);
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    /**
     * Retrun request to json.
     *
     * @param  String  $status
     * just allow string "error" or "success".
     * @param  String  $message
     * message response to tell what has happened.
     * @param  Array  $data
     * data response from here to requester.

     * give empty array [] or null if not giving data
     * @param  Int  $code
     * response code exp

     * 1xx Informational
     * -----------

     *  100 Continue

     *  101 Switching Protocols

     *  102 Processing (WebDAV)

     * 2xx Success
     * -----------

     *  *200 OK

     *  *201 Created

     *  202 Accepted

     *  203 Non-Authoritative Information

     *  *204 No Content

     *  205 Reset Content

     *  206 Partial Content

     *  207 Multi-Status (WebDAV)

     *  208 Already Reported (WebDAV)

     *  226 IM Used

     * 3xx Redirection
     * -----------

     *  300 Multiple Choices

     *  301 Moved Permanently

     *  302 Found

     *  303 See Other

     *  *304 Not Modified

     *  305 Use Proxy

     *  306 (Unused)

     *  307 Temporary Redirect

     *  308 Permanent Redirect (experimental)

     * 4xx Client Error
     * -----------

     *  *400 Bad Request

     *  *401 Unauthorized

     *  402 Payment Required

     *  *403 Forbidden

     *  *404 Not Found

     *  405 Method Not Allowed

     *  406 Not Acceptable

     *  407 Proxy Authentication Required

     *  408 Request Timeout

     *  *409 Conflict

     *  410 Gone

     *  411 Length Required

     *  412 Precondition Failed

     *  413 Request Entity Too Large

     *  414 Request-URI Too Long

     *  415 Unsupported Media Type

     *  416 Requested Range Not Satisfiable

     *  417 Expectation Failed

     *  418 I'm a teapot (RFC 2324)

     *  420 Enhance Your Calm (Twitter)

     *  422 Unprocessable Entity (WebDAV)

     *  423 Locked (WebDAV)

     *  424 Failed Dependency (WebDAV)

     *  425 Reserved for WebDAV

     *  426 Upgrade Required

     *  428 Precondition Required

     *  429 Too Many Requests

     *  431 Request Header Fields Too Large

     *  444 No Response (Nginx)

     *  449 Retry With (Microsoft)

     *  450 Blocked by Windows Parental Controls (Microsoft)

     *  451 Unavailable For Legal Reasons

     *  499 Client Closed Request (Nginx)

     * 5xx Server Error
     * -----------

     *  *500 Internal Server Error

     *  501 Not Implemented

     *  502 Bad Gateway

     *  503 Service Unavailable

     *  504 Gateway Timeout

     *  505 HTTP Version Not Supported

     *  506 Variant Also Negotiates (Experimental)

     *  507 Insufficient Storage (WebDAV)

     *  508 Loop Detected (WebDAV)

     *  509 Bandwidth Limit Exceeded (Apache)

     *  510 Not Extended

     *  511 Network Authentication Required

     *  598 Network read timeout error

     *  599 Network connect timeout error

     *-----

     *  "*"
     * --
     * Top 10 HTTP Status Code. More REST service-specific information is contained in the entry.

     * @return Response
     */

    public static function response($status, $message, $data, $code)
    {
        try {
            $count = count($data);
        } catch (\Throwable $th) {
            $count = 0;
        }
        return [
            'code' => $code,
            'res' => [
                $status => true,
                'message' => $message,
                'data' => $data,
                'rows' => $count,
            ],
        ];
    }

    /**
     * Method checker.
     *
     * @param  String  $set
     * @param  Array  $send
     * @param  Class  $next
     * @return Response
     */

    public static function method($set, $send, $next)
    {
        try {
            if ($set == $send or (is_array($send) and in_array($set, $send))) {
                return $next;
            }
            return FapiController::response('error', 'Method not allowed', [], 405);
        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

}
