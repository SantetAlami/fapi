<?php

namespace App\Http\Controllers;

use App\Http\Controllers\FapiController;
use App\Permission;
// use Illuminate\Http\Request;
use App\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PermissionController extends Controller
{

    /**
     * Get roles data for showing master role.
     *
     * @param  Int  $idUserRole
     * @param  Array  $staff
     * @param  Int  $idRole
     * @return \Illuminate\Http\Response
     * */

    public static function read($idUserRole, $staff, $idRole)
    {
        try {
            $role = Role::findOrFail($idUserRole);
            if ($role->level < Auth::user()->level()) {
                return FapiController::response('error', 'not enough role level', null, 403);
            } elseif (!in_array($role->group, Auth::user()->roleGroup()) && !in_array('su', Auth::user()->roleGroup())) {
                return FapiController::response('error', 'your role don\'t have authority', null, 403);
            }

            if (!empty($staff)) {
                $res = DB::table('permissions_manager')->whereIn('staff', $staff)->orderBy('staff')->orderBy('category')->orderBy('title')->get();
            } else {
                $res = DB::table('permissions_manager')->orderBy('staff')->orderBy('category')->orderBy('title')->get();
            }

            $IdPermits = DB::table('role_has_permissions')->where('role_id', $idUserRole)->pluck('permission_id');
            $permits = DB::table('permissions')->whereIn('id', $IdPermits)->pluck('name')->toArray();

            if (!empty($idRole)) {
                $IdPermitsActive = DB::table('role_has_permissions')->where('role_id', $idRole)->pluck('permission_id');
                $permitsActive = DB::table('permissions')->whereIn('id', $IdPermitsActive)->pluck('name')->toArray();
            }
            foreach ($res as $key => $item) {
                $res[$key]->modify = false;
                $res[$key]->active = false;
                if (count(array_intersect(explode(',', $item->permissions_name), $permits)) == count(explode(',', $item->permissions_name))) {
                    $res[$key]->modify = true;
                }
                if (isset($permitsActive) && count(array_intersect(explode(',', $item->permissions_name), $permitsActive)) == count(explode(',', $item->permissions_name))) {
                    $res[$key]->active = true;
                }
            }

            return FapiController::response('success', 'get all permission data', $res, 200);
        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Array  $request
     * @param  Int  $level
     * @param  Array  $group
     * @return \Illuminate\Http\Response
     */
    public static function store($request)
    {
        try {
            $manages = array_cut_value(array_search_like(Auth::user()->getManagePermit(), 'manage-%'), 'manage-');

            $permissions = [];
            foreach ($manages as $key => $value) {
                $permissions = array_merge($permissions,array_search_like($request['permissions'], '%' . $value));
            }

            // Filtering the array
            foreach ($permissions as $key => $value) {
                if(empty($value)) {
                    unset($permissions[$key]);
                }
            }

            $res = DB::table('permissions_manager')->insert(
                [
                    'staff' => $request['staff'],
                    'title' => $request['title'],
                    'category' => $request['category'],
                    'description' => $request['description'] ?? '',
                    'permissions_name' => implode(',', $permissions),
                ]
            );

            return FapiController::response('success', 'new permission has successfully created', $res, 201);
        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Array  $request
     * @param  Int  $id
     * @param  Int  $level
     * @param  Array  $group
     * @param  Bool  $manage
     * user can manage role
     * @return \Illuminate\Http\Response
     */
    public static function update($id, $request)
    {
        try {
            $manages = array_cut_value(array_search_like(Auth::user()->getManagePermit(), 'manage-%'), 'manage-');

            $permissions = [];
            foreach ($manages as $key => $value) {
                $permissions = array_merge($permissions,array_search_like($request['permissions'], '%' . $value));
            }
            
            // Filtering the array
            foreach ($permissions as $key => $value) {
                if(empty($value)) {
                    unset($permissions[$key]);
                }
            }

            $res = DB::table('permissions_manager')->where('id', $id)->update(
                [
                    'staff' => $request['staff'] ?? "",
                    'title' => $request['title'] ?? "",
                    'category' => $request['category'] ?? "",
                    'description' => $request['description'] ?? "",
                    'permissions_name' => implode(',', $permissions),
                ]
            );

            return FapiController::response('success', 'the permission has successfully updated', $res, 200);
        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Int  $id
     * @param  Int  $level
     * @param  Array  $group
     * @return \Illuminate\Http\Response
     */
    public static function destroy($id)
    {
        try {
            $res = DB::table('permissions_manager')->where('id', $id)->delete();

            if (empty($res)) {
                return FapiController::response('error', 'data not found, so it cannot be deleted', null, 400);
            }

            return FapiController::response('success', 'new permission has successfully created', $res, 201);
        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }

    }
}
