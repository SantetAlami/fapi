<?php

namespace App\Http\Controllers\Ban\update;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MutasiMobil extends Controller
{

    public static function run($db, $data)
    {
        //
        $lokasi = 'lokasi_mobil';
        $km = 'km_mobil';
        $pindah_lokasi = 'pindah_lokasi_mobil';

        DB::table($db . '.' . $pindah_lokasi)->where('faktur', $data['faktur'])->update([
            'tgl' => $data['tgl'],
            'user_id' => Auth::user()->id,
            'mobil_id' => $data['mobil_id'],
            'lokasi_id_sebelum' => $data['lokasi_sebelum'],
            'lokasi_id_sesudah' => $data['lokasi_sesudah'],
            'odometer_awal' => $data['odometer_awal'],
            'odometer_akhir' => $data['odometer_akhir'],
            'keterangan' => 'Ubah lokasi mobil',
        ]);

        DB::table($db . '.' . $km)->where('faktur', $data['faktur'])->update([
            'tgl' => $data['tgl'],
            'mobil_id' => $data['mobil_id'],
            'user_id' => Auth::user()->id,
            'odometer_awal' => $data['odometer_awal'],
            'odometer_akhir' => $data['odometer_akhir'],
            'keterangan' => 'Update odometer',
        ]);

        DB::table($db . '.' . $lokasi)->where('faktur', $data['faktur'])->update([
            'tgl' => $data['tgl'],
            'mobil_id' => $data['mobil_id'],
            'user_id' => Auth::user()->id,
            'kota_id' => $data['lokasi'],
            'keterangan' => 'Ubah lokasi mobil',
        ]);

        return ['message' => 'Update lokasi mobil terupdate', 'data' => []];
    }
}
