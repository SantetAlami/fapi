<?php

namespace App\Http\Controllers\Ban\update;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MutasiBan extends Controller
{

    public static function run($db, $data)
    {
        //

        $table = 'lokasi_ban';
        $uTable = 'pindah_lokasi_ban';
        foreach ($data['bans'] as $ban) {
            DB::table($db . '.' . $uTable)->where('faktur', $ban['faktur'])->update([
                'tgl' => $data['tgl'],
                'user_id' => Auth::user()->id,
                'ban_id' => $ban['id'],
                'lokasi_id_sebelum' => $data['lokasi_sebelum'],
                'lokasi_id_sesudah' => $data['lokasi_sesudah'],
                'keterangan' => 'Ubah lokasi ban',
            ]);

            DB::table($db . '.' . $table)->where('faktur', $ban['faktur'])->update([
                'tgl' => $data['tgl'],
                'ban_id' => $ban['id'],
                'user_id' => Auth::user()->id,
                'kota_id' => $data['lokasi'],
                'keterangan' => 'Ubah lokasi ban',
            ]
            );
        }

        return ['message' => 'Update lokasi ban terupdate', 'data' => []];
    }

}
