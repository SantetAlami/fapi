<?php

namespace App\Http\Controllers\Ban\update;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Pasang extends Controller
{
    public static function run($db, $data)
    {
        $posisi = "posisi";
        $km_ban = "km_ban";
        $kondisi = "kondisi";
        $uTable = "ganti_ban";

        foreach ($data['bans'] as $k => $ban) {

            DB::table($db . '.' . $posisi)->where('faktur', $ban['faktur'])->update([
                'tgl' => $data['tgl'],
                'ban_id' => $ban['id'],
                'user_id' => Auth::user()->id,
                'mobil_id' => $data['mobil_id'],
                'posisi' => $ban['posisi'],
                'keterangan' => 'Pasang Ban',
            ]);
            DB::table($db . '.' . $km_ban)->where('faktur', $ban['faktur'])->update([
                'tgl' => $data['tgl'],
                'ban_id' => $ban['id'],
                'user_id' => Auth::user()->id,
                'odometer_awal' => $data['odometer'],
                'odometer_akhir' => $data['odometer'],
                'keterangan' => 'Pasang Ban',
            ]);
            DB::table($db . '.' . $kondisi)->where('faktur', $ban['faktur'])->update([
                'tgl' => $data['tgl'],
                'ban_id' => $ban['id'],
                'user_id' => Auth::user()->id,
                'kondisi_id' => 1,
                'keterangan' => 'Pasang Ban',
            ]);

            DB::table($db . '.' . $uTable)->where('faktur', $ban['faktur'])->update([
                'tgl' => $data['tgl'],
                'user_id' => Auth::user()->id,
                'mobil_id' => $data['mobil_id'],
                'ban_id_lepas' => null,
                'odo_lepas' => null,
                'kondisi_id_lepas' => null,
                'ketebalan_lepas' => null,
                'ban_id_pasang' => $ban['id'],
                'posisi_pasang' => $ban['posisi'],
                'odo_pasang' => $data['odometer'],
                'keterangan' => 'Pasang Ban',
            ]);

        }

        return ['message' => 'Update pasang ban success', 'data' => []];
    }

}
