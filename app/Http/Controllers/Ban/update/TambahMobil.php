<?php

namespace App\Http\Controllers\Ban\update;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TambahMobil extends Controller
{
    public static function run($db, $data)
    {
        $mobil_tabel = "mobil";
        $lokasi = "lokasi_mobil";
        $km_mobil = "km_mobil";

        $faktur = $data['faktur'];

        $mobil = DB::table($db . '.' . $mobil_tabel)->where('id', $data['id'])->update(
            [
                'no_polisi' => $data['no_polisi'],
                'kapasitas' => $data['kapasitas'],
                'jumlah_ban' => $data['jumlah_ban'],
                'jumlah_serep' => $data['jumlah_serep'],
            ]
        );

        DB::table($db . '.' . $lokasi)->where('faktur', $faktur)->update([
            'tgl' => $data['tgl'],
            'mobil_id' => $data['id'],
            'user_id' => Auth::user()->id,
            'kota_id' => $data['kota_id'],
            'keterangan' => 'Tambah mobil',
        ]);
        DB::table($db . '.' . $km_mobil)->where('faktur', $faktur)->update([
            'tgl' => $data['tgl'],
            'mobil_id' => $data['id'],
            'user_id' => Auth::user()->id,
            'odometer_awal' => $data['odometer'],
            'odometer_akhir' => $data['odometer'],
            'keterangan' => 'Update odometer',
        ]);

        return ['message' => 'Update mobil success', 'data' => []];
    }
}
