<?php

namespace App\Http\Controllers\Ban\update;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Pergantian extends Controller
{
    public static function run($db, $data)
    {
        $posisi = "posisi";
        $ketebalan = "ketebalan";
        $km_ban = "km_ban";
        $kondisi = "kondisi";
        $uTable = "ganti_ban";

        foreach ($data['bans'] as $k => $ban) {

            //-------------------------------------------------insert lepas---------------------------------------------------------

            DB::table($db . '.' . $posisi)->where('faktur', $ban['faktur'])->update([
                'tgl' => $data['tgl'],
                'ban_id' => $ban['lepas'],
                'user_id' => Auth::user()->id,
                'mobil_id' => 0,
                'posisi' => '',
                'keterangan' => 'Ganti Ban',
            ]);
            DB::table($db . '.' . $ketebalan)->where('faktur', $ban['faktur'])->update([
                'tgl' => $data['tgl'],
                'ban_id' => $ban['lepas'],
                'user_id' => Auth::user()->id,
                'ketebalan' => $ban['ketebalan_lepas'],
                'keterangan' => 'Ganti Ban',
            ]);
            DB::table($db . '.' . $km_ban)->where('faktur', $ban['faktur'])->update([
                'tgl' => $data['tgl'],
                'ban_id' => $ban['lepas'],
                'user_id' => Auth::user()->id,
                'odometer_awal' => $data['odometer_awal'],
                'odometer_akhir' => $data['odometer'],
                'keterangan' => 'Ganti Ban',
            ]);
            DB::table($db . '.' . $kondisi)->where('faktur', $ban['faktur'])->update([
                'tgl' => $data['tgl'],
                'ban_id' => $ban['lepas'],
                'user_id' => Auth::user()->id,
                'kondisi_id' => $ban['kondisi_lepas'],
                'keterangan' => 'Ganti Ban',
            ]);

            //-------------------------------------------------insert pasang---------------------------------------------------------

            DB::table($db . '.' . $posisi)->where('faktur', $ban['faktur'])->update([
                'tgl' => $data['tgl'],
                'ban_id' => $ban['pasang'],
                'user_id' => Auth::user()->id,
                'mobil_id' => $data['mobil_id'],
                'posisi' => $data['posisi_pasang'],
                'keterangan' => 'Ganti Ban',
            ]);
            DB::table($db . '.' . $km_ban)->where('faktur', $ban['faktur'])->update([
                'tgl' => $data['tgl'],
                'ban_id' => $ban['pasang'],
                'user_id' => Auth::user()->id,
                'odometer_awal' => $data['odometer'],
                'odometer_akhir' => $data['odometer'],
                'keterangan' => 'Ganti Ban',
            ]);
            DB::table($db . '.' . $kondisi)->where('faktur', $ban['faktur'])->update([
                'tgl' => $data['tgl'],
                'ban_id' => $ban['pasang'],
                'user_id' => Auth::user()->id,
                'kondisi_id' => 1,
                'keterangan' => 'Ganti Ban',
            ]);

            //-------------------------------------------------insert lepas---------------------------------------------------------
            DB::table($db . '.' . $uTable)->where('faktur', $ban['faktur'])->update([
                'tgl' => $data['tgl'],
                'user_id' => Auth::user()->id,
                'mobil_id' => $data['mobil_id'],
                'ban_id_lepas' => $ban['lepas'],
                'odo_lepas' => $data['odometer'],
                'kondisi_id_lepas' => $ban['kondisi_lepas'],
                'ketebalan_lepas' => $ban['ketebalan_lepas'],
                'ban_id_pasang' => $ban['pasang'],
                'posisi_pasang' => $data['posisi_pasang'],
                'odo_pasang' => $data['odometer'],
                'keterangan' => 'Ganti Ban',
            ]);

        }

        return ['message' => 'Update pergantian ban success', 'data' => []];
    }
}
