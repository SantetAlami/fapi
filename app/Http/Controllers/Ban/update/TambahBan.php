<?php

namespace App\Http\Controllers\Ban\update;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TambahBan extends Controller
{
    public static function run($db, $data)
    {
        $ban_tabel = "ban";
        $no_seri = "no_seri";
        // $km_ban = "km_ban";
        $ketebalan = "ketebalan";
        $posisi = "posisi";
        $kondisi = "kondisi";
        $lokasi = "lokasi_ban";
        $status_ban = "status_ban";

        $ban = DB::table($db . '.' . $ban_tabel)->where('id', $data['id'])->update(
            [
                'no_seri' => $data['no_seri'],
                'vendor' => $data['vendor'],
                'ukuran' => $data['ukuran'],
                'merek' => $data['merek'],
            ]
        );

        DB::table($db . '.' . $no_seri)->where('faktur', $data['faktur'])->update([
            'tgl' => $data['tgl'],
            'ban_id' => $data['id'],
            'user_id' => Auth::user()->id,
            'no_seri' => $data['no_seri'],
            'keterangan' => 'Tambah Ban',
        ]);

        DB::table($db . '.' . $ketebalan)->where('faktur', $data['faktur'])->update([
            'tgl' => $data['tgl'],
            'ban_id' => $data['id'],
            'user_id' => Auth::user()->id,
            'ketebalan' => $data['ketebalan'],
            'keterangan' => 'Tambah Ban',
        ]);

        DB::table($db . '.' . $posisi)->where('faktur', $data['faktur'])->update([
            'tgl' => $data['tgl'],
            'ban_id' => $data['id'],
            'user_id' => Auth::user()->id,
            'mobil_id' => $data['mobil_id'] ?? 0,
            'posisi' => $data['posisi'] ?? '',
            'keterangan' => 'Tambah Ban',
        ]);

        DB::table($db . '.' . $kondisi)->where('faktur', $data['faktur'])->update([
            'tgl' => $data['tgl'],
            'ban_id' => $data['id'],
            'user_id' => Auth::user()->id,
            'kondisi_id' => isset($data['mobil_id']) ? 1 : 2,
            'keterangan' => 'Tambah Ban',
        ]);

        DB::table($db . '.' . $lokasi)->where('faktur', $data['faktur'])->update([
            'tgl' => $data['tgl'],
            'ban_id' => $data['id'],
            'user_id' => Auth::user()->id,
            'kota_id' => $data['kota_id'],
            'keterangan' => 'Tambah Ban',
        ]);

        DB::table($db . '.' . $status_ban)->where('faktur', $data['faktur'])->update([
            'tgl' => $data['tgl'],
            'ban_id' => $data['id'],
            'user_id' => Auth::user()->id,
            'status_id' => $data['status_id'],
            'keterangan' => 'Tambah Ban',
        ]);

        return ['message' => 'Update ban success', 'data' => []];
    }

}
