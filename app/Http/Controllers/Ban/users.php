<?php

namespace App\Http\Controllers\Ban;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class users extends Controller
{
    public static function get($db, $pillar)
    {
        $users = DB::table($db . '.users')->where('id', Auth::user()->id)->first();

        return $users->$pillar;
    }

    public static function setLog($db, $key, $data)
    {
        $template = DB::table($db . '.log_content')->where('key', $key)->first()->content;

        foreach ($data as $index => $value) {
            $template = str_replace("[$index]", $value, $template);
        }

        DB::table($db . '.log')->insert([
            "user_id" => Auth::user()->id,
            "jenis" => $key,
            "content" => $template,
        ]);
    }
}
