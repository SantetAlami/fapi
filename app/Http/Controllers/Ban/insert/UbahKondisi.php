<?php

namespace App\Http\Controllers\Ban\insert;

use App\Http\Controllers\Ban\users;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UbahKondisi extends Controller
{

    public static function run($db, $data)
    {
        //
        $pFaktur = "UK";

        $table = 'kondisi';
        $status = 'status_ban';
        $uTable = 'ubah_kondisi';
        foreach ($data['bans'] as $ban) {
            $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

            $sebelum = DB::select("CALL $db.`sp_ban`('$ban', '$data[tgl]');")[0];

            if (in_array($sebelum->kondisi_id, [3, 5])) {
                DB::table($db . '.' . $uTable)->insert([
                    'tgl' => $data['tgl'],
                    'faktur' => $faktur,
                    'user_id' => Auth::user()->id,
                    'ban_id' => $ban,
                    'kondisi_id_sebelum' => $sebelum->kondisi_id,
                    'kondisi_id_sesudah' => $data['kondisi'] ?? $data['status'],
                    'keterangan' => 'Ubah kondisi ban',
                ]);

                $kondisi = [
                    'user_id' => Auth::user()->id,
                    'kondisi_id' => $data['kondisi'] ?? $data['status'],
                    'keterangan' => 'Ubah kondisi ban',
                ];

                switch ($sebelum->status) {
                    case 1:
                        $sts = 3;
                        break;
                    case 2:
                        $sts = 4;
                        break;
                    case 3:
                        $sts = 1;
                        break;
                    case 4:
                        $sts = 2;
                        break;
                    case 5:
                        $sts = 7;
                        break;
                    case 6:
                        $sts = 8;
                        break;
                    case 7:
                        $sts = 5;
                        break;
                    case 8:
                        $sts = 6;
                        break;

                    default:
                        break;
                }
                $_status = [
                    'user_id' => Auth::user()->id,
                    'status_id' => $sts,
                    'keterangan' => 'Ubah kondisi ban',
                ];

                if (!DB::table($db . '.' . $table)->where(['tgl' => $data['tgl'], 'ban_id' => $ban])->exists()) {
                    $kondisi['faktur'] = $faktur;
                }
                if (!DB::table($db . '.' . $status)->where(['tgl' => $data['tgl'], 'ban_id' => $ban])->exists()) {
                    $_status['faktur'] = $faktur;
                }

                DB::table($db . '.' . $table)->updateOrInsert(
                    ['tgl' => $data['tgl'], 'ban_id' => $ban],
                    $kondisi
                );
                DB::table($db . '.' . $status)->updateOrInsert(
                    ['tgl' => $data['tgl'], 'ban_id' => $ban],
                    $_status
                );

                users::setLog($db, "ubah_kondisi", [
                    "user" => Auth::user()->name,
                    "no_seri" => "",
                    "kondisi_sebelum" => DB::table($db . '.mkondisi')->where('id', $sebelum->kondisi_id)->first()->keterangan,
                    "kondisi_sesudah" => DB::table($db . '.mkondisi')->where('id', ($data['kondisi'] ?? $data['status']))->first()->keterangan,
                    "tgl_input" => $data['tgl'],
                ]);

            }

        }

        return ['message' => 'Kondisi ban terupdate', 'data' => []];
    }

}
