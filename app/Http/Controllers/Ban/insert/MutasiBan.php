<?php

namespace App\Http\Controllers\Ban\insert;

use App\Http\Controllers\Ban\users;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MutasiBan extends Controller
{

    public static function run($db, $data)
    {
        //
        $pFaktur = "PB";

        $table = 'lokasi_ban';
        $uTable = 'pindah_lokasi_ban';
        foreach ($data['bans'] as $ban) {
            if (isset($ban['faktur'])) {
                $upd = [
                    'user_id' => Auth::user()->id,
                    'ban_id' => $ban['id'],
                    'lokasi_id_sesudah' => $ban['lokasi_sesudah'] ?? $data['lokasi'],
                    'keterangan' => 'Ubah lokasi ban (Update)',
                ];
                if (isset($ban['lokasi_sebelum'])) {
                    $upd['lokasi_id_sebelum'] = $ban['lokasi_sebelum'];
                }

                DB::table($db . '.' . $uTable)->where('faktur', $ban['faktur'])->update($upd);

                DB::table($db . '.' . $table)->where('faktur', $ban['faktur'])->update([
                    'ban_id' => $ban['id'],
                    'user_id' => Auth::user()->id,
                    'kota_id' => $ban['lokasi_sesudah'] ?? $data['lokasi'],
                    'keterangan' => 'Ubah lokasi ban (Update)',
                ]);

                users::setLog($db, "update_mutasi_ban", [
                    "user" => Auth::user()->name,
                    "no_seri" => DB::table($db . '.no_seri')->where('ban_id', $ban)->orderByDesc('tgl')->first()->no_seri,
                    "lokasi_awal" => DB::table($db . '.kota')->where('id', $ban['lokasi_sebelum'])->first()->keterangan,
                    "lokasi_akhir" => DB::table($db . '.kota')->where('id', $data['lokasi'])->first()->keterangan,
                    "tgl_input" => $data['tgl'],
                ]);

            } else {
                $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

                $sebelum = DB::select("CALL $db.`sp_ban`('$ban', '$data[tgl]');")[0];

                if (in_array($sebelum->kondisi_id, [2, 3, 5])) {
                    DB::table($db . '.' . $uTable)->insert([
                        'tgl' => $data['tgl'],
                        'faktur' => $faktur,
                        'user_id' => Auth::user()->id,
                        'ban_id' => $ban,
                        'lokasi_id_sebelum' => $sebelum->lokasi_id,
                        'lokasi_id_sesudah' => $data['lokasi'],
                        'keterangan' => 'Ubah lokasi ban',
                    ]);

                    $lokasi = [
                        'user_id' => Auth::user()->id,
                        'kota_id' => $data['lokasi'],
                        'keterangan' => 'Ubah lokasi ban',
                    ];

                    if (!DB::table($db . '.' . $table)->where(['tgl' => $data['tgl'], 'ban_id' => $ban])->exists()) {
                        $lokasi['faktur'] = $faktur;
                    }

                    DB::table($db . '.' . $table)->updateOrInsert(
                        ['tgl' => $data['tgl'], 'ban_id' => $ban],
                        $lokasi
                    );

                    users::setLog($db, "mutasi_ban", [
                        "user" => Auth::user()->name,
                        "no_seri" => DB::table($db . '.no_seri')->where('ban_id', $ban)->orderByDesc('tgl')->first()->no_seri,
                        "lokasi_awal" => DB::table($db . '.kota')->where('id', $sebelum->lokasi_id)->first()->keterangan,
                        "lokasi_akhir" => DB::table($db . '.kota')->where('id', $data['lokasi'])->first()->keterangan,
                        "tgl_input" => $data['tgl'],
                    ]);
                }
            }
        }
        return ['message' => 'Lokasi ban terupdate', 'data' => []];
    }
}
