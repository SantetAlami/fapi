<?php

namespace App\Http\Controllers\Ban\insert;

use App\Http\Controllers\Ban\users;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MutasiMobil extends Controller
{

    public static function run($db, $data)
    {
        //
        $pFaktur = "PM";

        $lokasi = 'lokasi_mobil';
        $km = 'km_mobil';
        $pindah_lokasi = 'pindah_lokasi_mobil';

        if (isset($data['faktur'])) {
            DB::table($db . '.' . $pindah_lokasi)->where('faktur', $data['faktur'])->update([
                'user_id' => Auth::user()->id,
                'mobil_id' => $data['mobil_id'],
                'lokasi_id_sebelum' => $data['lokasi_sebelum'],
                'lokasi_id_sesudah' => $data['lokasi_sesudah'] ?? $data['lokasi'],
                'odometer_awal' => $data['odometer_awal'],
                'odometer_akhir' => $data['odometer_akhir'],
                'keterangan' => 'Ubah lokasi mobil (Update)',
            ]);

            DB::table($db . '.' . $km)->where('faktur', $data['faktur'])->update([
                'mobil_id' => $data['mobil_id'],
                'user_id' => Auth::user()->id,
                'odometer_awal' => $data['odometer_awal'],
                'odometer_akhir' => $data['odometer_akhir'],
                'keterangan' => 'Update odometer (Update)',
            ]);

            DB::table($db . '.' . $lokasi)->where('faktur', $data['faktur'])->update([
                'mobil_id' => $data['mobil_id'],
                'user_id' => Auth::user()->id,
                'kota_id' => $data['lokasi'],
                'keterangan' => 'Ubah lokasi mobil (Update)',
            ]);
            
            users::setLog($db, "update_mutasi_mobil", [
                "user" => Auth::user()->name,
                "no_polisi" => DB::table($db . '.mobil')->where('id', $data['mobil_id'])->first()->no_polisi,
                "lokasi_awal" => DB::table($db . '.kota')->where('id', DB::table($db . '.' . $lokasi)->where('mobil_id', $data['mobil_id'])->where('tgl', '<', $data['tgl'])->orderBy('tgl', 'desc')->first()->kota_id)->first()->keterangan,
                "lokasi_akhir" => DB::table($db . '.kota')->where('id', $data['lokasi'])->first()->keterangan,
                "tgl_input" => $data['tgl'],
            ]);
    
        } else {
            $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

            $sebelum = DB::table($db . '.' . $km)->where('mobil_id', $data['mobil_id'])->where('tgl', '<', $data['tgl'])->orderBy('tgl', 'desc')->first()->odometer_akhir;
            $lokasi_sebelum = DB::table($db . '.' . $lokasi)->where('mobil_id', $data['mobil_id'])->where('tgl', '<', $data['tgl'])->orderBy('tgl', 'desc')->first()->kota_id;

            $lokasi_ = [
                'user_id' => Auth::user()->id,
                'kota_id' => $data['lokasi'],
                'keterangan' => 'Ubah lokasi mobil',
            ];

            $km_mobil = [
                'user_id' => Auth::user()->id,
                'odometer_awal' => $data['odometer_awal'] ?? $sebelum,
                'odometer_akhir' => $data['odometer_akhir'],
                'keterangan' => 'Update odometer',
            ];

            if (!DB::table($db . '.' . $lokasi)->where(['tgl' => $data['tgl'], 'mobil_id' => $data['mobil_id']])->exists()) {
                $lokasi_['faktur'] = $faktur;
            }

            if (!DB::table($db . '.' . $km)->where(['tgl' => $data['tgl'], 'mobil_id' => $data['mobil_id']])->exists()) {
                $km_mobil['faktur'] = $faktur;
            }

            DB::table($db . '.' . $pindah_lokasi)->insert([
                'tgl' => $data['tgl'],
                'faktur' => $faktur,
                'user_id' => Auth::user()->id,
                'mobil_id' => $data['mobil_id'],
                'lokasi_id_sebelum' => $lokasi_sebelum,
                'lokasi_id_sesudah' => $data['lokasi'],
                'odometer_awal' => $data['odometer_awal'] ?? $sebelum,
                'odometer_akhir' => $data['odometer_akhir'],
                'keterangan' => 'Ubah lokasi mobil',
            ]);

            DB::table($db . '.' . $km)->updateOrInsert(
                ['tgl' => $data['tgl'], 'mobil_id' => $data['mobil_id']],
                $km_mobil
            );

            DB::table($db . '.' . $lokasi)->updateOrInsert(
                ['tgl' => $data['tgl'], 'mobil_id' => $data['mobil_id']],
                $lokasi_
            );
        }

        users::setLog($db, "mutasi_mobil", [
            "user" => Auth::user()->name,
            "no_polisi" => DB::table($db . '.mobil')->where('id', $data['mobil_id'])->first()->no_polisi,
            "lokasi_awal" => DB::table($db . '.kota')->where('id', DB::table($db . '.' . $lokasi)->where('mobil_id', $data['mobil_id'])->where('tgl', '<', $data['tgl'])->orderBy('tgl', 'desc')->first()->kota_id)->first()->keterangan,
            "lokasi_akhir" => DB::table($db . '.kota')->where('id', $data['lokasi'])->first()->keterangan,
            "tgl_input" => $data['tgl'],
        ]);

        return ['message' => 'Lokasi mobil terupdate', 'data' => []];
    }
}
