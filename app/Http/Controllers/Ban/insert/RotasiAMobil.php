<?php

namespace App\Http\Controllers\Ban\insert;

use App\Http\Controllers\Ban\users;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RotasiAMobil extends Controller
{
    public static function run($db, $data)
    {
        $pFaktur = "AM";

        $table = "posisi";
        $uTable = "rotasi_antar_mobil";

        if (!empty($data['tukar'])) {
            foreach ($data['tukar'] as $k => $ban) {
                $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

                $dari = "";
                $ke = "";
                $mobil_dari = "";
                $mobil_ke = "";
                $ban_dari = "";
                $ban_ke = "";

                foreach ($ban as $key => $item) {
                    if ($key == 0) {
                        $res1 = DB::select("CALL $db.`sp_ban`('$item', '$data[tgl]');")[0];
                        $dari = $res1->posisi;
                        $mobil_dari = $res1->mobil_id;
                    }
                    if ($key == 1) {
                        $res2 = DB::select("CALL $db.`sp_ban`('$item', '$data[tgl]');")[0];
                        $ke = $res2->posisi;
                        $mobil_ke = $res2->mobil_id;
                    }
                }

                foreach ($ban as $index => $item) {
                    $posisi = [
                        'user_id' => Auth::user()->id,
                        'mobil_id' => ($index == 0) ? $mobil_ke : $mobil_dari,
                        'posisi' => ($index == 0) ? $ke : $dari,
                        'keterangan' => 'Rotasi Antar Mobil',
                    ];

                    $ban_dari = ($index == 0) ? $item : $ban_dari;
                    $ban_ke = ($index == 1) ? $item : $ban_ke;

                    if (!DB::table($db . '.' . $table)->where(['tgl' => $data['tgl'], 'ban_id' => $item])->exists()) {
                        $posisi['faktur'] = $faktur;
                    }

                    DB::table($db . '.' . $table)->updateOrInsert(
                        ['tgl' => $data['tgl'], 'ban_id' => $item],
                        $posisi
                    );
                }

                DB::table($db . '.' . $uTable)->insert([
                    'tgl' => $data['tgl'],
                    'faktur' => $faktur,
                    'user_id' => Auth::user()->id,
                    'mobil_id_dari' => $mobil_dari,
                    'ban_id_dari' => $ban_dari,
                    'posisi_dari' => $dari,
                    'mobil_id_ke' => $mobil_ke,
                    'ban_id_ke' => $ban_ke,
                    'posisi_ke' => $ke,
                    'keterangan' => 'Rotasi Antar Mobil',
                ]);

                users::setLog($db, "rotasi_antar_mobil", [
                    "user" => Auth::user()->name,
                    "no_polisi_1" => DB::table($db . '.mobil')->where('id', $mobil_dari)->first()->no_polisi . " No Seri: " . DB::table($db . '.no_seri')->where('ban_id', $ban_dari)->orderByDesc('tgl')->first()->no_seri . " (Posisi : $dari)",
                    "no_polisi_2" => DB::table($db . '.mobil')->where('id', $mobil_ke)->first()->no_polisi . " No Seri: " . DB::table($db . '.no_seri')->where('ban_id', $ban_ke)->orderByDesc('tgl')->first()->no_seri . " (Posisi : $ke)",
                    "tgl_input" => $data['tgl'],
                ]);

            }
        }

        if (!empty($data['pindah'])) {
            foreach ($data['pindah'] as $k => $ban) {
                $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

                $dari = "";
                $ke = (substr($ban['posisi'], 0, 1) === '0') ? str_replace('0', '', $ban['posisi']) : $ban['posisi'];
                $mobil_dari = "";
                $mobil_ke = $ban['mobil_ke'];
                $ban_dari = "";
                $ban_ke = 0;

                $res = DB::select("CALL $db.`sp_ban`('$ban[ban_id]', '$data[tgl]');")[0];
                $dari = $res->posisi;
                $ban_dari = $res->ban_id;
                $mobil_dari = $res->mobil_id;

                $posisi = [
                    'user_id' => Auth::user()->id,
                    'mobil_id' => $mobil_ke,
                    'posisi' => $ke,
                    'keterangan' => 'Rotasi Antar Mobil (pindah dari mobil)',
                ];

                if (!DB::table($db . '.' . $table)->where(['tgl' => $data['tgl'], 'ban_id' => $ban['ban_id']])->exists()) {
                    $posisi['faktur'] = $faktur;
                }

                DB::table($db . '.' . $table)->updateOrInsert(
                    ['tgl' => $data['tgl'], 'ban_id' => $ban['ban_id']],
                    $posisi
                );

                DB::table($db . '.' . $uTable)->insert([
                    'tgl' => $data['tgl'],
                    'faktur' => $faktur,
                    'user_id' => Auth::user()->id,
                    'mobil_id_dari' => $mobil_dari,
                    'ban_id_dari' => $ban_dari,
                    'posisi_dari' => $dari,
                    'mobil_id_ke' => $mobil_ke,
                    'ban_id_ke' => $ban_ke,
                    'posisi_ke' => $ke,
                    'keterangan' => 'Rotasi Antar Mobil (pindah dari mobil)',
                ]);

                users::setLog($db, "pindah_dari_mobil", [
                    "user" => Auth::user()->name,
                    "no_polisi_1" => (DB::table($db . '.mobil')->where('id', $mobil_dari)->first()->no_polisi ?? '') . " No Seri: " . (DB::table($db . '.no_seri')->where('ban_id', $ban_dari)->orderByDesc('tgl')->first()->no_seri ?? '') . " (Posisi : $dari)",
                    "no_polisi_2" => (DB::table($db . '.mobil')->where('id', $mobil_ke)->first()->no_polisi ?? '') . " (Posisi : $ke)",
                    "tgl_input" => $data['tgl'],
                ]);

            }

        }

        return ['message' => 'Rotasi ban antar mobil berhasil', 'data' => []];
    }

}
