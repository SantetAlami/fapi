<?php

namespace App\Http\Controllers\Ban\insert;

use App\Http\Controllers\Ban\users;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HapusFaktur extends Controller
{

    public static function run($db, $data)
    {
        //
        $trx = [
            "CK" => "Ketebalan",
            "KM" => "KM Harian",
            "PB" => "Mutasi Ban",
            "PM" => "Mutasi Mobil",
            "PS" => "Pasang",
            "GB" => "Pergantian",
            "RB" => "Rotasi",
            "AM" => "Rotasi Antar Mobil",
            "NT" => "SJ Ban Baru",
            "BK" => "SJ Ban Keluar",
            "VT" => "SJ Ban Masuk",
            "TB" => "Tambah Ban",
            "TM" => "Tambah Mobil",
            "UK" => "Ubah Kondisi",
            "VF" => "Verifikasi",
        ];

        $tables = DB::select("SELECT
            TABLE_NAME conn
            FROM information_schema.COLUMNS
            WHERE TABLE_SCHEMA = 'sewa_ban'
            AND COLUMN_NAME ='faktur' AND SUBSTR(TABLE_NAME, 1, 2) <> 'v_'
        ");

        $maxTgl = "0000-00-00 00:00";
        foreach ($tables as $table) {
            $reqId = DB::table($db . '.' . $table->conn)->where('faktur', $data['faktur'])->first();
            if ($reqId) {
                $id = $reqId->ban_id ?? $reqId->mobil_id ?? $reqId->id;
                $key = !isset($reqId->mobil_id) ? (isset($reqId->ban_id) ? 'ban_id' : 'id') : 'mobil_id';
                $req = DB::table($db . '.' . $table->conn)->where($key, $id)->orderByDesc('tgl')->orderByDesc(isset($reqId->updated_at) ? 'updated_at' : 'created_at')->first();

                if ($maxTgl < ($req->tgl . ' ' . substr($req->updated_at ?? $req->created_at, 11, 6))) {
                    $maxTgl = ($req->tgl . ' ' . substr($req->updated_at ?? $req->created_at, 11, 6));
                }

                // if ($table->conn == 'pindah_lokasi_mobil') {
                //     dd([$maxTgl > (($reqId->tgl . ' ' . substr($reqId->updated_at ?? $reqId->created_at, 11, 6)) ?? "0000-00-00 00:00:00"), $maxTgl , (($reqId->tgl . ' ' . substr($reqId->updated_at ?? $reqId->created_at, 11, 6)) ?? "0000-00-00 00:00:00")]);
                // }
                if ($maxTgl > (($reqId->tgl . ' ' . substr($reqId->updated_at ?? $reqId->created_at, 11, 6)) ?? "0000-00-00 00:00:00")) {
                    return ['error' => 'error', 'message' => 'Tidak bisa di hapus', 'data' => []];
                }
            }
        }

        users::setLog($db, "hapus_faktur", [
            "user" => Auth::user()->name,
            "faktur" => $data['faktur'],
            "trx" => $trx[substr($data['faktur'], "0", "2")],
        ]);

        DB::select("CALL $db.sp_delete_by_faktur(?)", [$data['faktur']]);

        return ['message' => 'Data faktur terhapus', 'data' => []];

    }

}
