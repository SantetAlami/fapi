<?php

namespace App\Http\Controllers\Ban\insert;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Ban\users;

class Pergantian extends Controller
{
    public static function run($db, $data)
    {
        $pFaktur = "GB";

        $posisi = "posisi";
        $ketebalan = "ketebalan";
        $km_ban = "km_ban";
        $kondisi = "kondisi";
        $uTable = "ganti_ban";
        $riwayat = "riwayat";

        $mobil = DB::table($db . '.mobil')->where('id', $data['mobil_id'])->first();

        foreach ($data['bans'] as $k => $ban) {
            $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

            $lepas = DB::select("CALL $db.`sp_ban`('$ban[lepas]', '$data[tgl]');")[0];
            $pasang = DB::select("CALL $db.`sp_ban`('$ban[pasang]', '$data[tgl]');")[0];

            if ($lepas->posisi == '') {
                return ['error' => 'error', 'message' => 'Ups', 'data' => $lepas];
            }
            //-------------------------------------------------insert lepas---------------------------------------------------------
            $posisi_lepas = [
                'user_id' => Auth::user()->id,
                'mobil_id' => 0,
                'posisi' => '',
                'keterangan' => 'Ganti Ban',
            ];
            $ketebalan_lepas = [
                'user_id' => Auth::user()->id,
                'ketebalan' => $ban['ketebalan_lepas'],
                'keterangan' => 'Ganti Ban',
            ];
            $km_ban_lepas = [
                'user_id' => Auth::user()->id,
                'odometer_awal' => $lepas->odometer_akhir,
                'odometer_akhir' => $data['odometer'],
                'keterangan' => 'Ganti Ban',
            ];
            $kondisi_lepas = [
                'user_id' => Auth::user()->id,
                'kondisi_id' => $ban['kondisi_lepas'],
                'keterangan' => 'Ganti Ban',
            ];
            $riwayat_lepas = [
                'user_id' => Auth::user()->id,
                'faktur_lepas' => $faktur,
                'tgl_lepas' => $data['tgl'],
                'km_tempuh' => $data['odometer'],
                'ketebalan_lepas' => $ban['ketebalan_lepas'],
                'keterangan' => $data['keterangan'],
                'tindakan' => $ban['kondisi_lepas'],
                'lokasi_lepas_id' => $lepas->lokasi_id,
                'lokasi_lepas' => DB::table($db . '.kota')->where('id', $lepas->lokasi_id)->first()->keterangan,
            ];

            if (!DB::table($db . '.' . $posisi)->where(['tgl' => $data['tgl'], 'ban_id' => $ban['lepas']])->exists()) {
                $posisi_lepas['faktur'] = $faktur;
            }
            if (!DB::table($db . '.' . $ketebalan)->where(['tgl' => $data['tgl'], 'ban_id' => $ban['lepas']])->exists()) {
                $ketebalan_lepas['faktur'] = $faktur;
            }
            if (!DB::table($db . '.' . $km_ban)->where(['tgl' => $data['tgl'], 'ban_id' => $ban['lepas']])->exists()) {
                $km_ban_lepas['faktur'] = $faktur;
            }
            if (!DB::table($db . '.' . $kondisi)->where(['tgl' => $data['tgl'], 'ban_id' => $ban['lepas']])->exists()) {
                $kondisi_lepas['faktur'] = $faktur;
            }

            DB::table($db . '.' . $posisi)->updateOrInsert(
                ['tgl' => $data['tgl'], 'ban_id' => $ban['lepas']],
                $posisi_lepas
            );
            DB::table($db . '.' . $ketebalan)->updateOrInsert(
                ['tgl' => $data['tgl'], 'ban_id' => $ban['lepas']],
                $ketebalan_lepas
            );
            DB::table($db . '.' . $km_ban)->updateOrInsert(
                ['tgl' => $data['tgl'], 'ban_id' => $ban['lepas']],
                $km_ban_lepas
            );
            DB::table($db . '.' . $kondisi)->updateOrInsert(
                ['tgl' => $data['tgl'], 'ban_id' => $ban['lepas']],
                $kondisi_lepas
            );
            DB::table($db . '.' . $riwayat)->where(['ban_id' => $ban['lepas'], 'status_id' => $lepas->status_id])->update(
                $riwayat_lepas
            );

            //-------------------------------------------------insert pasang---------------------------------------------------------

            $posisi_pasang = [
                'user_id' => Auth::user()->id,
                'mobil_id' => $data['mobil_id'],
                'posisi' => $lepas->posisi,
                'keterangan' => 'Ganti Ban',
            ];
            $ketebalan_pasang = [
                'user_id' => Auth::user()->id,
                'ketebalan' => $pasang->ketebalan,
                'keterangan' => 'Ganti Ban',
            ];
            $km_ban_pasang = [
                'user_id' => Auth::user()->id,
                'odometer_awal' => $data['odometer'],
                'odometer_akhir' => $data['odometer'],
                'keterangan' => 'Ganti Ban',
            ];
            $kondisi_pasang = [
                'user_id' => Auth::user()->id,
                'kondisi_id' => 1,
                'keterangan' => 'Ganti Ban',
            ];
            $riwayat_pasang = [
                'user_id' => Auth::user()->id,
                'faktur_pasang' => $faktur,
                'tgl_pasang' => $data['tgl'],
                'no_polisi_pasang' => $data['mobil_id'],
                'km_pasang' => $data['odometer'],
                'kapasitas' => $mobil->kapasitas,
                'posisi' => $lepas->posisi,
                'lokasi_pasang_id' => $lepas->lokasi_id,
                'lokasi_pasang' => DB::table($db . '.kota')->where('id', $lepas->lokasi_id)->first()->keterangan,
            ];

            if (!DB::table($db . '.' . $posisi)->where(['tgl' => $data['tgl'], 'ban_id' => $ban['pasang']])->exists()) {
                $posisi_pasang['faktur'] = $faktur;
            }
            if (!DB::table($db . '.' . $ketebalan)->where(['tgl' => $data['tgl'], 'ban_id' => $ban['pasang']])->exists()) {
                $ketebalan_pasang['faktur'] = $faktur;
            }
            if (!DB::table($db . '.' . $km_ban)->where(['tgl' => $data['tgl'], 'ban_id' => $ban['pasang']])->exists()) {
                $km_ban_pasang['faktur'] = $faktur;
            }
            if (!DB::table($db . '.' . $kondisi)->where(['tgl' => $data['tgl'], 'ban_id' => $ban['pasang']])->exists()) {
                $kondisi_pasang['faktur'] = $faktur;
            }
            if (DB::table($db . '.' . $riwayat)->where(['ban_id' => $ban['pasang'], 'status_id' => $pasang->status_id])->whereNull('faktur_pasang')->exists()) {
                DB::table($db . '.' . $riwayat)->where(['ban_id' => $ban['pasang'], 'status_id' => $pasang->status_id])->update(
                    $riwayat_pasang
                );
            }

            DB::table($db . '.' . $posisi)->updateOrInsert(
                ['tgl' => $data['tgl'], 'ban_id' => $ban['pasang']],
                $posisi_pasang
            );
            DB::table($db . '.' . $ketebalan)->updateOrInsert(
                ['tgl' => $data['tgl'], 'ban_id' => $ban['pasang']],
                $ketebalan_pasang
            );
            DB::table($db . '.' . $km_ban)->updateOrInsert(
                ['tgl' => $data['tgl'], 'ban_id' => $ban['pasang']],
                $km_ban_pasang
            );
            DB::table($db . '.' . $kondisi)->updateOrInsert(
                ['tgl' => $data['tgl'], 'ban_id' => $ban['pasang']],
                $kondisi_pasang
            );

            //-------------------------------------------------insert lepas---------------------------------------------------------
            DB::table($db . '.' . $uTable)->insert([
                'tgl' => $data['tgl'],
                'faktur' => $faktur,
                'user_id' => Auth::user()->id,
                'mobil_id' => $data['mobil_id'],
                'ban_id_lepas' => $ban['lepas'],
                'odo_lepas' => $data['odometer'],
                'kondisi_id_lepas' => $ban['kondisi_lepas'],
                'ketebalan_lepas' => $ban['ketebalan_lepas'],
                'ban_id_pasang' => $ban['pasang'],
                'posisi_pasang' => $lepas->posisi,
                'odo_pasang' => $data['odometer'],
                'kota_id' => $lepas->lokasi_id,
                'keterangan' => 'Ganti Ban',
            ]);

            users::setLog($db, "pasang", [
                "user" => Auth::user()->name,
                "no_seri_pasang" => (DB::table($db . '.no_seri')->where('ban_id', $ban['pasang'])->orderByDesc('tgl')->first()->no_seri) . "di posisi" . $lepas->posisi,
                "no_seri_lepas" => DB::table($db . '.no_seri')->where('ban_id', $ban['lepas'])->orderByDesc('tgl')->first()->no_seri,
                "tgl_input" => $data['tgl'],
            ]);

        }

        return ['message' => 'Pergantian ban success', 'data' => []];
    }

}
