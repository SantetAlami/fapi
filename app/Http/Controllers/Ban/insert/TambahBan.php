<?php

namespace App\Http\Controllers\Ban\insert;

use App\Http\Controllers\Ban\users;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TambahBan extends Controller
{
    public static function run($db, $data)
    {
        $pFaktur = "TB";

        $ban_tabel = "ban";
        $no_seri = "no_seri";
        $km_ban = "km_ban";
        $riwayat = "riwayat";
        $ketebalan = "ketebalan";
        $posisi = "posisi";
        $kondisi = "kondisi";
        $lokasi = "lokasi_ban";
        $status_ban = "status_ban";

        if (!DB::table($db . '.' . $ban_tabel)->where('no_seri', $data['no_seri'])->exists()) {
            if (isset($data['faktur'])) {
                $ban = DB::table($db . '.' . $ban_tabel)->where('id', $data['id'])->update(
                    [
                        'no_seri' => $data['no_seri'],
                        'vendor' => $data['vendor'],
                        'ukuran' => $data['ukuran'],
                        'merek' => $data['merek'],
                    ]
                );

                DB::table($db . '.' . $riwayat)->where('faktur_verifikasi', $data['faktur'])->where('ban_id', $data['id'])->update([
                    'user_id' => Auth::user()->id,
                    'lokasi_datang' => DB::table($db . '.kota')->where('id', $data['kota_id'])->first()->keterangan,
                    'no_seri' => $data['no_seri'],
                    'vendor' => $data['vendor'],
                    'ukuran' => $data['ukuran'],
                    'merek' => $data['merek'],
                    'status' => DB::table($db . '.mstatus')->where('id', $data['status_id'])->first()->keterangan,
                    'ketebalan' => $data['ketebalan'],
                ]);

                DB::table($db . '.' . $no_seri)->where('faktur', $data['faktur'])->where('ban_id', $data['id'])->update([
                    'user_id' => Auth::user()->id,
                    'no_seri' => $data['no_seri'],
                    'keterangan' => 'Tambah Ban (update)',
                ]);

                DB::table($db . '.' . $ketebalan)->where('faktur', $data['faktur'])->where('ban_id', $data['id'])->update([
                    'user_id' => Auth::user()->id,
                    'ketebalan' => $data['ketebalan'],
                    'keterangan' => 'Tambah Ban (update)',
                ]);

                DB::table($db . '.' . $posisi)->where('faktur', $data['faktur'])->where('ban_id', $data['id'])->update([
                    'user_id' => Auth::user()->id,
                    'mobil_id' => $data['mobil_id'] ?? 0,
                    'posisi' => $data['posisi'] ?? '',
                    'keterangan' => 'Tambah Ban (update)',
                ]);

                DB::table($db . '.' . $kondisi)->where('faktur', $data['faktur'])->where('ban_id', $data['id'])->update([
                    'user_id' => Auth::user()->id,
                    'kondisi_id' => isset($data['mobil_id']) ? 1 : 2,
                    'keterangan' => 'Tambah Ban (update)',
                ]);

                DB::table($db . '.' . $lokasi)->where('faktur', $data['faktur'])->where('ban_id', $data['id'])->update([
                    'user_id' => Auth::user()->id,
                    'kota_id' => $data['kota_id'],
                    'keterangan' => 'Tambah Ban (update)',
                ]);

                DB::table($db . '.' . $status_ban)->where('faktur', $data['faktur'])->where('ban_id', $data['id'])->update([
                    'user_id' => Auth::user()->id,
                    'status_id' => $data['status_id'],
                    'keterangan' => 'Tambah Ban (update)',
                ]);

                users::setLog($db, "update_ban", [
                    "user" => Auth::user()->name,
                    "no_seri" => $data['no_seri'],
                ]);

                return ['message' => 'Update ban success', 'data' => []];

            } else {
                $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

                $ban = DB::table($db . '.' . $ban_tabel)->insertGetId(
                    [
                        'faktur' => $faktur,
                        'no_seri' => $data['no_seri'],
                        'vendor' => $data['vendor'],
                        'ukuran' => $data['ukuran'],
                        'merek' => $data['merek'],
                    ]
                );

                $riwayat_ = [
                    'user_id' => Auth::user()->id,
                    'ban_id' => $ban,
                    'tgl_masuk' => $data['tgl'],
                    'faktur_verifikasi' => $faktur,
                    'lokasi_datang_id' => $data['kota_id'],
                    'lokasi_datang' => DB::table($db . '.kota')->where('id', $data['kota_id'])->first()->keterangan,
                    'no_seri' => $data['no_seri'],
                    'vendor' => $data['vendor'],
                    'ukuran' => $data['ukuran'],
                    'merek' => $data['merek'],
                    'status_id' => $data['status_id'],
                    'status' => DB::table($db . '.mstatus')->where('id', $data['status_id'])->first()->keterangan,
                    'ketebalan' => $data['ketebalan'],
                ];
                $no_seri_ = [
                    'user_id' => Auth::user()->id,
                    'no_seri' => $data['no_seri'],
                    'keterangan' => 'Tambah Ban',
                ];
                $ketebalan_ = [
                    'user_id' => Auth::user()->id,
                    'ketebalan' => $data['ketebalan'],
                    'keterangan' => 'Tambah Ban',
                ];
                $posisi_ = [
                    'user_id' => Auth::user()->id,
                    'mobil_id' => $data['mobil_id'] ?? 0,
                    'posisi' => $data['posisi'] ?? '',
                    'keterangan' => 'Tambah Ban',
                ];
                $kondisi_ = [
                    'user_id' => Auth::user()->id,
                    'kondisi_id' => isset($data['mobil_id']) ? 1 : 2,
                    'keterangan' => 'Tambah Ban',
                ];
                $lokasi_ = [
                    'user_id' => Auth::user()->id,
                    'kota_id' => $data['kota_id'],
                    'keterangan' => 'Tambah Ban',
                ];
                $status_ban_ = [
                    'user_id' => Auth::user()->id,
                    'status_id' => $data['status_id'],
                    'keterangan' => 'Tambah Ban',
                ];

                // if (!DB::table($db . '.' . $riwayat)->where(['ban_id' => $ban, 'status' => $data['status_id']])->exists()) {
                //     $riwayat_['faktur_verifikasi'] = $faktur;
                // }
                if (!DB::table($db . '.' . $no_seri)->where(['tgl' => $data['tgl'], 'ban_id' => $ban])->exists()) {
                    $no_seri_['faktur'] = $faktur;
                }
                if (!DB::table($db . '.' . $ketebalan)->where(['tgl' => $data['tgl'], 'ban_id' => $ban])->exists()) {
                    $ketebalan_['faktur'] = $faktur;
                }
                if (!DB::table($db . '.' . $posisi)->where(['tgl' => $data['tgl'], 'ban_id' => $ban])->exists()) {
                    $posisi_['faktur'] = $faktur;
                }
                if (!DB::table($db . '.' . $kondisi)->where(['tgl' => $data['tgl'], 'ban_id' => $ban])->exists()) {
                    $kondisi_['faktur'] = $faktur;
                }
                if (!DB::table($db . '.' . $lokasi)->where(['tgl' => $data['tgl'], 'ban_id' => $ban])->exists()) {
                    $lokasi_['faktur'] = $faktur;
                }
                if (!DB::table($db . '.' . $status_ban)->where(['tgl' => $data['tgl'], 'ban_id' => $ban])->exists()) {
                    $status_ban_['faktur'] = $faktur;
                }

                if (isset($data['mobil_id'])) {
                    $km_ban_ = [
                        'user_id' => Auth::user()->id,
                        'odometer_awal' => $data['odometer_awal'] ?? 0,
                        'odometer_akhir' => $data['odometer_akhir'] ?? 0,
                        'keterangan' => 'Tambah Ban',
                    ];
                    if (!DB::table($db . '.' . $km_ban)->where(['tgl' => $data['tgl'], 'ban_id' => $ban])->exists()) {
                        $km_ban_['faktur'] = $faktur;
                    }
                    DB::table($db . '.' . $km_ban)->updateOrInsert(
                        ['tgl' => $data['tgl'], 'ban_id' => $ban],
                        $km_ban_
                    );
                }
                DB::table($db . '.' . $riwayat)->insertOrIgnore(
                    $riwayat_
                );
                DB::table($db . '.' . $no_seri)->updateOrInsert(
                    ['tgl' => $data['tgl'], 'ban_id' => $ban],
                    $no_seri_
                );
                DB::table($db . '.' . $ketebalan)->updateOrInsert(
                    ['tgl' => $data['tgl'], 'ban_id' => $ban],
                    $ketebalan_
                );
                DB::table($db . '.' . $posisi)->updateOrInsert(
                    ['tgl' => $data['tgl'], 'ban_id' => $ban],
                    $posisi_
                );
                DB::table($db . '.' . $kondisi)->updateOrInsert(
                    ['tgl' => $data['tgl'], 'ban_id' => $ban],
                    $kondisi_
                );
                DB::table($db . '.' . $lokasi)->updateOrInsert(
                    ['tgl' => $data['tgl'], 'ban_id' => $ban],
                    $lokasi_
                );
                DB::table($db . '.' . $status_ban)->updateOrInsert(
                    ['tgl' => $data['tgl'], 'ban_id' => $ban],
                    $status_ban_
                );

                users::setLog($db, "tambah_ban", [
                    "user" => Auth::user()->name,
                    "no_seri" => $data['no_seri'],
                    "tgl_input" => $data['tgl'],
                ]);

                return ['message' => 'Tambah ban success', 'data' => []];
            }

        } else {
                return ['error' => 'error', 'message' => 'Duplicate no seri', 'data' => []];
        }

    }

}
