<?php

namespace App\Http\Controllers\Ban\insert;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Ban\users;

class Pasang extends Controller
{
    public static function run($db, $data)
    {
        $pFaktur = "PS";

        $posisi = "posisi";
        $km_ban = "km_ban";
        $kondisi = "kondisi";
        $uTable = "ganti_ban";
        $riwayat = "riwayat";

        $mobil = DB::table($db . '.mobil')->where('id', $data['mobil_id'])->first();

        foreach ($data['bans'] as $k => $ban) {
            $pasang = DB::select("CALL $db.`sp_ban`('$ban[id]', '$data[tgl]');")[0];

            if (isset($ban['faktur'])) {
                DB::table($db . '.' . $posisi)->where('faktur', $ban['faktur'])->update([
                    'ban_id' => $ban['id'],
                    'user_id' => Auth::user()->id,
                    'mobil_id' => $data['mobil_id'],
                    'posisi' => $ban['posisi'],
                    'keterangan' => 'Pasang Ban',
                ]);
                DB::table($db . '.' . $km_ban)->where('faktur', $ban['faktur'])->update([
                    'ban_id' => $ban['id'],
                    'user_id' => Auth::user()->id,
                    'odometer_awal' => $data['odometer'],
                    'odometer_akhir' => $data['odometer'],
                    'keterangan' => 'Pasang Ban',
                ]);
                DB::table($db . '.' . $kondisi)->where('faktur', $ban['faktur'])->update([
                    'ban_id' => $ban['id'],
                    'user_id' => Auth::user()->id,
                    'kondisi_id' => 1,
                    'keterangan' => 'Pasang Ban',
                ]);
                DB::table($db . '.' . $kondisi)->where('faktur_pasang', $ban['faktur'])->update([
                    'ban_id' => $ban['id'],
                    'user_id' => Auth::user()->id,
                    'tgl_pasang' => $data['tgl'],
                    'no_polisi_pasang' => $data['mobil_id'],
                    'km_pasang' => $data['odometer'],
                    'kapasitas' => $mobil->kapasitas,
                    'posisi' => $ban['posisi'],
                    'lokasi_pasang' => $pasang->lokasi_id,
                ]);
                DB::table($db . '.' . $uTable)->where('faktur', $ban['faktur'])->update([
                    'user_id' => Auth::user()->id,
                    'mobil_id' => $data['mobil_id'],
                    'ban_id_lepas' => null,
                    'odo_lepas' => null,
                    'kondisi_id_lepas' => null,
                    'ketebalan_lepas' => null,
                    'ban_id_pasang' => $ban['id'],
                    'posisi_pasang' => $ban['posisi'],
                    'odo_pasang' => $data['odometer'],
                    'keterangan' => 'Pasang Ban',
                ]);

                users::setLog($db, "update_pasang", [
                    "user" => Auth::user()->name,
                    "no_seri" => DB::table($db . '.no_seri')->where('ban_id', $ban['id'])->orderByDesc('tgl')->first()->no_seri,
                    "no_polisi" => DB::table($db . '.mobil')->where('id', $data['mobil_id'])->first()->no_polisi,
                    "tgl_input" => $data['tgl'],
                ]);
                
            } else {
                $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

                $terpasang = collect(DB::select('CALL ' . $db . '.sp_posisi(\'' . $data['mobil_id'] . '\',\'' . $data['tgl'] . '\');'));

                if ($terpasang->where('posisi', $ban['posisi'])->count() >= 1) {
                    return ['error' => 'error', 'message' => 'Tidak bisa memasang di posisi yang telah dipasang (duplikat)', 'data' => []];
                }
                //-------------------------------------------------insert pasang---------------------------------------------------------

                $posisi_pasang = [
                    'user_id' => Auth::user()->id,
                    'mobil_id' => $data['mobil_id'],
                    'posisi' => $ban['posisi'],
                    'keterangan' => 'Pasang Ban',
                ];
                $km_ban_pasang = [
                    'user_id' => Auth::user()->id,
                    'odometer_awal' => $data['odometer'],
                    'odometer_akhir' => $data['odometer'],
                    'keterangan' => 'Pasang Ban',
                ];
                $kondisi_pasang = [
                    'user_id' => Auth::user()->id,
                    'kondisi_id' => 1,
                    'keterangan' => 'Pasang Ban',
                ];
                $riwayat_pasang = [
                    'user_id' => Auth::user()->id,
                    'faktur_pasang' => $faktur,
                    'tgl_pasang' => $data['tgl'],
                    'no_polisi_pasang' => $data['mobil_id'],
                    'km_pasang' => $data['odometer'],
                    'kapasitas' => $mobil->kapasitas,
                    'posisi' => $ban['posisi'],
                    'lokasi_pasang_id' => $pasang->lokasi_id,
                    'lokasi_pasang' => DB::table($db . '.kota')->where('id', $pasang->lokasi_id)->first()->keterangan,
                ];
    

                if (!DB::table($db . '.' . $posisi)->where(['tgl' => $data['tgl'], 'ban_id' => $ban['id']])->exists()) {
                    $posisi_pasang['faktur'] = $faktur;
                }
                if (!DB::table($db . '.' . $km_ban)->where(['tgl' => $data['tgl'], 'ban_id' => $ban['id']])->exists()) {
                    $km_ban_pasang['faktur'] = $faktur;
                }
                if (!DB::table($db . '.' . $kondisi)->where(['tgl' => $data['tgl'], 'ban_id' => $ban['id']])->exists()) {
                    $kondisi_pasang['faktur'] = $faktur;
                }
                if (DB::table($db . '.' . $riwayat)->where(['ban_id' => $ban['id'], 'status_id' => $pasang->status_id])->whereNull('faktur_pasang')->exists()) {
                    DB::table($db . '.' . $riwayat)->where(['ban_id' => $ban['id'], 'status_id' => $pasang->status_id])->update(
                        $riwayat_pasang
                    );
                }    

                DB::table($db . '.' . $posisi)->updateOrInsert(
                    ['tgl' => $data['tgl'], 'ban_id' => $ban['id']],
                    $posisi_pasang
                );
                DB::table($db . '.' . $km_ban)->updateOrInsert(
                    ['tgl' => $data['tgl'], 'ban_id' => $ban['id']],
                    $km_ban_pasang
                );
                DB::table($db . '.' . $kondisi)->updateOrInsert(
                    ['tgl' => $data['tgl'], 'ban_id' => $ban['id']],
                    $kondisi_pasang
                );

                //-------------------------------------------------insert lepas---------------------------------------------------------
                DB::table($db . '.' . $uTable)->insert([
                    'tgl' => $data['tgl'],
                    'faktur' => $faktur,
                    'user_id' => Auth::user()->id,
                    'mobil_id' => $data['mobil_id'],
                    'ban_id_lepas' => null,
                    'odo_lepas' => null,
                    'kondisi_id_lepas' => null,
                    'ketebalan_lepas' => null,
                    'ban_id_pasang' => $ban['id'],
                    'posisi_pasang' => $ban['posisi'],
                    'odo_pasang' => $data['odometer'],
                    'kota_id' => $pasang->lokasi_id,
                    'keterangan' => 'Pasang Ban',
                ]);

                users::setLog($db, "pasang", [
                    "user" => Auth::user()->name,
                    "no_seri" => DB::table($db . '.no_seri')->where('ban_id', $ban['id'])->orderByDesc('tgl')->first()->no_seri,
                    "no_polisi" => DB::table($db . '.mobil')->where('id', $data['mobil_id'])->first()->no_polisi,
                    "tgl_input" => $data['tgl'],
                ]);    
        
            }

        }

        return ['message' => 'Pasang ban success', 'data' => []];
    }

}
