<?php

namespace App\Http\Controllers\Ban\insert;

use App\Http\Controllers\Ban\users;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TambahMobil extends Controller
{
    public static function run($db, $data)
    {
        $pFaktur = "TM";

        $mobil_tabel = "mobil";
        $lokasi = "lokasi_mobil";
        $km_mobil = "km_mobil";

        if (isset($data['faktur'])) {

            $faktur = $data['faktur'];

            $mobil = DB::table($db . '.' . $mobil_tabel)->where('id', $data['id'])->update(
                [
                    'no_polisi' => $data['no_polisi'],
                    'kapasitas' => $data['kapasitas'],
                    'jumlah_ban' => $data['jumlah_ban'],
                    'jumlah_serep' => $data['jumlah_serep'],
                ]
            );

            DB::table($db . '.' . $lokasi)->where('faktur', $faktur)->update([
                'mobil_id' => $data['id'],
                'user_id' => Auth::user()->id,
                'kota_id' => $data['kota_id'],
                'keterangan' => 'Tambah mobil',
            ]);
            DB::table($db . '.' . $km_mobil)->where('faktur', $faktur)->update([
                'mobil_id' => $data['id'],
                'user_id' => Auth::user()->id,
                'odometer_awal' => $data['odometer'],
                'odometer_akhir' => $data['odometer'],
                'keterangan' => 'Update odometer',
            ]);

            users::setLog($db, "update_mobil", [
                "user" => Auth::user()->name,
                "no_polisi" => $data['no_polisi'],
            ]);

            return ['message' => 'Update mobil success', 'data' => []];

        } else {

            $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

            $mobil = DB::table($db . '.' . $mobil_tabel)->insertGetId(
                [
                    'faktur' => $faktur,
                    'no_polisi' => $data['no_polisi'],
                    'kapasitas' => $data['kapasitas'],
                    'jumlah_ban' => $data['jumlah_ban'],
                    'jumlah_serep' => $data['jumlah_serep'],
                ]
            );

            $lokasi_ = [
                'user_id' => Auth::user()->id,
                'kota_id' => $data['kota_id'],
                'keterangan' => 'Tambah mobil',
            ];
            $km_mobil_ = [
                'user_id' => Auth::user()->id,
                'odometer_awal' => $data['odometer'],
                'odometer_akhir' => $data['odometer'],
                'keterangan' => 'Update odometer',
            ];

            if (!DB::table($db . '.' . $lokasi)->where(['tgl' => $data['tgl'], 'mobil_id' => $mobil])->exists()) {
                $lokasi_['faktur'] = $faktur;
            }
            if (!DB::table($db . '.' . $km_mobil)->where(['tgl' => $data['tgl'], 'mobil_id' => $mobil])->exists()) {
                $km_mobil_['faktur'] = $faktur;
            }

            DB::table($db . '.' . $lokasi)->updateOrInsert(
                ['tgl' => $data['tgl'], 'mobil_id' => $mobil],
                $lokasi_
            );
            DB::table($db . '.' . $km_mobil)->updateOrInsert(
                ['tgl' => $data['tgl'], 'mobil_id' => $mobil],
                $km_mobil_
            );

            users::setLog($db, "tambah_mobil", [
                "user" => Auth::user()->name,
                "no_polisi" => $data['no_polisi'],
                "tgl_input" => $data['tgl'],
            ]);

            return ['message' => 'Tambah mobil success', 'data' => []];

        }
    }
}
