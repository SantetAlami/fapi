<?php

namespace App\Http\Controllers\Ban\insert;

use App\Http\Controllers\Ban\users;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HapusMobil extends Controller
{
    public static function run($db, $data)
    {
        $pFaktur = "HM";

        $kondisi = "kondisi";
        $ubah_kondisi = "ubah_kondisi";

        $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

        $bans = DB::select("CALL $db.`sp_posisi`('" . $data['id'] . "', '" . $data['tgl'] . "')");

        foreach ($bans as $ban) {
            if (is_numeric($ban->posisi)) {

                $ksebelum = DB::table($db . '.' . $kondisi)->where('ban_id', $ban->ban_id)->where('tgl', '<', $data['tgl'])->orderBy('tgl', 'desc')->first();
                $_kondisi = [
                    'user_id' => Auth::user()->id,
                    'kondisi_id' => 7,
                    'keterangan' => 'hapus mobil',
                ];

                if (!DB::table($db . '.' . $kondisi)->where(['tgl' => $data['tgl'], 'ban_id' => $ban->ban_id])->exists()) {
                    $_kondisi['faktur'] = $faktur;
                }

                DB::table($db . '.' . $kondisi)->updateOrInsert(
                    ['tgl' => $data['tgl'], 'ban_id' => $ban->ban_id],
                    $_kondisi
                );

                DB::table($db . '.' . $ubah_kondisi)->insert([
                    'tgl' => $data['tgl'],
                    'faktur' => $faktur,
                    'user_id' => Auth::user()->id,
                    'ban_id' => $ban->ban_id,
                    'kondisi_id_sebelum' => $ksebelum->kondisi_id,
                    'kondisi_id_sesudah' => 7,
                    'keterangan' => 'hapus mobil',
                ]);

            }
        }

        $mMobil = DB::table($db . '.mobil')->where('id', $data['id'])->first();
        DB::table($db . '.mobil')->where('id', $data['id'])->update(['hapus' => date("Ymd", Carbon::now()->timestamp)]);

        users::setLog($db, "hapus_mobil", [
            "user" => Auth::user()->name,
            "no_polisi" => $mMobil->no_polisi,
            "tgl_input" => $data['tgl'],
        ]);

        return ['message' => 'Km harian terupdate', 'data' => []];

    }

}
