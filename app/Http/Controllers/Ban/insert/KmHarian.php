<?php

namespace App\Http\Controllers\Ban\insert;

use App\Http\Controllers\Ban\users;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class KmHarian extends Controller
{
    public static function run($db, $data)
    {
        /*
        "data": {
        "tgl": "2020-08-22",
        "mobils": {
        {
        "mobil_id": "735",
        "odometer": 300
        },
        {
        "mobil_id": "735",
        "faktur": "KM2020082100012",
        "odometer": 300
        }
        }
         */

        $pFaktur = "KM";

        $table = 'km_mobil';
        $off = 'mobil_off';
        $uTable = 'km_ban';
        foreach ($data['mobils'] as $mobil) {

            $faktur = $mobil['faktur'] ?? DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

            // $sebelum = DB::table($db . '.' . $table)->where('mobil_id', $mobil['mobil_id'])->where('tgl', '<', $data['tgl'])->orderBy('tgl', 'desc')->first()->odometer_akhir ?? $mobil['odometer_akhir'];

            $m_off = $mobil['off'] ?? 0;
            if ($mobil['odometer_akhir'] - $mobil['odometer_awal'] == 0 or $m_off == 1) {
                $km = DB::table($db . '.' . $table)->where(['tgl' => $data['tgl'], 'mobil_id' => $mobil['mobil_id']])->get();
                if (count($km) >= 1) {
                    $off_faktur = $km[0]->faktur;
                    DB::table($db . '.' . $table)->where(['tgl' => $data['tgl'], 'mobil_id' => $mobil['mobil_id']])->delete();
                    DB::table($db . '.' . $uTable)->where(['faktur' => $off_faktur])->delete();
                }

                $off_ = [
                    'user_id' => Auth::user()->id,
                    'keterangan' => 'Mobil Off',
                ];

                if (!DB::table($db . '.' . $off)->where(['tgl' => $data['tgl'], 'mobil_id' => $mobil['mobil_id']])->exists()) {
                    $off_['faktur'] = $faktur;
                }

                DB::table($db . '.' . $off)->updateOrInsert(
                    ['tgl' => $data['tgl'], 'mobil_id' => $mobil['mobil_id']],
                    $off_
                );

            } else {

                $reqOff = DB::table($db . '.' . $off)->where(['tgl' => $data['tgl'], 'mobil_id' => $mobil['mobil_id']])->get();

                if (count($reqOff) != 0) {
                    DB::table($db . '.' . $off)->where(['tgl' => $data['tgl'], 'mobil_id' => $mobil['mobil_id']])->delete();
                }

                $km_mobil = [
                    'user_id' => Auth::user()->id,
                    'odometer_awal' => $mobil['odometer_awal'],
                    'odometer_akhir' => $mobil['odometer_akhir'],
                    'url' => $mobil['imageUrl'] ?? '',
                    'keterangan' => 'Update odometer',
                ];

                if (!DB::table($db . '.' . $table)->where(['tgl' => $data['tgl'], 'mobil_id' => $mobil['mobil_id']])->exists()) {
                    $km_mobil['faktur'] = $faktur;
                }

                DB::table($db . '.' . $table)->updateOrInsert(
                    ['tgl' => $data['tgl'], 'mobil_id' => $mobil['mobil_id']],
                    $km_mobil
                );

                $bans = DB::select("CALL $db.`sp_posisi`('" . $mobil['mobil_id'] . "', '" . $data['tgl'] . "')");
                // $bans = DB::select("CALL $db.`sp_posisi`('?', '?', '', '');", [$mobil['mobil_id'], $data['tgl']]);
                foreach ($bans as $ban) {
                    if (is_numeric($ban->posisi)) {

                        $km = DB::table($db . '.' . $uTable)->where('ban_id', $ban->ban_id)->where('tgl', '<', $data['tgl'])->orderBy('tgl', 'desc')->first();
                        $km_ban = [
                            'user_id' => Auth::user()->id,
                            'odometer_awal' => $km->odometer_akhir ?? $mobil['odometer_awal'],
                            'odometer_akhir' => $mobil['odometer_akhir'],
                            'url' => $mobil['imageUrl'] ?? '',
                            'keterangan' => 'Update odometer',
                        ];

                        if (!DB::table($db . '.' . $uTable)->where(['tgl' => $data['tgl'], 'ban_id' => $ban->ban_id])->exists()) {
                            $km_ban['faktur'] = $faktur;
                        }
                        // dd($km->odometer_akhir);
                        DB::table($db . '.' . $uTable)->updateOrInsert(
                            ['tgl' => $data['tgl'], 'ban_id' => $ban->ban_id],
                            $km_ban
                        );
                    }
                }

                $mMobil = DB::table($db . '.mobil')->where('id', $mobil['mobil_id'])->first();

                users::setLog($db, "km_harian", [
                    "user" => Auth::user()->name,
                    "no_polisi" => $mMobil->no_polisi,
                    "tgl_input" => $data['tgl'],
                ]);

            }

        }

        return ['message' => 'Km harian terupdate', 'data' => []];

    }

}
