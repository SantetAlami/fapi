<?php

namespace App\Http\Controllers\Ban\insert;

use App\Http\Controllers\Ban\users;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SJBanVulk extends Controller
{
    public static function run($db, $data)
    {
        $pFaktur = "VT";

        $kondisi = "kondisi";
        $lokasi_ban = "lokasi_ban";
        $surat_jalan = "surat_jalan";
        $surat_jalan_vulk = "surat_jalan_vulk";
        $surat_jalan_afkir = "surat_jalan_afkir";
        $ket = 'casing';

        $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

        $bans = $data['bans'];
        $no_sj = $data['no_sj'];

        if (DB::table($db . '.' . $surat_jalan)->where('no', $no_sj)->count() > 1) {
            return ['error' => 'error', 'message' => 'Duplicate No Surat Jalan', 'data' => []];
        }

        if ($data['kondisi_id'] == 5) {
            $ket = 'afkir';
            $surat_jalan_vulk = $surat_jalan_afkir;
        }

        foreach ($bans as $key => $ban) {
            $sebelum = DB::select("CALL $db.`sp_ban`('$ban', '$data[tgl]');")[0];

            if (in_array($sebelum->kondisi_id, [4])) {
                $lokasi_ban_ = [
                    'user_id' => Auth::user()->id,
                    'kota_id' => $data['kota_id'],
                    'keterangan' => 'SJ ban vulk' . $ket,
                ];
                $kondisi_ = [
                    'user_id' => Auth::user()->id,
                    'kondisi_id' => $data['kondisi_id'], //hanya boleh 2 atau 5
                    'keterangan' => 'SJ ban vulk' . $ket,
                ];
                $surat_jalan_vulk_ = [
                    'tgl' => $data['tgl'],
                    'user_id' => Auth::user()->id,
                    'keterangan' => 'SJ ban vulk' . $ket,
                ];

                if (!DB::table($db . '.' . $lokasi_ban)->where(['tgl' => $data['tgl'], 'ban_id' => $ban])->exists()) {
                    $lokasi_ban_['faktur'] = $faktur;
                }
                if (!DB::table($db . '.' . $kondisi)->where(['tgl' => $data['tgl'], 'ban_id' => $ban])->exists()) {
                    $kondisi_['faktur'] = $faktur;
                }
                if (!DB::table($db . '.' . $surat_jalan_vulk)->where(['no_sj' => $no_sj, 'ban_id' => $ban])->exists()) {
                    $surat_jalan_vulk_['faktur'] = $faktur;
                }

                DB::table($db . '.' . $lokasi_ban)->updateOrInsert(
                    ['tgl' => $data['tgl'], 'ban_id' => $ban],
                    $lokasi_ban_
                );
                DB::table($db . '.' . $kondisi)->updateOrInsert(
                    ['tgl' => $data['tgl'], 'ban_id' => $ban],
                    $kondisi_
                );
                DB::table($db . '.' . $surat_jalan_vulk)->updateOrInsert(
                    ['no_sj' => $no_sj, 'ban_id' => $ban],
                    $surat_jalan_vulk_
                );
            }
        }

        $surat_jalan_ = [
            'tgl' => $data['tgl'],
            'no' => $no_sj,
            'user_id' => Auth::user()->id,
            'faktur' => $faktur,
            'keterangan' => 'SJ ban vulk',
        ];

        DB::table($db . '.' . $surat_jalan)->insert(
            $surat_jalan_
        );

        users::setLog($db, "sj_masuk", [
            "user" => Auth::user()->name,
            "no_sj" => $no_sj,
            "jumlah_ban" => count($bans),
            "lokasi" => DB::table($db . '.kota')->where('id', $data['kota_id'])->first()->keterangan,
            "tgl_input" => $data['tgl'],
        ]);

        return ['message' => 'SJ ban vulk' . $ket . ' success', 'data' => []];
    }
}
