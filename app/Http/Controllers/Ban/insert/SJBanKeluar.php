<?php

namespace App\Http\Controllers\Ban\insert;

// use App\Http\Controllers\Ban\GateController;
use App\Http\Controllers\Ban\users;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SJBanKeluar extends Controller
{
    public static function run($db, $data)
    {
        $pFaktur = "BK";

        $mban = "ban";
        $kondisi = "kondisi";
        $surat_jalan = "surat_jalan";
        $surat_jalan_keluar = "surat_jalan_keluar";

        $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

        $bans = $data['bans'];
        $no_sj = $data['no_sj'];

        if (DB::table($db . '.' . $surat_jalan)->where('no', $no_sj)->count() > 1) {
            return ['error' => 'error', 'message' => 'Duplicate No Surat Jalan', 'data' => []];
        }

        foreach ($bans as $key => $ban) {
            $sebelum = DB::select("CALL $db.`sp_ban`('$ban', '$data[tgl]');")[0];

            if (in_array($sebelum->kondisi_id, [3]) && $sebelum->status_id != 6) {

                DB::table($db . '.' . $mban)->where('id', $ban)->update(['vendor' => $data['vendor']]);

                $kondisi_ = [
                    'user_id' => Auth::user()->id,
                    'kondisi_id' => 4,
                    'keterangan' => 'Kirim ke vendor vulk',
                ];
                $surat_jalan_keluar_ = [
                    'tgl' => $data['tgl'],
                    'user_id' => Auth::user()->id,
                    'vendor' => $data['vendor'],
                    'keterangan' => 'Kirim ke vendor vulk',
                ];

                if (!DB::table($db . '.' . $kondisi)->where(['tgl' => $data['tgl'], 'ban_id' => $ban])->exists()) {
                    $kondisi_['faktur'] = $faktur;
                }
                if (!DB::table($db . '.' . $surat_jalan_keluar)->where(['no_sj' => $no_sj, 'ban_id' => $ban])->exists()) {
                    $surat_jalan_keluar_['faktur'] = $faktur;
                }

                DB::table($db . '.' . $kondisi)->updateOrInsert(
                    ['tgl' => $data['tgl'], 'ban_id' => $ban],
                    $kondisi_
                );
                DB::table($db . '.' . $surat_jalan_keluar)->updateOrInsert(
                    ['no_sj' => $no_sj, 'ban_id' => $ban],
                    $surat_jalan_keluar_
                );
            }
        }

        $surat_jalan_ = [
            'tgl' => $data['tgl'],
            'no' => $no_sj,
            'user_id' => Auth::user()->id,
            'faktur' => $faktur,
            'keterangan' => 'SJ ban keluar',
        ];

        DB::table($db . '.' . $surat_jalan)->insert(
            $surat_jalan_
        );

        users::setLog($db, "sj_keluar", [
            "user" => Auth::user()->name,
            "no_sj" => $no_sj,
            "jumlah_ban" => count($bans),
            "tgl_input" => $data['tgl'],
        ]);

        return ['message' => 'Kirim ke vendor vulk success', 'data' => []];
    }

}
