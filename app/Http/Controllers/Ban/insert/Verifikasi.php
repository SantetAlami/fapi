<?php

namespace App\Http\Controllers\Ban\insert;

use App\Http\Controllers\Ban\users;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Verifikasi extends Controller
{
    public static function run($db, $data)
    {
        $pFaktur = "VF";

        $kondisi = "kondisi";
        $verifikasi = "verifikasi";

        $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

        $bans = $data['bans'];
        $no_sj = $data['no_sj'];
        $status_ban = "status_ban";
        $riwayat = "riwayat";
        $sebelum = DB::select("CALL $db.`sp_perjalanan`('" . ($data['kota_id'] ?? users::get($db, 'kota_id')) . "', '" . $data['tgl'] . "', '" . $no_sj . "');");

        $lokasi = DB::table($db . '.kota')->where('id', $data['kota_id'] ?? users::get($db, 'kota_id'))->first()->keterangan;
        if (empty($sebelum)) {
            return ['error' => 'error', 'message' => 'Tidak di temukan no surat jalan di lokasi ' . $lokasi, 'data' => []];
        }

        foreach ($sebelum as $key => $ban) {

            if (in_array($ban->ban_id, $bans)) {
                switch (true) {
                    case substr($ban->faktur, 0, 2) == 'NT':
                        $status_id = $ban->status_id;
                        break;
                    case substr($ban->faktur, 0, 2) == 'BA':
                        $status_id = $ban->status_id;
                        $kondisi_id = 5;
                        break;
                    case $ban->status_id == 1 and substr($ban->faktur, 0, 2) == 'VT':
                        $status_id = 2;
                        break;
                    case $ban->status_id == 2 and substr($ban->faktur, 0, 2) == 'VT':
                        $status_id = 5;
                        break;
                    case $ban->status_id == 5 and substr($ban->faktur, 0, 2) == 'VT':
                        $status_id = 6;
                        break;
                    case $ban->status_id == 6:
                        return ['error' => 'error', 'message' => 'Ban ' . $ban->no_seri . ' sudah di vulkanisir sebanyak 3x', 'data' => []];
                        break;

                    default:
                        break;

                }

                $kondisi_ = [
                    'user_id' => Auth::user()->id,
                    'kondisi_id' => $kondisi_id ?? 2,
                    'keterangan' => 'Verifikasi kedatangan',
                ];
                $status_ban_ = [
                    'user_id' => Auth::user()->id,
                    'status_id' => $status_id,
                    'keterangan' => 'Verifikasi kedatangan',
                ];
                $verifikasi_ = [
                    'tgl' => $data['tgl'],
                    'user_id' => Auth::user()->id,
                    'vendor' => $ban->vendor,
                    'kota_id' => $data['kota_id'],
                    'keterangan' => 'Verifikasi kedatangan',
                ];
                $riwayat_ = [
                    'user_id' => Auth::user()->id,
                    'ban_id' => $ban->ban_id,
                    'tgl_masuk' => $data['tgl'],
                    'lokasi_datang_id' => $data['kota_id'],
                    'lokasi_datang' => $lokasi,
                    'faktur_verifikasi' => $faktur,
                    'no_seri' => $ban->no_seri,
                    'vendor' => $ban->vendor,
                    'ukuran' => $ban->ukuran,
                    'merek' => $ban->merek,
                    'status_id' => $status_id,
                    'status' => $ban->status_ban,
                    'ketebalan' => $ban->ketebalan,
                ];

                if (!DB::table($db . '.' . $kondisi)->where(['tgl' => $data['tgl'], 'ban_id' => $ban->ban_id])->exists()) {
                    $kondisi_['faktur'] = $faktur;
                }
                if (!DB::table($db . '.' . $status_ban)->where(['tgl' => $data['tgl'], 'ban_id' => $ban->ban_id])->exists()) {
                    $status_ban_['faktur'] = $faktur;
                }
                if (!DB::table($db . '.' . $verifikasi)->where(['no_sj' => $no_sj, 'ban_id' => $ban->ban_id])->exists()) {
                    $verifikasi_['faktur'] = $faktur;
                }
                // if (!DB::table($db . '.' . $riwayat)->where(['tgl_masuk' => $data['tgl'], 'ban_id' => $ban->ban_id, 'status' => $ban->status_ban])->exists()) {
                //     $riwayat_['faktur_verifikasi'] = $faktur;
                // }

                DB::table($db . '.' . $kondisi)->updateOrInsert(
                    ['tgl' => $data['tgl'], 'ban_id' => $ban->ban_id],
                    $kondisi_
                );
                DB::table($db . '.' . $status_ban)->updateOrInsert(
                    ['tgl' => $data['tgl'], 'ban_id' => $ban->ban_id],
                    $status_ban_
                );
                DB::table($db . '.' . $verifikasi)->updateOrInsert(
                    ['no_sj' => $no_sj, 'ban_id' => $ban->ban_id],
                    $verifikasi_
                );
                DB::table($db . '.' . $riwayat)->insertOrIgnore(
                    $riwayat_
                );
            }

            users::setLog($db, "verifikasi", [
                "user" => Auth::user()->name,
                "no_sj" => $no_sj,
                "jumlah_ban" => count($bans),
                "lokasi" => DB::table($db . '.kota')->where('id', ($data['kota_id'] ?? users::get($db, 'kota_id')))->first()->keterangan,
                "tgl_input" => $data['tgl'],
            ]);
    
        }

        return ['message' => 'Verifikasi kedatangan success', 'data' => []];
    }
}
