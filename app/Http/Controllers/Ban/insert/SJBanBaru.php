<?php

namespace App\Http\Controllers\Ban\insert;

use App\Http\Controllers\Ban\users;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SJBanBaru extends Controller
{
    public static function run($db, $data)
    {
        $pFaktur = "NT";

        $ban_tabel = "ban";
        $no_seri = "no_seri";
        // $km_ban = "km_ban";
        $ketebalan = "ketebalan";
        $posisi = "posisi";
        $kondisi = "kondisi";
        $lokasi = "lokasi_ban";
        $status_ban = "status_ban";
        $surat_jalan = "surat_jalan";
        $surat_jalan_baru = "surat_jalan_baru";
        // $riwayat = "riwayat";

        $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

        $bans = $data['bans'];
        $no_sj = $data['no_sj'];
        if (DB::table($db . '.' . $surat_jalan)->where('no', $no_sj)->count() > 1) {
            return ['error' => 'error', 'message' => 'Duplicate No Surat Jalan', 'data' => []];
        }
        foreach ($bans as $key => $ban) {
            $ban_id = DB::table($db . '.' . $ban_tabel)->insertGetId(
                [
                    'faktur' => $faktur,
                    'no_seri' => $ban['no_seri'],
                    'vendor' => $data['vendor'],
                    'ukuran' => $ban['ukuran'],
                    'merek' => $ban['merek'],
                ]
            );

            // $km_ban_ = [
            //     'user_id' => Auth::user()->id,
            //     'odometer_awal' => 0,
            //     'odometer_akhir' => 0,
            //     'keterangan' => 'SJ ban baru',
            // ];
            $no_seri_ = [
                'user_id' => Auth::user()->id,
                'no_seri' => $ban['no_seri'],
                'keterangan' => 'SJ ban baru',
            ];
            $ketebalan_ = [
                'user_id' => Auth::user()->id,
                'ketebalan' => $ban['ketebalan'],
                'keterangan' => 'SJ ban baru',
            ];
            $posisi_ = [
                'user_id' => Auth::user()->id,
                'mobil_id' => 0,
                'posisi' => '',
                'keterangan' => 'SJ ban baru',
            ];
            $kondisi_ = [
                'user_id' => Auth::user()->id,
                'kondisi_id' => 6,
                'keterangan' => 'SJ ban baru',
            ];
            $lokasi_ = [
                'user_id' => Auth::user()->id,
                'kota_id' => $data['kota_id'],
                'keterangan' => 'SJ ban baru',
            ];
            $status_ban_ = [
                'user_id' => Auth::user()->id,
                'status_id' => $ban['status_id'],
                'keterangan' => 'SJ ban baru',
            ];
            $surat_jalan_baru_ = [
                'user_id' => Auth::user()->id,
                'tgl' => $data['tgl'],
                'vendor' => $data['vendor'],
                'kota_id' => $data['kota_id'],
                'keterangan' => 'SJ ban baru',
            ];
            // $riwayat_ = [
            //     'user_id' => Auth::user()->id,
            //     'no_seri' => $ban['no_seri'],
            //     'vendor' => $data['vendor'],
            //     'ukuran' => $ban['ukuran'],
            //     'merek' => $ban['merek'],
            //     'ketebalan' => $ban['ketebalan'],
            // ];

            // if (!DB::table($db . '.' . $km_ban)->where(['tgl' => $data['tgl'], 'ban_id' => $ban_id])->exists()) {
            //     $km_ban_['faktur'] = $faktur;
            // }
            if (!DB::table($db . '.' . $no_seri)->where(['tgl' => $data['tgl'], 'ban_id' => $ban_id])->exists()) {
                $no_seri_['faktur'] = $faktur;
            }
            if (!DB::table($db . '.' . $ketebalan)->where(['tgl' => $data['tgl'], 'ban_id' => $ban_id])->exists()) {
                $ketebalan_['faktur'] = $faktur;
            }
            if (!DB::table($db . '.' . $posisi)->where(['tgl' => $data['tgl'], 'ban_id' => $ban_id])->exists()) {
                $posisi_['faktur'] = $faktur;
            }
            if (!DB::table($db . '.' . $kondisi)->where(['tgl' => $data['tgl'], 'ban_id' => $ban_id])->exists()) {
                $kondisi_['faktur'] = $faktur;
            }
            if (!DB::table($db . '.' . $lokasi)->where(['tgl' => $data['tgl'], 'ban_id' => $ban_id])->exists()) {
                $lokasi_['faktur'] = $faktur;
            }
            if (!DB::table($db . '.' . $status_ban)->where(['tgl' => $data['tgl'], 'ban_id' => $ban_id])->exists()) {
                $status_ban_['faktur'] = $faktur;
            }
            if (!DB::table($db . '.' . $surat_jalan_baru)->where(['no_sj' => $no_sj, 'ban_id' => $ban_id])->exists()) {
                $surat_jalan_baru_['faktur'] = $faktur;
            }
            // if (!DB::table($db . '.' . $riwayat)->where(['tgl_masuk' => $data['tgl'], 'ban_id' => $ban_id, 'status' => $ban['status_id']])->exists()) {
            //     $riwayat_['faktur_verifikasi'] = $faktur;
            // }

            // DB::table($db . '.' . $km_ban)->updateOrInsert(
            //     ['tgl' => $data['tgl'], 'ban_id' => $ban_id],
            //     $km_ban_
            // );
            DB::table($db . '.' . $no_seri)->updateOrInsert(
                ['tgl' => $data['tgl'], 'ban_id' => $ban_id],
                $no_seri_
            );
            DB::table($db . '.' . $ketebalan)->updateOrInsert(
                ['tgl' => $data['tgl'], 'ban_id' => $ban_id],
                $ketebalan_
            );
            DB::table($db . '.' . $posisi)->updateOrInsert(
                ['tgl' => $data['tgl'], 'ban_id' => $ban_id],
                $posisi_
            );
            DB::table($db . '.' . $kondisi)->updateOrInsert(
                ['tgl' => $data['tgl'], 'ban_id' => $ban_id],
                $kondisi_
            );
            DB::table($db . '.' . $lokasi)->updateOrInsert(
                ['tgl' => $data['tgl'], 'ban_id' => $ban_id],
                $lokasi_
            );
            DB::table($db . '.' . $status_ban)->updateOrInsert(
                ['tgl' => $data['tgl'], 'ban_id' => $ban_id],
                $status_ban_
            );
            DB::table($db . '.' . $surat_jalan_baru)->updateOrInsert(
                ['no_sj' => $no_sj, 'ban_id' => $ban_id],
                $surat_jalan_baru_
            );
            // DB::table($db . '.' . $riwayat)->updateOrInsert(
            //     ['tgl_masuk' => $data['tgl'], 'ban_id' => $ban_id, 'status' => $ban['status_id']],
            //     $riwayat_
            // );
        }

        $surat_jalan_ = [
            'tgl' => $data['tgl'],
            'no' => $no_sj,
            'user_id' => Auth::user()->id,
            'kota_id' => $data['kota_id'],
            'faktur' => $faktur,
            'keterangan' => 'SJ ban baru',
        ];

        DB::table($db . '.' . $surat_jalan)->insert(
            $surat_jalan_
        );

        users::setLog($db, "sj_baru", [
            "user" => Auth::user()->name,
            "no_sj" => $no_sj,
            "jumlah_ban" => count($bans),
            "lokasi" => DB::table($db . '.kota')->where('id', $data['kota_id'])->first()->keterangan,
            "tgl_input" => $data['tgl'],
        ]);

        return ['message' => 'SJ ban baru success', 'data' => []];
    }
}
