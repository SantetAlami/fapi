<?php

namespace App\Http\Controllers\Ban\insert;

use App\Http\Controllers\Ban\users;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Ketebalan extends Controller
{

    public static function run($db, $data)
    {
        //
        $pFaktur = "CK";

        $table = "ketebalan";
        $uTable = "update_ketebalan";
        foreach ($data['bans'] as $ban) {
            $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

            $sebelum = DB::table($db . '.' . $table)->where('ban_id', $ban['ban_id'])->where('tgl', '<', $data['tgl'])->orderBy('tgl', 'desc')->first()->ketebalan;
            $ksebelum = DB::table($db . '.' . $uTable)->where('ban_id', $ban['ban_id'])->where('tgl', '<', $data['tgl'])->orderBy('tgl', 'desc')->first()->km_tempuh;

            $ketebalan = [
                'user_id' => Auth::user()->id,
                'ketebalan' => $ban['ketebalan'],
                'used' => min(explode("-", $ban['ketebalan'])) - $sebelum,
                'efisiensi' => ((min(explode("-", $ban['km_tempuh'])) - $ksebelum)) / min(explode("-", $ban['ketebalan'])) - $sebelum,
                'keterangan' => 'Update Ketebalan',
            ];

            $update_ketebalan = [
                'user_id' => Auth::user()->id,
                'ketebalan_awal' => $sebelum,
                'ketebalan_akhir' => $ban['ketebalan'],
                'km_tempuh' => $ban['km_tempuh'],
                'efisiensi' => ((min(explode("-", $ban['km_tempuh'])) - $ksebelum)) / min(explode("-", $ban['ketebalan'])) - $sebelum,
                'keterangan' => 'Update Ketebalan',
            ];

            if (!DB::table($db . '.' . $table)->where(['tgl' => $data['tgl'], 'ban_id' => $ban['ban_id']])->exists()) {
                $ketebalan['faktur'] = $faktur;
            }

            if (!DB::table($db . '.' . $uTable)->where(['tgl' => $data['tgl'], 'ban_id' => $ban['ban_id']])->exists()) {
                $update_ketebalan['faktur'] = $faktur;
            }

            DB::table($db . '.' . $table)->updateOrInsert(
                ['tgl' => $data['tgl'], 'ban_id' => $ban['ban_id']],
                $ketebalan
            );

            DB::table($db . '.' . $uTable)->updateOrInsert(
                ['tgl' => $data['tgl'], 'ban_id' => $ban['ban_id']],
                $update_ketebalan
            );
        }

        users::setLog($db, "ketebalan", [
            "user" => Auth::user()->name,
            "no_seri" => DB::table($db . '.no_seri')->where('ban_id', $ban['ban_id'])->orderByDesc('tgl')->first()->no_seri,
            "ketebalan_awal" => $sebelum,
            "ketebalan_akhir" => $ban['ketebalan'],
            "tgl_input" => $data['tgl'],
        ]);

        return ['message' => 'Ketebalan ban terupdate', 'data' => []];

    }

}
