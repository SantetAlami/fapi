<?php

namespace App\Http\Controllers\Ban\insert;

use App\Http\Controllers\Ban\users;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Rotasi extends Controller
{
    public static function run($db, $data)
    {
        $pFaktur = "RB";

        $table = "posisi";
        $uTable = "rotasi";

        foreach ($data['bans'] as $k => $ban) {
            $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

            $dari = "";
            $ke = null;

            foreach ($ban as $key => $item) {
                try {
                    $dari = ($key == 0) ? DB::select("CALL $db.`sp_ban`('" . ($item['id'] ?? $item) . "', '$data[tgl]');")[0]->posisi : $dari;
                    $ke = ($key == 1) ? DB::select("CALL $db.`sp_ban`('" . ($item['id'] ?? $item) . "', '$data[tgl]');")[0]->posisi : $ke;
                } catch (\Throwable $th) {
                }
            }

            // if ($dari == "" or $ke == "") {
            //     return ['error' => 'error', 'message' => 'Ups', 'data' => []];
            // }

            foreach ($ban as $index => $id_ban) {
                $posisi = [
                    'user_id' => Auth::user()->id,
                    'mobil_id' => $data['mobil_id'],
                    'posisi' => $id_ban['posisi'] ?? (($index == 0) ? $ke : $dari),
                    'keterangan' => 'Rotasi',
                ];

                if (!DB::table($db . '.' . $table)->where(['tgl' => $data['tgl'], 'ban_id' => $id_ban['id'] ?? $id_ban])->exists()) {
                    $posisi['faktur'] = $faktur;
                }

                DB::table($db . '.' . $table)->updateOrInsert(
                    ['tgl' => $data['tgl'], 'ban_id' => $id_ban['id'] ?? $id_ban],
                    $posisi
                );
            }
            DB::table($db . '.' . $uTable)->insert([
                'tgl' => $data['tgl'],
                'faktur' => $faktur,
                'user_id' => Auth::user()->id,
                'mobil_id' => $data['mobil_id'],
                'ban_id_dari' => $ban[0]['id'] ?? ($ban[0] ?? 0),
                'posisi_dari' => $dari,
                'ban_id_ke' => $ban[1]['id'] ?? ($ban[1] ?? 0),
                'posisi_ke' => $ke ?? ($ban[1]['posisi'] ?? $ban[0]['posisi']),
                'keterangan' => 'Rotasi',
            ]);

            users::setLog($db, "rotasi", [
                "user" => Auth::user()->name,
                "no_seri_1" => DB::table($db . '.no_seri')->where('ban_id', $ban[0]['id'] ?? ($ban[0] ?? 0))->orderByDesc('tgl')->first()->no_seri . " (Posisi : $dari)",
                "no_seri_2" => (DB::table($db . '.no_seri')->where('ban_id', $ban[1]['id'] ?? ($ban[1] ?? 0))->orderByDesc('tgl')->first()->no_seri ?? 0) . " (Posisi : " . ($ke ?? ($ban[1]['posisi'] ?? $ban[0]['posisi'])) . ")",
                "tgl_input" => $data['tgl'],
            ]);

        }

        return ['message' => 'Rotasi ban berhasil', 'data' => []];
    }

}
