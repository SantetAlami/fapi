<?php

namespace App\Http\Controllers\Ban;

use App\Http\Controllers\Ban\insert\HapusFaktur;
use App\Http\Controllers\Ban\insert\Ketebalan;
use App\Http\Controllers\Ban\insert\KmHarian;
use App\Http\Controllers\Ban\insert\MutasiBan;
use App\Http\Controllers\Ban\insert\MutasiMobil;
use App\Http\Controllers\Ban\insert\Pasang;
use App\Http\Controllers\Ban\insert\Pergantian;
use App\Http\Controllers\Ban\insert\Rotasi;
use App\Http\Controllers\Ban\insert\RotasiAMobil;
use App\Http\Controllers\Ban\insert\SJBanBaru;
use App\Http\Controllers\Ban\insert\SJBanKeluar;
use App\Http\Controllers\Ban\insert\SJBanVulk;
use App\Http\Controllers\Ban\insert\TambahBan;
use App\Http\Controllers\Ban\insert\TambahMobil;
use App\Http\Controllers\Ban\insert\UbahKondisi;
use App\Http\Controllers\Ban\insert\Verifikasi;
use App\Http\Controllers\Ban\insert\HapusMobil;
// use App\Http\Controllers\Ban\update\MutasiBan as UMutasiBan;
// use App\Http\Controllers\Ban\update\MutasiMobil as UMutasiMobil;
// use App\Http\Controllers\Ban\update\Pasang as UPasang;
// use App\Http\Controllers\Ban\update\Pergantian as UPergantian;
// use App\Http\Controllers\Ban\update\TambahBan as UTambahBan;
// use App\Http\Controllers\Ban\update\TambahMobil as UTambahMobil;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FapiController;
use Carbon\Carbon;

class GateController extends Controller
{

    public function operation($operation, $data, $db)
    {
        $db = empty($db) ? "sewa_ban" : $db;

        switch ($operation) {
            case 'tambah_ban':
                $res = TambahBan::run($db, $data);
                break;

            case 'km_daily':
                $res = KmHarian::run($db, $data);
                break;

            case 'cek_ketebalan':
                $res = Ketebalan::run($db, $data);
                break;

            case 'rotasi':
                $res = Rotasi::run($db, $data);
                break;

            case 'pergantian':
                $res = Pergantian::run($db, $data);
                break;

            case 'ganti_kondisi':
                $res = UbahKondisi::run($db, $data);
                break;

            case 'mutasi_ban':
                $res = MutasiBan::run($db, $data);
                break;

            case 'rotasi_antar_mobil':
                $res = RotasiAMobil::run($db, $data);
                break;

            case 'sj_ban_baru':
                $res = SJBanBaru::run($db, $data);
                break;
            case 'sj_ban_vulk':
                $res = SJBanVulk::run($db, $data);
                break;
            case 'sj_ban_keluar':
                $res = SJBanKeluar::run($db, $data);
                break;
            case 'verifikasi':
                $res = Verifikasi::run($db, $data);
                break;

            case 'mutasi_mobil':
                $res = MutasiMobil::run($db, $data);
                break;

            case 'tambah_mobil':
                $res = TambahMobil::run($db, $data);
                break;

            case 'pasang_ban':
                $res = Pasang::run($db, $data);
                break;

            // case 'update_mutasi_ban':
            // case 'update_pindah_ban':
            // case 'update_pindah_lokasi_ban':
            //     $res = UMutasiBan::run($db, $data);
            //     break;

            // case 'rotasi_antar_mobil':
            // case 'rotasi_ban_antar_mobil':
            //     $res = RotasiAMobil::run($db, $data);
            //     break;

            case 'hapus_faktur':
                $res = HapusFaktur::run($db, $data);
                break;

            case 'hapus_mobil':
                $res = HapusMobil::run($db, $data);
                break;

            default:
                return FapiController::response('error', "operation $operation not found", [], 400);
                break;
        }

        return FapiController::response($res['error'] ?? 'success', $res['message'], $res['data'], isset($res['error']) ? 409 : 200);

    }

    public static function insertHistory($id_user, $requsetData)
    {
        // dd($requsetData);
        $arrBans = [];
        $arrBans['tgl_history_ban'] = $requsetData->tgl_history_ban ?? date("Y-m-d H:i:s", Carbon::now()->timestamp);
        $arrBans['id_user'] = $id_user;
        $arrBans['faktur'] = $requsetData->faktur;
        // $arrBans['faktur'] = $prefix . date("Ymd", Carbon::now()->timestamp) . str_pad($id_user, 3, '0', STR_PAD_LEFT) . str_pad($jumlah_hari_ini, 6, '0', STR_PAD_LEFT);

        $arrBans['id_mobil'] = $requsetData->id_mobil;
        $arrBans['lokasi'] = $requsetData->lokasi;

        $arrBans['id_ban'] = $requsetData->id_ban;
        $arrBans['posisi_ban'] = $requsetData->posisi_ban;

        $arrBans['odometer'] = $requsetData->odometer ?? 0;
        $arrBans['odometer_akhir'] = $requsetData->odometer_akhir ?? 0;
        $arrBans['ketebalan'] = $requsetData->ketebalan ?? null;

        $arrBans['status_ban'] = $requsetData->status_ban;
        $arrBans['tipe_ban'] = $requsetData->tipe_ban;

        $arrBans['no_po'] = $requsetData->no_po ?? null;
        $arrBans['no_surat_jalan'] = $requsetData->no_surat_jalan ?? null;

        $arrBans['id_vendor'] = $requsetData->id_vendor ?? null;

        $arrBans['keterangan'] = $requsetData->keterangan;
        $arrBans['checked'] = $requsetData->checked ?? null;
        $arrBans['keterangan_lain'] = $requsetData->keterangan_lain;

        // \DB::table('history_ban')->insert($arrBans);
        return $arrBans;

    }

}
