<?php

namespace App\Http\Controllers;

use App\Gate_validation;
use Illuminate\Support\Facades\DB;

class ValidateController extends Controller
{
    public static function getRules($db, $gate)
    {
        $req = DB::table('auth.gate_validation')->where("staff", $db)->where("gate", $gate)->get();

        $rules = [];
        foreach ($req as $key => $item) {
            if (!empty($item->required)) {
                $rules[$item->pillar]['required'] = $item->required;
            }
            if (!empty($item->email)) {
                $rules[$item->pillar]['email'] = $item->email;
            }
            if (!empty($item->unique)) {
                $rules[$item->pillar]['unique'] = $item->unique;
            }
            if (!empty($item->required_if)) {
                $rules[$item->pillar]['required_if'] = $item->required_if;
            }
            if (!empty($item->must_if)) {
                $rules[$item->pillar]['must_if'] = $item->must_if;
            }
            if (!empty($item->math)) {
                $rules[$item->pillar]['math'] = $item->math;
            }
        }

        return $rules;
    }
    public static function throwable($pillar, $key, $subkey = [])
    {
        $throwable = [
            "required" => ":field tidak boleh kosong.",
            "email" => ":field harus alamat email yang valid.",
            "unique" => ":field sudah ada.",
            "required_if" => [
                "=" => ":field tidak boleh kosong jika :target sama dengan :target_val.",
                "!=" => ":field tidak boleh kosong jika :target berbeda dengan :target_val.",
                "<" => ":field tidak boleh kosong jika :target kurang dari :target_val.",
                "<=" => ":field tidak boleh kosong jika :target sama atau kurang dari :target_val.",
                ">" => ":field tidak boleh kosong jika :target kurang dari :target_val.",
                ">=" => ":field tidak boleh kosong jika :target sama atau lebih dari :target_val.",
            ],
            "must_if" => [
                "=" => ":field harus :val jika :target sama dengan :target_val.",
                "!=" => ":field harus :val jika :target berbeda dengan :target_val.",
                "<" => ":field harus :val jika :target kurang dari :target_val.",
                "<=" => ":field harus :val jika :target sama atau kurang dari :target_val.",
                ">" => ":field harus :val jika :target kurang dari :target_val.",
                ">=" => ":field harus :val jika :target sama atau lebih dari :target_val.",
            ],
            "math" => [
                "=" => ":field tidak sama dengan :target.",
                "!=" => ":field harus berbeda dengan :target.",
                "<" => ":field harus kurang dari :target.",
                "<=" => ":field harus sama atau kurang dari :target.",
                ">" => ":field harus kurang dari :target.",
                ">=" => ":field harus sama atau lebih dari :target.",
            ],
        ];

        if (!empty($subkey)) {
            switch ($key) {
                case 'must_if':
                    // "dua puluh", pillar2 = 20
                    $target = explode(" ", trim($subkey[1]));

                    $message = str_replace(":val", $subkey[0], $throwable[$key][$target[1]]);
                    $message = str_replace(":target", $target[0], $message);
                    $message = str_replace(":target_val", $target[0], $message);
                    return str_replace(':target', UCFirst($subkey[1]), $message);
                    break;

                case 'math':
                    return str_replace(':target', UCFirst($subkey[1]), $throwable[$key][$subkey[0]]);
                    break;

                default:
                    # code...
                    break;
            }
        }

        return str_replace(':field', str_replace("_", " ", UCFirst($pillar)), $throwable[$key]);
    }

    public static function check($validate, $db, $gate, $pillar, $id, $value, $RequestData)
    {
        $err = [];
        if (isset($validate['required'])) {
            if ($validate['required'] == 1 and ValidateController::required($value)) {
                $err[] = ValidateController::throwable($pillar, 'required');
            }
        }
        if (isset($validate['email'])) {
            if ($validate['email'] == 1 and ValidateController::email($value)) {
                $err[] = ValidateController::throwable($pillar, 'email');
            }
        }
        if (isset($validate['unique'])) {
            // return FapiController::response('error', 'uuuuu', ValidateController::unique($db, $gate, $pillar, $id, $value), 409);
            if ($validate['unique'] == 1 and ValidateController::unique($db, $gate, $pillar, $id, $value)) {
                $err[] = ValidateController::throwable($pillar, 'unique');
            }
        }
        if (isset($validate['must_if'])) {
            $valid = explode(",", $validate['must_if']);
            if (ValidateController::must_if($RequestData, $valid, $value)) {
                $err[] = ValidateController::throwable($pillar, 'must_if', [$valid[0], $RequestData[$valid[1]]]);
            }
        }
        if (isset($validate['math'])) {
            $valid = explode(",", str_replace(" ", "", $validate['math']));
            if (ValidateController::math($RequestData, $valid, $value)) {
                $err[] = ValidateController::throwable($pillar, 'math', [$valid[0], $RequestData[$valid[1]]]);
            }
        }

        return $err;
    }

    /**
     * Store a new user.
     *
     * @param  Array  $rule
     * @return Response
     */

    public static function gate($RequestData, $db, $gate, $id, $rules = [])
    {
        // [
        //     "name" => "",
        //     "email" => "isi",
        //     "password" => "isi",
        //     "password_confirm" => "isi",
        // ];
        // [
        //     "name" => ["required" => 1],
        //     "email" => ["required" => 1, "email" => 1, "unique" => 1],
        // ];
        try {
            if (isset($RequestData[0])) {
                foreach ($RequestData as $key => $request) {
                    return ValidateController::hallway($request, $db, $gate, $id, $rules);
                }
            } else {
                return ValidateController::hallway($RequestData, $db, $gate, $id, $rules);
            }
        } catch (\Throwable $e) {
            return FapiController::response('error', 'validating error ' . $e->getMessage(), null, 409);
        }
    }

    public static function hallway($RequestData, $db, $gate, $id, $rules)
    {
        $cols = DB::select("SELECT * FROM (
            SELECT `COLUMN_NAME` AS col
            FROM `INFORMATION_SCHEMA`.`COLUMNS`
            WHERE `TABLE_SCHEMA`='$db' AND `TABLE_NAME`='$gate'
        ) a WHERE col NOT IN ('id', 'created_at', 'updated_at')");

        $data = [];
        foreach ($cols as $key => $item) {
            $data[$item->col] = $RequestData[$item->col] ?? '';
        }

        $validate = $rules;
        if (empty($rules)) {
            $res = Gate_validation::where('staff', $db)->where('gate', $gate)->get()->toArray();
            foreach ($res as $key => $value) {
                $validate[$value['pillar']][] = $value;
            }
        }

        $err = [];
        $checkData = $id != 0 ? $RequestData : $data;
        foreach ($checkData as $key => $value) {
            if (isset($validate[$key])) {
                if (isset($validate[$key][0])) {
                    for ($u = 0; $u < count($validate[$key]); $u++) {
                        $res = ValidateController::check($validate[$key][$u], $db, $gate, $key, $id, $value, $checkData);
                        if (!empty($res)) {
                            $err[$key] = $res;
                        }
                    }
                } else {
                    $res = ValidateController::check($validate[$key], $db, $gate, $key, $id, $value, $checkData);
                    if (!empty($res)) {
                        $err[$key] = $res;
                    }
                }
            }
        }
        if ($err) {
            return FapiController::response('error', 'something not valid', $err, 409);
        }
    }

    public static function required($value)
    {
        if (empty($value)) {
            return true;
        }
        return false;
    }

    public static function email($value)
    {
        $find1 = strpos($value, '@');
        $find2 = strpos($value, '.');
        return !($find1 !== false && $find2 !== false && $find2 > $find1);
    }

    public static function unique($db, $gate, $key, $id, $value)
    {
        $tableFromId = DB::table($db . '.' . $gate)->where('id', $id)->first();
        if (isset($tableFromId->$key)) {
            if ($tableFromId->$key == $value) {
                return false;
            }
        }

        $table = DB::table($db . '.' . $gate)->where($key, $value)->first();

        if ($table) {
            if ($table->id != $id) {
                return true; //salah
            }
        }
        return false;
    }

    public static function must_if($RequestData, $valid, $value)
    {
        switch ($valid[1]) {
            case '=':
                $val = "";
                switch ($RequestData[$valid[1]]) {
                    case "<null>":
                        $val = null;
                        break;

                    case "<empty>":
                        $val = "";
                        break;

                    default:
                        $val = $RequestData[$valid[1]];
                        break;
                }
                if ($value == $val) {
                    return true;
                }
                break;

            case '!=':
                $val = "";
                switch ($RequestData[$valid[1]]) {
                    case "<null>":
                        $val = null;
                        break;

                    case "<empty>":
                        $val = "";
                        break;

                    default:
                        $val = $RequestData[$valid[1]];
                        break;
                }
                if ($value != $val) {
                    return true;
                }
                break;

            case '<':
                if ($value < $RequestData[$valid[1]]) {
                    return true;
                }
                break;

            case '<=':
                if ($value <= $RequestData[$valid[1]]) {
                    return true;
                }
                break;

            case '>':
                if ($value > $RequestData[$valid[1]]) {
                    return true;
                }
                break;

            case '>=':
                if ($value >= $RequestData[$valid[1]]) {
                    return true;
                }
                break;

            default:
                return false;
                break;
        }
        return false;
    }

    public static function math($RequestData, $valid, $value)
    {
        switch ($valid[1]) {
            case '=':
                $val = "";
                switch ($RequestData[$valid[1]]) {
                    case "<null>":
                        $val = null;
                        break;

                    case "<empty>":
                        $val = "";
                        break;

                    default:
                        $val = $RequestData[$valid[1]];
                        break;
                }
                if ($value == $val) {
                    return true;
                }
                break;

            case '!=':
                $val = "";
                switch ($RequestData[$valid[1]]) {
                    case "<null>":
                        $val = null;
                        break;

                    case "<empty>":
                        $val = "";
                        break;

                    default:
                        $val = $RequestData[$valid[1]];
                        break;
                }
                if ($value != $val) {
                    return true;
                }
                break;

            case '<':
                if ($value < $RequestData[$valid[1]]) {
                    return true;
                }
                break;

            case '<=':
                if ($value <= $RequestData[$valid[1]]) {
                    return true;
                }
                break;

            case '>':
                if ($value > $RequestData[$valid[1]]) {
                    return true;
                }
                break;

            case '>=':
                if ($value >= $RequestData[$valid[1]]) {
                    return true;
                }
                break;

            default:
                return false;
                break;
        }
        return false;
    }

}
