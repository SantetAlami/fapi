<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Events\MessageSent;
use App\Message;
use Illuminate\Support\Facades\Auth;

class MessagesController extends Controller
{

    /**
     * Delete method.
     *
     * @param  Array  $requestData
     * @return Response
     */

    public function send($requestData)
    {
        try {
            $channel_id = $requestData['channel_id'];
            $message = $requestData['message'];
            $reply_id = $requestData['reply_id'] ?? null;

            $channel = Channel::where('id', $channel_id)->first();

            $send = [
                'channel_id' => $channel->id,
                'user_id' => Auth::user()->id,
                'message' => $message,
                'message_id' => $reply_id,
                'author_username' => Auth::user()->name,
            ];
            $message = Message::create($send);

            event(new MessageSent('Channel-' . $channel->id, $send));
            return FapiController::response('success', 'get data', $message, 200);

        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    /**
     * Delete method.
     *
     * @param  Array  $requestData
     * @return Response
     */

    public function store($requestData)
    {
        try {
            $res = [];
            Message::create($requestData);

            return FapiController::response('success', 'data has successfully created', $res, 201);

        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    /**
     * Update by id method.
     *
     * @param  Array  $requestData
     * @param  DB::class  $table
     * @return Response
     */

    public static function updateById($requestData)
    {
        try {
            $cek = Message::where('id', $requestData['id'])->where('user_id', Auth::user()->id)->first();
            if (empty($cek)) {
                return FapiController::response('success', 'data not found, so it cannot be updated', null, 200);
            }
            $cek->update($requestData['data']);

            $res = Message::where('id', $requestData['id'])->where('user_id', Auth::user()->id)->first();

            return FapiController::response('success', 'data has successfully updated', $res, 200);

        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    /**
     * Update method.
     *
     * @param  Array  $requestData
     * @param  DB::class  $table
     * @return Response
     */

    public static function update($requestData)
    {
        try {
            $cek = Message::where('id', $requestData['id'])->first();
            if (empty($cek)) {
                return FapiController::response('success', 'data not found, so it cannot be updated', null, 200);
            }
            $cek->update($requestData['data']);

            $res = Message::where('id', $requestData['id'])->first();

            return FapiController::response('success', 'data has successfully updated', $res, 200);

        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    /**
     * Delete by id method.
     *
     * @param  Array  $requestData
     * @param  DB::class  $table
     * @return Response
     */

    public static function deleteById($requestData)
    {
        try {
            $res = Message::where('id', $requestData['id'])->where('user_id', Auth::user()->id)->delete();
            if (empty($res)) {
                return FapiController::response('success', 'data not found, so it cannot be updated', null, 200);
            }

            return FapiController::response('success', 'data has successfully deleted', null, 200);

        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    /**
     * Delete method.
     *
     * @param  Array  $requestData
     * @param  DB::class  $table
     * @return Response
     */

    public static function delete($requestData)
    {
        try {
            $res = Message::where('id', $requestData['id'])->delete();
            if (empty($res)) {
                return FapiController::response('success', 'data not found, so it cannot be updated', null, 200);
            }

            return FapiController::response('success', 'data has successfully deleted', null, 200);

        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    /**
     * Delete method.
     *
     * @param  Array  $requestData
     * @param  DB::class  $table
     * @return Response
     */

}
