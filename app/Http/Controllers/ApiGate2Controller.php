<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

// use App\Events\MessageSent;

class ApiGate2Controller extends Controller
{

    /**
     * Start Future Application Programming Interface.
     *
     * @param  Request  $request
     * @param  String  $db
     * @return Response
     */

    public function start(Request $request, $db = null, $gate = null, $operation = null, $id = null)
    {
        $requestData = [];

        try {
            if (empty($request->user())) {
                $credentials = ["email" => 'guest', "password" => '1234'];
                Auth::attempt($credentials);
            }

            $expiresAt = Carbon::now()->addMinutes(1);
            Cache::put('user-is-online-' . $request->user()->id, true, $expiresAt);
            // event(new MessageSent('UserOnline', $message));

        } catch (\Throwable $th) {
        }

        if ($gate) {
            switch ($db) {
                case 'auth':
                    $operation = $gate;
                    $gate = 'auth';
                    break;

                case 'user':
                    $id = $operation;
                    $operation = $gate;
                    $gate = 'user';
                    $db = 'auth';
                    break;

                case 'role':
                    $id = $operation;
                    $operation = $gate;
                    $gate = 'role';
                    $db = 'auth';
                    break;

                case 'permission':
                    $id = $operation;
                    $operation = $gate;
                    $gate = 'permission';
                    $db = 'auth';
                    break;

                default:
                    # code...
                    break;
            }
        }

        // dd([$db, $gate, $operation, $id]);

        if (empty($gate)) {
            $requestData = $request->all();
            if (isset($requestData[0])) {
                $res = [];
                $code = [];
                foreach ($requestData as $key => $value) {
                    $future = $this->connection($value, $db ?? $value['staff'] ?? 'auth', $request->user(), $request->method());
                    $res[] = $future['res'];
                    $code[] = $future['code'];
                }
                return response()->json($res, $code[0]);

            } else {
                $requestData = $request->all();
                $future = $this->connection($requestData, $db, $request->user(), $request->method());
                return response()->json($future['res'], $future['code']);
            }
        } else {
            $requestData['gate'] = $gate;
            $requestData['operation'] = $operation;
            if ($id) {
                $requestData['id'] = $id;
            }
            $requestData['data'] = $request->all();
            $future = $this->connection($requestData, $db, $request->user(), $request->method());
        }
        return response()->json($future['res'], $future['code']);
    }

    /**
     * Start Future Connection Application Programming Interface.
     *
     * @param  Array  $request
     * @param  String  $db
     * @param  Object  $user
     * @return Response
     */

    public static function connection($requestData, $db, $user, $method)
    {
        // dd($requestData);
        LogController::user($requestData, $db, $user['id']);

        try {
            if ($db == 'auth' || $db == 'main') {
                switch ($requestData['gate']) {
                    case 'auth':
                        switch ($requestData['operation']) {
                            case 'getQR':
                                return FapiController::method($method, 'GET', AuthController::getLoginQR());
                                break;
                            case 'loginQRScan':
                                return FapiController::method($method, 'POST', AuthController::loginQRScan($requestData['data']));
                                break;
                            case 'login':
                                return FapiController::method($method, 'POST', AuthController::login($requestData['data']));
                                break;
                            case 'register':
                                return FapiController::method($method, 'POST', AuthController::register($requestData['data']));
                                break;
                            case 'logout':
                                return FapiController::method($method, 'GET', AuthController::logout());
                                break;

                            default:
                                return FapiController::response('error', 'operation not found', null, 404);
                                break;
                        }

                        break;

                    case "role":
                        switch ($requestData['operation']) {
                            case 'getPermission':
                            case 'getPermissions':
                                if ($user->can('read-roles')) {
                                    return FapiController::method($method, ['GET', 'POST'], RoleController::getPermissions($user->idRole(), $requestData['id'] ?? '', $user->level(), $user->roleGroup()));
                                }
                                break;
                            case 'get':
                            case 'read':
                            case 'getRole':
                                if ($user->can('read-roles')) {
                                    return FapiController::method($method, ['GET', 'POST'], RoleController::read($user->level(), $user->roleGroup()));
                                }
                                break;
                            case 'add':
                            case 'create':
                            case 'addRole':
                            case 'createRole':
                                if ($user->can('create-roles')) {
                                    return FapiController::method($method, 'POST', RoleController::store($requestData['data'], $user->level(), $user->roleGroup()));
                                }
                                break;
                            case 'edit':
                            case 'update':
                            case 'updateRole':
                                if ($user->can('update-roles') and $user->can('manage-roles')) {
                                    return FapiController::method($method, ['POST', 'PUT', 'PATCH'], RoleController::update($requestData['id'], $requestData['data'], $user->level(), $user->roleGroup(), true));
                                }
                                break;
                            case 'delete':
                            case 'destroy':
                            case 'deleteRole':
                            case 'destroyRole':
                                if ($user->can('delete-roles') and $user->can('manage-roles')) {
                                    return FapiController::method($method, 'DELETE', RoleController::destroy($requestData['id'], $user->level(), $user->roleGroup()));
                                }
                                break;

                            default:
                                return FapiController::response('error', 'operation ' . $requestData['operation'] . ' not found', null, 404);
                                break;
                        }
                        break;

                    case "permission":
                        switch ($requestData['operation']) {
                            case 'get':
                            case 'read':
                            case 'getPermission':
                                if ($user->can('read-roles')) {
                                    return FapiController::method($method, ['GET', 'POST'], PermissionController::read($user->idRole(), $requestData['staff'] ?? [], $requestData['id'] ?? 0));
                                }
                                break;
                            case 'add':
                            case 'create':
                            case 'addRole':
                            case 'createRole':
                                if ($user->hasRole('super admin')) {
                                    return FapiController::method($method, 'POST', PermissionController::store($requestData['data']));
                                }
                                break;
                            case 'edit':
                            case 'update':
                            case 'updateRole':
                                if ($user->hasRole('super admin')) {
                                    return FapiController::method($method, ['POST', 'PUT', 'PATCH'], PermissionController::update($requestData['id'], $requestData['data']));
                                }
                                break;
                            case 'delete':
                            case 'destroy':
                            case 'deleteRole':
                            case 'destroyRole':
                                if ($user->hasRole('super admin')) {
                                    return FapiController::method($method, 'DELETE', PermissionController::destroy($requestData['id']));
                                }
                                break;

                            default:
                                return FapiController::response('error', 'operation ' . $requestData['operation'] . ' not found', null, 404);
                                break;
                        }
                        break;

                    case "user":
                        $rules = [
                            "name" => [
                                "required" => "1",
                            ],
                            "username" => [
                                "required" => "1",
                                "unique" => "1",
                            ],
                            "email" => [
                                "required" => "1",
                                "email" => "1",
                                "unique" => "1",
                            ],
                            "password" => [
                                "required" => "1",
                                "math" => "=,password_confirm",
                            ],
                            "password_confirm" => [
                                "required" => "1",
                                "math" => "=,password",
                            ],
                        ];
                        switch ($requestData['operation']) {
                            case 'profil':
                            case 'profile':
                                return FapiController::method($method, ['GET', 'POST'], UserController::profile($requestData['data'] ?? []));
                                break;
                            case 'editProfil':
                            case 'editProfile':
                                unset($rules['password']);
                                unset($rules['password_confirm']);
                                if ($valid = ValidateController::gate($requestData['data'], env('DB_DATABASE'), 'users', $user['id'], $rules)) {
                                    return $valid;
                                }

                                return FapiController::method($method, ['GET', 'POST'], UserController::update("", $requestData['data'], "", true));
                                break;
                            case 'add':
                            case 'create':
                            case 'insert':
                                if ($user->can('access-users') and $user->can('create-users')) {
                                    if ($valid = ValidateController::gate($requestData['data'], env('DB_DATABASE'), 'users', 0, $rules)) {
                                        return $valid;
                                    }

                                    return FapiController::method($method, 'POST', UserController::store($requestData['data'], ["level" => $user->level(), "roleGroup" => $user->roleGroup()]));
                                }
                                break;
                            case 'get':
                            case 'read':
                            case 'search':
                                if ($user->can('read-users')) {
                                    return FapiController::method($method, ['GET', 'POST'], UserController::index($requestData['data'], ["level" => $user->level(), "roleGroup" => $user->roleGroup()]));
                                }
                                break;
                            case 'update':
                            case 'edit':
                            case 'change':
                                if ($user->can('access-users') and $user->can('update-users')) {
                                    unset($rules['password']);
                                    unset($rules['password_confirm']);
                                    if ($valid = ValidateController::gate($requestData['data'], env('DB_DATABASE'), 'users', $requestData['id'], $rules)) {
                                        return $valid;
                                    }
                                    return FapiController::method($method, ['PUT', 'PATCH'], UserController::update($requestData['id'], $requestData['data'], ["level" => $user->level(), "roleGroup" => $user->roleGroup()]));
                                }
                                break;
                            case 'del':
                            case 'delete':
                            case 'remove':
                                if ($user->can('access-users') and $user->can('delete-users')) {
                                    return FapiController::method($method, 'DELETE', UserController::destroy($requestData['id'], ["level" => $user->level(), "roleGroup" => $user->roleGroup()]));
                                }
                                break;

                            default:
                                return FapiController::response('error', 'operation ' . $requestData['operation'] . ' not found', null, 404);
                                break;
                        }
                        break;

                    default:
                        return FapiController::response('error', 'gate ' . $requestData['gate'] . ' not found', null, 404);

                        break;
                }
            } else if ($db == 'sys') {
                $namespace = 'App\Http\Controllers\\';
                $controller = $namespace . $requestData['gate'] . '\\GateController';

                if ($user->can('access-sys-' . $requestData['gate'] . '-' . $requestData['operation'])) {
                    return FapiController::method($method, 'POST', app($controller)->operation($requestData['operation'], $requestData['data'], $requestData['gate']));
                }

            } else {

                $table = DB::table($db . '.' . $requestData['gate']);

                $gate = (substr($requestData['gate'], 0, 2) == 'v_') ? str_replace("v_", "", $requestData['gate']) : $requestData['gate'];

                switch ($requestData['operation']) {
                    case 'add':
                    case 'create':
                    case 'insert':
                        $rules = ValidateController::getRules($db, $gate);
                        if ($valid = ValidateController::gate($requestData['data'], $db, $gate, $requestData['id'] ?? 0, $rules)) {
                            return $valid;
                        }
                        if ($user->can('access-' . $db . '-' . $gate) and $user->can('create-' . $db . '-' . $gate)) {
                            return FapiController::method($method, 'POST', FapiController::insert($requestData, $table));
                        }
                        break;

                    case 'getById':
                    case 'readById':
                    case 'searchById':
                        if ($user->can('read-' . $db . '-' . $gate . '-id')) {
                            return FapiController::method($method, ['GET', 'POST'], FapiController::readById($requestData, $table));
                        }
                        break;

                    case 'get':
                    case 'read':
                    case 'search':
                        if ($user->can('read-' . $db . '-' . $gate)) {
                            return FapiController::method($method, ['GET', 'POST'], FapiController::read($requestData, $table));
                        }
                        break;

                    case 'updateById':
                    case 'editById':
                    case 'changeById':
                        $rules = ValidateController::getRules($db, $gate);
                        if ($valid = ValidateController::gate($requestData['data'], $db, $gate, $requestData['id'] ?? 0, $rules)) {
                            return $valid;
                        }
                        if ($user->can('update-' . $db . '-' . $gate . '-id')) {
                            return FapiController::method($method, ['POST', 'PUT', 'PATCH'], FapiController::updateById($requestData, $table));
                        }
                        break;

                    case 'update':
                    case 'edit':
                    case 'change':
                        $rules = ValidateController::getRules($db, $gate);
                        if ($valid = ValidateController::gate($requestData['data'], $db, $gate, $requestData['id'] ?? 0, $rules)) {
                            return $valid;
                        }
                        if ($user->can('access-' . $db . '-' . $gate) and $user->can('update-' . $db . '-' . $gate)) {
                            return FapiController::method($method, ['POST', 'PUT', 'PATCH'], FapiController::update($requestData, $table));
                        }
                        break;

                    case 'delById':
                    case 'deleteById':
                    case 'removeById':
                        if ($user->can('delete-' . $db . '-' . $gate . '-id')) {
                            return FapiController::method($method, 'DELETE', FapiController::deleteById($requestData, $table));
                        }
                        break;

                    case 'del':
                    case 'delete':
                    case 'remove':
                        if ($user->can('access-' . $db . '-' . $gate) and $user->can('delete-' . $db . '-' . $gate)) {
                            return FapiController::method($method, 'DELETE', FapiController::delete($requestData, $table));
                        }
                        break;

                    case 'sp':
                        if ($user->can('access-' . $db . '-' . $gate)) {
                            return FapiController::method($method, 'POST', FapiController::sp($db, $requestData));
                        }
                        break;

                    default:
                        return FapiController::response('error', 'operation not found', null, 404);
                        break;

                }
            }

            if ($user->email == 'guest') {
                return FapiController::response('error', 'unauthorization', null, 401);
            }

            return FapiController::response('error', 'forbidden action', null, 403);

        } catch (\Throwable $e) {
            if ($e->getMessage() == 'Call to a member function can() on null') {
                return FapiController::response('error', 'unauthorization', null, 401);
            }
            if ($e->getMessage() == 'Call to a member function contains() on array') {
                if ($user->email == 'guest') {
                    return FapiController::response('error', 'unauthorization', null, 401);
                }

                return FapiController::response('error', 'forbidden action', null, 403);
            }
            return FapiController::response('error', 'json data sent is not standard ' . $e->getMessage(), null, 400);
        }

    }
}
