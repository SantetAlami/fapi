<?php

namespace App\Http\Controllers;

use App\Http\Controllers\FapiController;
use App\Permission;
// use Illuminate\Http\Request;
use App\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    /**
     * Get permission data for table view create new and edit role permissions.
     *
     * @param  Int  $idRole
     * set idRole zero "0" for create view (just get managing value without permitActive value)
     * @param  Int  $level
     * @param  Array  $group
     * @return \Illuminate\Http\Response
     */

    public static function getPermissions($idUserRole, $idRole, $level, $group)
    {
        try {
            $role = Role::findOrFail($idUserRole);
            if ($role->level < $level) {
                return FapiController::response('error', 'not enough role level', null, 403);
            } elseif (!in_array($role->group, $group) && !in_array('su', $group)) {
                return FapiController::response('error', 'your role don\'t have authority', null, 403);
            }

            $res['manages'] = DB::table('view_permissions')->whereIn('manage', Auth::user()->getManagePermit())->orderBy('staff')->orderBy('title')->get();

            if($idRole != ''){
                $IdPermits = DB::table('role_has_permissions')->where('role_id', $idRole)->pluck('permission_id');
                $permits = DB::table('permissions')->whereIn('id', $IdPermits)->pluck('name');
                $res['permitActive'] = $permits;
            }


            return FapiController::response('success', 'get all permission data', $res, 200);
        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    /**
     * Get roles data for showing master role.
     *
     * @param  Array  $request
     * @param  Int  $level
     * @param  Array  $group
     * @return Array
     * @return \Illuminate\Http\Response
     * */

    public static function read($level, $group, $cek = false)
    {
        try {
            if (!in_array('su', $group)) {
                $res = DB::table('roles')->where('level', '>', $level)->whereIn('group', $group)->get();
            } else {
                $res = DB::table('roles')->where('level', '>', $level)->get();
            }

            if ($cek) {
                return $res->pluck('name')->toArray();
            }
            return FapiController::response('success', 'get all role data', $res, 200);
        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  Array  $request
     * @param  Int  $level
     * @param  Array  $group
     * @return \Illuminate\Http\Response
     */
    public static function store($request, $level, $group)
    {
        // $this->validate($request, ['name' => 'required|unique:roles']);
        // if ($request->user()->can('create-role')) {
        // dd($request->user()->level());
        try {
            if($request['name'] == 'su'){
                return FapiController::response('error', 'su is super admin and can\'t add more', null, 403);
            }

            if ($request['level'] <= $level) {
                return FapiController::response('error', 'not enough role level', null, 403);
            } elseif (!in_array($request['group'], $group) && !in_array('su', $group)) {
                return FapiController::response('error', 'your role don\'t have authority', null, 403);
            }

            $role = Role::create(['name' => $request['name'], 'guard_name' => 'api', 'level' => $request['level'], 'group' => $request['group'], 'tag' => $request['tag']]);

            $manages = array_cut_value(array_search_like(Auth::user()->getManagePermit(), 'manage-%'), 'manage-');

            $permissions = [];
            foreach ($manages as $key => $value) {
                $permissions[] = array_search_like($request['permissions'], '%' . $value);
            }

            $role->givePermissionTo($permissions);

            // return response()->json($role);
            return FapiController::response('success', 'the role has successfully created', $role, 201);
        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
        // }else{
        //     return abort(404);
        // }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Array  $request
     * @param  Int  $id
     * @param  Int  $level
     * @param  Array  $group
     * @param  Bool  $manage
     * user can manage role
     * @return \Illuminate\Http\Response
     */
    public static function update($id, $request, $level, $group, $manage)
    {
        // if ($request->user()->can('edit-role')) {
        try {
            if ($role = Role::findOrFail($id)) {
                if ($role->level <= $level || $request['level'] <= $level) {
                    return FapiController::response('error', 'not enough role level', null, 403);
                } elseif (!in_array($role->group, $group) && !in_array('su', $group)) {
                    return FapiController::response('error', 'your role don\'t have authority', null, 403);
                }

                // super admin role has everything
                if ($role->name === 'super admin') {
                    $role->syncPermissions(Permission::all());
                    // return response()->json($role);
                    return FapiController::response('success', 'data has successfully updated', $role, 200);
                }
                // if ($role->name === 'user') {
                //     $role->revokePermissionTo(Permission::all());
                //     return response()->json($role);
                // }

                $update = ['name' => $request['name'], 'level' => $request['level']];

                if($manage){
                    $update = ['name' => $request['name'], 'level' => $request['level'], 'group' => $request['group'], 'tag' => ($request['tag'] ?? '')];
                }

                $role->update($update);

                $manages = array_cut_value(array_search_like(Auth::user()->getManagePermit(), 'manage-%'), 'manage-');

                $permissions = [];
                foreach ($manages as $key => $value) {
                    $permissions[] = array_search_like($request['permissions'], '%' . $value);
                }
                $role->syncPermissions($permissions); // tolong abis ini di pluck name aja

                return FapiController::response('success', 'data has successfully updated', $role, 200);
            } else {
                // return response()->json(['error' => 'Role with id ' . $id . ' note found', 404]);
                return FapiController::response('error', 'role with id ' . $id . ' not found', null, 404);
            }
        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
        // }else{
        //     return abort(404);
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Int  $id
     * @param  Int  $level
     * @param  Array  $group
     * @return \Illuminate\Http\Response
     */
    public static function destroy($id, $level, $group)
    {
        try {
            if ($role = Role::findOrFail($id)) {
                if ($role->level <= $level) {
                    return FapiController::response('error', 'not enough role level', null, 403);
                } elseif (!in_array($role->group, $group) && !in_array('su', $group)) {
                    return FapiController::response('error', 'your role don\'t have authority', null, 403);
                }

                // admin role has everything
                if ($role->name === 'super admin') {
                    $role->syncPermissions(Permission::all());
                    // return response()->json(['error' => 'super admin can\'t be deleted.', 403]);
                    return FapiController::response('error', 'super admin can\'t be delete', null, 403);
                }
                // if ($role->name === 'user') {
                //     // $role->revokePermissionTo(Permission::all());
                //     // return response()->json(['error' => 'user can\'t be deleted.', 403]);
                //     return FapiController::response('error', 'user  can\'t be delete', null, 403);
                // }
                // if(\$role->user()->hasRole($role->name)){
                //     return response()->json(['error'=>'You can\'t delete your role.', 403]);
                // }

                $role->revokePermissionTo(Permission::all());
                $role->delete();
                // return response()->json(['deleted' => 'deleted']);
                return FapiController::response('success', 'data has successfully deleted', null, 200);
            } else {
                // return response()->json(['error' => 'Role with id ' . $id . ' note found', 404]);
                return FapiController::response('error', 'role with id ' . $id . ' not found', null, 404);
            }
        } catch (\Throwable $e) {
            if ($e->getMessage() == 'No query results for model [App\\Role] ' . $id) {
                return FapiController::response('error', 'role with id ' . $id . ' not found', null, 404);
            }

            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }
}
