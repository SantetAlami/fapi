<?php

namespace App\Http\Controllers;

use App\Http\Controllers\FapiController;
use App\Http\Controllers\LogController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

// use App\Events\MessageSent;

class ApiGate2_1Controller extends Controller
{

    /**
     * Start Future Application Programming Interface.
     *
     * @param  Request  $request
     * @param  String  $db
     * @return Response
     */

    public function start(Request $request, $params)
    {
        $requestData = [];

        try {
            if (empty($request->user())) {
                $credentials = ["email" => 'guest', "password" => '1234'];
                Auth::attempt($credentials);
            }

            $expiresAt = Carbon::now()->addMinutes(1);
            Cache::put('user-is-online-' . $request->user()->id, true, $expiresAt);
            // event(new MessageSent('UserOnline', $message));

        } catch (\Throwable $th) {
        }

        $requestData = $request->all();

        $params = str_replace('/', '\\', $params);

        $future = $this->connection($requestData, $params, $request->user(), $request->method());

        return response()->json($future['res'], $future['code']);
    }

    /**
     * Start Future Connection Application Programming Interface.
     *
     * @param  Array  $request
     * @param  String  $db
     * @param  Object  $user
     * @return Response
     */

    public static function connection($requestData, $params, $user, $method)
    {
        // dd($requestData);
        LogController::user($requestData, 'sys', $user['id']);

        $paramArr = explode("\\", $params);
        $db = current($paramArr);
        $operation = end($paramArr);
        $operation_master = $paramArr[(count($paramArr) - 1) - 1];
        try {
            $namespace = 'App\Http\Controllers\\';
            $controller = $namespace . str_replace($operation, ucfirst($operation), $params);

            // dd($paramArr);
            // dd('access-sys-' . current($paramArr) . '-' . end($paramArr));
            if ($user->can('access-sys-' . $db . '-' . $operation) || $user->can('access-sys-' . $db . '-' . $operation . $operation_master)) {
                // dd($paramArr);
                $res = FapiController::method($method, 'POST', app($controller)->run($db, $requestData));

                if(!isset($res['status'])){
                    return $res;
                }
                return FapiController::response($res['status'], $res['message'], $res['data'], isset($res['error']) ? 409 : 200);
            }

            if ($user->email == 'guest') {
                return FapiController::response('error', 'unauthorization', null, 401);
            }

            return FapiController::response('error', 'forbidden action', null, 403);

        } catch (\Throwable $e) {
            if ($e->getMessage() == 'Call to a member function can() on null') {
                return FapiController::response('error', 'unauthorization', null, 401);
            }
            if ($e->getMessage() == 'Call to a member function contains() on array') {
                if ($user->email == 'guest') {
                    return FapiController::response('error', 'unauthorization', null, 401);
                }

                return FapiController::response('error', 'forbidden action', null, 403);
            }
            return FapiController::response('error', 'json data sent is not standard ' . $e->getMessage(), null, 400);
        }

    }
}
