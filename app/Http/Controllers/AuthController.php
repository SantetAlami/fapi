<?php

namespace App\Http\Controllers;

use App\Events\QRLogin;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\UserController;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    /**
     * Store a new user.
     *
     * @param  Object  $request
     * @return Response
     */
    public static function register($request)
    {
        //validate incoming request
        // $this->validate($request, [
        //     'name' => 'required|string',
        //     'email' => 'required|email|unique:users',
        //     'password' => 'required|confirmed',
        // ]);

        try {
            $user = new User;
            $user->name = $request['name'];
            $user->username = $request['username'];
            $user->email = $request['email'];
            $hashedPassword = app('hash')->make($request['password']);
            $user->password = $hashedPassword;
            $user->assignRole('guest'); //public register save as common user as default role
            $user->save();

            $credentials = ["username" => $request['username'], "password" => $hashedPassword];

            if ($token = Auth::attempt($credentials)) {
                $res = Auth::user();
                $res = UserController::profile(["retailpos"], true);
                $res->token = $token;
            }

            if (isset($request['staff'])) {
                foreach ($request['staff'] as $key => $item) {
                    $item->id = $res->id;
                    DB::table($key . '.users')->insertOrIgnore($item);
                }
            }

            //return successful response
            // return response()->json(['user' => $user, 'message' => 'User Registration'], 201);
            return FapiController::response('success', 'the user has successfully registered', $user, 201);

        } catch (\Throwable $e) {
            //return error message
            // return response()->json(['message' => 'User Registration Failed!'], 409);
            return FapiController::response('error', 'user registration failed ' . $e->getMessage(), null, 409);
        }

    }

    /**
     * Get a JWT via given credentials via qr.
     *
     * @param  Object  $request
     * @return Response
     */
    public static function getLoginQR()
    {

        try {
            return FapiController::response('success', 'get login qr success', (string) Str::uuid(), 200);
        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    /**
     * Get a JWT via given credentials via qr.
     *
     * @param  Object  $request
     * @return Response
     */
    //39f9bd9e-7e51-4c7b-875a-3fb6fe9924a4
    //eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hdXRoLWJvYXJkY2FzdC50ZXN0XC9hcGlcL2ZhcGlcL255YW4xXC9hdXRoIiwiaWF0IjoxNTk5NDUwNzg2LCJleHAiOjE2MzA5ODY3ODYsIm5iZiI6MTU5OTQ1MDc4NiwianRpIjoiVFM2N2dXb2c2UEVUNWVBMiIsInN1YiI6MSwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.hMIGx99_xAHMPnB5hnlIDeJE509CphdJPERopQgLB_Y
    public static function loginQRScan($request)
    {

        try {
            event(new QRLogin($request['uid'], $request['token']));

            return FapiController::response('success', 'login qr success', null, 200);
        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param  Object  $request
     * @return Response
     */

    public static function login($request)
    {
        //validate incoming request
        // $this->validate($request, [
        //     'data.email' => 'required|string',
        //     'data.password' => 'required|string',
        // ]);

        try {
            $field_no_1 = $request['email'] ?? $request['username'];
            $fieldType = filter_var($field_no_1, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

            $credentials = [$fieldType => $field_no_1, "password" => $request['password']];

            // $token = Auth::attempt($credentials);
            // if (!$token = Auth::attempt($credentials)) {
            // return response()->json(['message' => 'Unauthorized'], 401);
            // }

            // return $this->respondWithToken($token);
            if ($token = Auth::attempt($credentials)) {
                $res = Auth::user();
                $res = UserController::profile($request['staff'] ?? [], true);
                $res->token = $token;

                if (isset($request['staff'])) {
                    foreach ($request['staff'] as $key => $value) {
                        if (empty($res->$value)) {
                            return FapiController::response('error', 'email and password combination does not match', null, 401);
                        }
                    }
                }
                // $permit = Auth::user()->getAllpermissionsAttribute();
                // unset($res->permissions);
                // $res->role = $res->roles[0]->name;
                // $res->level = $res->roles[0]->level;
                // unset($res->roles);
                // $res->permissions = $permit;

                return FapiController::response('success', 'login success', $res, 200);
            }
            return FapiController::response('error', 'email and password combination does not match', null, 401);
        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    public static function logout()
    {
        Auth::logout();

        return FapiController::response('success', 'Successfully logged out', null, 200);
    }
}
