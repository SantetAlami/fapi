<?php

namespace App\Http\Controllers;

use App\Http\Controllers\RoleController;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    // public static function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Get the authenticated User.
     *
     * @return Response
     */
    public static function profile($db = [], $json = false)
    {
        try {
            $res = Auth::user();
            // $res->id = $res->id;
            // return response()->json(['user' => Auth::user()], 200);
            $permit = Auth::user()->getAllpermissionsAttribute($db);
            unset($res->permissions);
            $roles = $res->roles[0];
            unset($res->roles);
            $res->role = $roles->name;
            $res->level = $roles->level;
            $res->group = $roles->group;
            $res->tag = $roles->tag;

            if ($db != []) {
                try {
                    foreach ($db as $key => $value) {
                        $additional = DB::table($value . '.users')->where('id', Auth::user()->id)->first();
                        unset($additional->id);
                        $res->$value = $additional;
                        foreach ($additional as $index => $item) {
                            if (substr($index, -3) == '_id') {
                                $addtbl = rtrim($index, "_id");
                                try {
                                    $res->$value->$addtbl = DB::table($value . ".v_" . $addtbl)->where(["id" => $item])->first();
                                } catch (\Throwable $th) {
                                    $select = DB::table("information_schema.COLUMNS")->where(["TABLE_SCHEMA" => $value, "TABLE_NAME" => $addtbl])->where("COLUMN_COMMENT", "NOT LIKE", "%hidden pillar%")->pluck("COLUMN_NAME")->toArray();
                                    $res->$value->$addtbl = DB::table($value . "." . $addtbl)->selectRaw(implode(",", $select))->where(["id" => $item])->first();
                                }
                            }
                        }
                    }
                } catch (\Throwable $th) {
                    // return FapiController::response('error', $th->getMessage(), null, 401);
                }
            }

            $res->permissions = $permit;
            $res->status = (Cache::has('user-is-online-' . $res->id)) ? 'online' : 'offline';
            if ($json) {
                return $res;
            }
            return FapiController::response('success', 'see profile', $res, 200);
        } catch (\Throwable $th) {
            return FapiController::response('error', 'unauthorization', null, 401);
        }
    }

    /**
     * Get all User.
     *
     * @return Response
     */
    public function allUsers()
    {
        return response()->json(['users' => User::all()], 200);
    }

    /**
     * Get one user.
     *
     * @return Response
     */
    public function singleUser($id)
    {
        try {
            $user = User::findOrFail($id);

            return response()->json(['user' => $user], 200);

        } catch (\Throwable $th) {

            return response()->json(['message' => 'user not found!'], 404);
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @param Array $request
     * @param  Array  $currentUser
     * @return \Illuminate\Http\Response
     */
    public static function index($request, $currentUser)
    {
        try {
            $roles = RoleController::read($currentUser['level'], $currentUser['roleGroup'], true);
            $users = User::with(['roles']);

            if (isset($request['select'])) {
                if (is_array($request['select'])) {
                    $select = $request['select'];
                } else {
                    $select = [];
                    foreach (explode(',', $request['select']) as $key => $value) {
                        $select[] = ltrim(rtrim($value));
                    }
                }

                $users->select($select);
            }

            $users = FapiController::where($request, $users);

            if (isset($request['and'])) {
                $and = $request['and'];
                $users->where(function ($q) use ($and) {
                    $q = FapiController::where($and, $q);
                });
            }

            if (isset($request['or'])) {
                $or = $request['or'];
                $users->orWhere(function ($q) use ($or) {
                    $q = FapiController::where($or, $q);
                });
            }

            $res = $users->whereHas("roles", function ($q) use ($roles) {$q->whereIn("name", $roles);})->get();

            foreach ($res as $key => $value) {
                if (isset($request['staff'])) {
                    try {
                        foreach ($request['staff'] as $val) {
                            $additional = DB::table($val . '.users')->where('id', $value->id)->first();
                            unset($additional->id);
                            $res[$key][$val] = $additional;
                        }
                    } catch (\Throwable $th) {
                        // return FapiController::response('error', $th->getMessage(), null, 401);
                    }
                }

                $res[$key]['status'] = (Cache::has('user-is-online-' . $value->id)) ? 'online' : 'offline';
            }

            return FapiController::response('success', 'get all user data', $res, 200);

        } catch (\Throwable $th) {
            return FapiController::response('error', $th->getMessage(), null, 409);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Array  $request
     * @param  Array  $currentUser
     * [level,roleGroup]
     * @return \Illuminate\Http\Response
     */
    public static function store($request, $currentUser)
    {
        try {
            $roles = RoleController::read($currentUser['level'], $currentUser['roleGroup'], true);

            if ($request['password'] !== $request['password_confirmation']) {
                return FapiController::response('error', 'password and password confirmation must same', null, 400);
            }

            // return [$request['role'], $roles];
            if (in_array($request['role'], $roles)) {
                $user = new User;
                $user->name = $request['name'];
                $user->username = $request['username'];
                $user->email = $request['email'];
                $plainPassword = $request['password'];
                $user->password = app('hash')->make($plainPassword);
                $user->assignRole($request['role']);

                $additional = $request;
                try {
                    if ($request['photo']->getClientOriginalName() != "") {
                        $request['photo']->move('users', (string) Str::uuid() . $request['photo']->getClientOriginalName());
                        $user->photo = (string) Str::uuid() . $request['photo']->getClientOriginalName();
                        unset($additional['photo']);
                    }
                } catch (\Throwable $th) {}
                $user->save();

                unset($additional['name']);
                unset($additional['username']);
                unset($additional['email']);
                unset($additional['password']);
                unset($additional['password_confirmation']);
                unset($additional['role']);
                if ($additional) {
                    foreach ($additional as $key => $value) {
                        if (is_array($value)) {
                            $req = $value;
                            $req['id'] = $user->id;
                            DB::table($key . '.users')->insert($req);
                        }
                    }
                }

                return FapiController::response('success', 'the user has successfully registered', $user, 201);
            }
            return FapiController::response('error', 'undefinded selected role', null, 400);

        } catch (\Throwable $th) {
            //return error message
            return FapiController::response('error', 'user registration failed ' . $th->getMessage(), null, 409);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Int $id
     * @param  Array  $request
     * @param  Array  $currentUser
     * @param  Bool $isMe
     * [level,roleGroup]
     * @return \Illuminate\Http\Response
     */
    public static function update($id, $request, $currentUser, $isMe = false)
    {
        try {
            if (!empty($request['password'])) {
                if ($request['password'] !== $request['password_confirmation']) {
                    return FapiController::response('error', 'password and password confirmation must same', null, 400);
                }
                $plainPassword = $request['password'];
                $request['password'] = app('hash')->make($plainPassword);
            }

            $user = new User;
            if ($isMe) {
                $res = $user->where('id', Auth::user()->id)->first();

                if (app('hash')->check($request['old_password'], $res->password)) {

                    $additional = $request;
                    try {
                        if ($request['photo']->getClientOriginalName() != "") {
                            if (File::exists('main/users/' . $res->photo)) {
                                File::delete('main/users/' . $res->photo);
                            }
                            $request['photo']->move('main/users', (string) Str::uuid() . $request['photo']->getClientOriginalName());
                            $res->photo = (string) Str::uuid() . $request['photo']->getClientOriginalName();
                            unset($additional['photo']);
                        }
                    } catch (\Throwable $th) {}
                    $res->update($request);

                    unset($additional['name']);
                    unset($additional['username']);
                    unset($additional['email']);
                    unset($additional['old_password']);
                    unset($additional['password']);
                    unset($additional['password_confirmation']);
                    unset($additional['role']);
                    if ($additional) {
                        foreach ($additional as $key => $value) {
                            if (is_array($value)) {
                                DB::table($key . '.users')->updateOrInsert(['id' => Auth::user()->id], $value);
                            }
                        }
                        $res = UserController::profile(array_keys($additional), true);
                    }

                    return FapiController::response('success', 'profile has successfully updated', $res, 200);
                }

                return FapiController::response('error', 'old password wrong', [], 406);

            } else {
                $roles = RoleController::read($currentUser['level'], $currentUser['roleGroup'], true);
                $res = $user->where('id', $id)->whereHas("roles", function ($q) use ($roles) {$q->whereIn("name", $roles);})->first();
                if (in_array($request['role'], $roles) && !$isMe) {

                    $res->syncRoles([$request['role']]);

                    $additional = $request;
                    try {
                        if ($request['photo']->getClientOriginalName() != "") {
                            if (File::exists('main/users/' . $res->photo)) {
                                File::delete('main/users/' . $res->photo);
                            }
                            $request['photo']->move('main/users', (string) Str::uuid() . $request['photo']->getClientOriginalName());
                            $res->photo = (string) Str::uuid() . $request['photo']->getClientOriginalName();
                            unset($additional['photo']);
                        }
                    } catch (\Throwable $th) {}
                    $res->update($request);

                    unset($additional['name']);
                    unset($additional['username']);
                    unset($additional['email']);
                    unset($additional['password']);
                    unset($additional['password_confirmation']);
                    unset($additional['role']);
                    if ($additional) {
                        foreach ($additional as $key => $value) {
                            if (is_array($value)) {
                                DB::table($key . '.users')->updateOrInsert(['id' => $id], $value);
                            }
                            // $res = UserController::profile(array_keys($additional), true);
                        }
                    }

                    return FapiController::response('success', 'user has successfully updated', $res, 200);
                }
                return FapiController::response('error', 'undefinded selected role', null, 400);
            }

        } catch (\Throwable $th) {
            return FapiController::response('error', 'unauthorization ' . $th->getMessage(), null, 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Int $id
     * @param  Array  $currentUser
     * @return \Illuminate\Http\Response
     */
    public static function destroy($id, $currentUser)
    {
        try {
            $roles = RoleController::read($currentUser['level'], $currentUser['roleGroup'], true);

            $user = new User;
            $user->where('id', $id)->whereHas("roles", function ($q) use ($roles) {$q->whereIn("name", $roles);})->delete();

            return FapiController::response('success', 'user has successfully deleted', null, 200);

        } catch (\Throwable $th) {
            return FapiController::response('error', $th->getMessage(), null, 409);
        }
    }

}
