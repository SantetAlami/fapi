<?php

namespace App\Http\Controllers;

use App\Http\Controllers\FapiController;
use Illuminate\Support\Facades\DB;

class LogController extends Controller
{
    /**
     * Store a new user.
     *
     * @param  Object  $request
     * @return Response
     */
    public static function user($requestData, $db, $user_id)
    {
        try {
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) //check ip from share internet
            {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) //to check ip is pass from proxy
            {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }

            $setLog = ['user_id' => $user_id, 'staff' => $db, 'gate' => $requestData['gate'], 'operation' => $requestData['operation'], 'ip' => $ip];
            DB::table('user_logs')->insert($setLog);

        } catch (\Throwable $e) {
            return FapiController::response('error', 'who are you ?', null, 403);
        }

    }

}
