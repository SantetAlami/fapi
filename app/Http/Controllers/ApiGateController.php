<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\RoleController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

// use App\Events\MessageSent;

class ApiGateController extends Controller
{

    /**
     * Start Future Application Programming Interface.
     *
     * @param  Request  $request
     * @param  String  $db
     * @return Response
     */

    public function start(Request $request, $db = null, $staff = null, $method = 'post')
    {
        $requestData = $request->all();

        try {
            if (empty($request->user())) {
                $credentials = ["email" => 'guest', "password" => '1234'];
                Auth::attempt($credentials);
            }

            $expiresAt = Carbon::now()->addMinutes(1);
            Cache::put('user-is-online-' . $request->user()->id, true, $expiresAt);
            // event(new MessageSent('UserOnline', $message));

        } catch (\Throwable $th) {
        }

        if (isset($requestData[0])) {
            $res = [];
            $code = [];
            foreach ($requestData as $key => $value) {
                $future = $this->connection($value, $db ?? $value['staff'] ?? 'auth', $staff, $request->user());
                $res[] = $future['res'];
                $code[] = $future['code'];
            }
            return response()->json($res, $code[0]);

        } else {
            $requestData = $request->all();
            $future = $this->connection($requestData, $db, $staff, $request->user());
            // return response()->json($future['res'], $future['code'])
            //     ->header('Link', '; rel=preload; as=style', false)
            //     ->header('Link', '; rel=preload; as=script', false);
            return response($future['res'], $future['code'])
                ->header('Link', '; rel=preload; as=style', false)
                ->header('Link', '; rel=preload; as=script', false);

        }
    }

    /**
     * Start Future Connection Application Programming Interface.
     *
     * @param  Array  $request
     * @param  String  $db
     * @param  Object  $user
     * @return Response
     */

    public static function connection($requestData, $db, $staff, $user)
    {
        // $requestData = $request->all();
        LogController::user($requestData, $db, $user['id']);

        try {
            if ($db == 'auth') {
                switch ($requestData['gate']) {
                    case 'auth':
                        switch ($requestData['operation']) {
                            case 'getQR':
                                return AuthController::getLoginQR();
                                break;
                            case 'loginQRScan':
                                return AuthController::loginQRScan($requestData['data']);
                                break;
                            case 'login':
                                return AuthController::login($requestData['data']);
                                break;
                            case 'register':
                                return AuthController::register($requestData['data']);
                                break;
                            case 'logout':
                                return AuthController::logout();
                                break;

                            default:
                                return FapiController::response('error', 'operation not found', null, 404);
                                break;
                        }
                        break;

                    case "role":
                        switch ($requestData['operation']) {
                            case 'getPermission':
                            case 'getPermissions':
                                if ($user->can('read-roles')) {
                                    if (isset($requestData['id'])) {
                                        return RoleController::getPermissions($user->idRole(), $requestData['id'], $user->level(), $user->roleGroup());
                                    }
                                    return RoleController::getPermissions($user->idRole(), '', $user->level(), $user->roleGroup());
                                }
                                break;
                            case 'get':
                            case 'read':
                            case 'getRole':
                                if ($user->can('read-roles')) {
                                    return RoleController::read($user->level(), $user->roleGroup());
                                }
                                break;
                            case 'add':
                            case 'create':
                            case 'addRole':
                            case 'createRole':
                                if ($user->can('create-roles')) {
                                    return RoleController::store($requestData['data'], $user->level(), $user->roleGroup());
                                }
                                break;
                            case 'edit':
                            case 'update':
                            case 'updateRole':
                                if ($user->can('update-roles')) {
                                    if ($user->can('manage-roles')) {
                                        return RoleController::update($requestData['id'], $requestData['data'], $user->level(), $user->roleGroup(), true);
                                    }
                                }
                                break;
                            case 'delete':
                            case 'destroy':
                            case 'deleteRole':
                            case 'destroyRole':
                                if ($user->can('delete-roles')) {
                                    return RoleController::destroy($requestData['id'], $user->level(), $user->roleGroup());
                                }
                                break;

                            default:
                                return FapiController::response('error', 'operation ' . $requestData['operation'] . ' not found', null, 404);
                                break;
                        }
                        break;

                    case "user":
                        $rules = [
                            "name" => [
                                "required" => "1",
                            ],
                            "username" => [
                                "required" => "1",
                                "unique" => "1",
                            ],
                            "email" => [
                                "required" => "1",
                                "email" => "1",
                                "unique" => "1",
                            ],
                            "password" => [
                                "required" => "1",
                                "math" => "=,password_confirm",
                            ],
                            "password_confirm" => [
                                "required" => "1",
                                "math" => "=,password",
                            ],
                        ];
                        switch ($requestData['operation']) {
                            case 'profil':
                            case 'profile':
                                return UserController::profile($requestData['data'] ?? []);
                                break;
                            case 'editProfil':
                            case 'editProfile':
                                unset($rules['password']);
                                unset($rules['password_confirm']);
                                if ($valid = ValidateController::gate($requestData['data'], env('DB_DATABASE'), 'users', $user['id'], $rules)) {
                                    return $valid;
                                }

                                return UserController::update("", $requestData['data'], "", true);
                                break;
                            case 'add':
                            case 'create':
                            case 'insert':
                                if ($user->can('create-users')) {
                                    if ($valid = ValidateController::gate($requestData['data'], env('DB_DATABASE'), 'users', 0, $rules)) {
                                        return $valid;
                                    }

                                    return UserController::store($requestData['data'], ["level" => $user->level(), "roleGroup" => $user->roleGroup()]);
                                }
                                break;
                            case 'get':
                            case 'read':
                            case 'search':
                                if ($user->can('read-users')) {
                                    return UserController::index($requestData['data'], ["level" => $user->level(), "roleGroup" => $user->roleGroup()]);
                                }
                                break;
                            case 'update':
                            case 'edit':
                            case 'change':
                                if ($user->can('update-users')) {
                                    unset($rules['password']);
                                    unset($rules['password_confirm']);
                                    if ($valid = ValidateController::gate($requestData['data'], env('DB_DATABASE'), 'users', $requestData['id'], $rules)) {
                                        return $valid;
                                    }
                                    return UserController::update($requestData['id'], $requestData['data'], ["level" => $user->level(), "roleGroup" => $user->roleGroup()]);
                                }
                                break;
                            case 'del':
                            case 'delete':
                            case 'remove':
                                if ($user->can('delete-users')) {
                                    return UserController::destroy($requestData['id'], ["level" => $user->level(), "roleGroup" => $user->roleGroup()]);
                                }
                                break;

                            default:
                                return FapiController::response('error', 'operation ' . $requestData['operation'] . ' not found', null, 404);
                                break;
                        }
                        break;

                    default:
                        return FapiController::response('error', 'gate ' . $requestData['gate'] . ' not found', null, 404);

                        break;
                }
            } else if ($db == 'sys') {
                // $app = new \Laravel\Lumen\Application(
                //     dirname(__DIR__)
                // );
                $namespace = 'App\Http\Controllers\\';
                $controller = $namespace . $requestData['gate'] . 'Controller';
                // return app($controller)->{$requestData['operation']}($requestData['data']);
                // try {
                if ($user->can('access-' . (empty($staff) ? "sewa_ban" : $staff) . '-' . $requestData['operation'])) {
                    return app($controller)->operation($requestData['operation'], $requestData['data'], $staff);
                }
                // } catch (\Throwable $th) {
                //     return app($controller)->{$requestData['operation']}($requestData['data']);
                // }

                // return $controller->callAction($requestData['operation'], $requestData);
                // if ($user->can('access-' . $db . '-' . $requestData['gate'] . '-' . $requestData['operation'])) {
                // $controller = $requestData['gate'] . 'Controller';
                // $classname = 'App\\Http\\Controllers\\' . $requestData['gate'] . 'Controller';
                // $class = new $classname();
                // return $class::call_user_func($requestData['operation'], $requestData);
                // return $app->call($classname . '@' . $requestData['operation'], $requestData);
                // }
            } else {

                $table = DB::table($db . '.' . $requestData['gate']);

                $gate = (substr($requestData['gate'], 0, 2) == 'v_') ? str_replace("v_", "", $requestData['gate']) : $requestData['gate'];

                // if ($user->can('access-' . $db . '-' . $gate)) {
                switch ($requestData['operation']) {
                    case 'add':
                    case 'create':
                    case 'insert':
                        $rules = ValidateController::getRules($db, $requestData['gate']);
                        if ($valid = ValidateController::gate($requestData['data'], $db, $requestData['gate'], $requestData['id'] ?? 0, $rules)) {
                            return $valid;
                        }
                        if ($user->can('create-' . $db . '-' . $requestData['gate'])) {
                            return FapiController::insert($requestData, $table);
                        }
                        break;

                    case 'getById':
                    case 'readById':
                    case 'searchById':
                        if ($user->can('read-' . $db . '-' . $requestData['gate']) . '-id') {
                            return FapiController::readById($requestData, $table);
                        }
                        break;

                    case 'get':
                    case 'read':
                    case 'search':
                        if ($user->can('read-' . $db . '-' . $gate)) {
                            return FapiController::read($requestData, $table);
                        }
                        break;

                    case 'updateById':
                    case 'editById':
                    case 'changeById':
                        $rules = ValidateController::getRules($db, $requestData['gate']);
                        if ($valid = ValidateController::gate($requestData['data'], $db, $requestData['gate'], $requestData['id'] ?? 0, $rules)) {
                            return $valid;
                        }
                        if ($user->can('update-' . $db . '-' . $requestData['gate']) . '-id') {
                            return FapiController::update($requestData, $table);
                        }
                        break;

                    case 'update':
                    case 'edit':
                    case 'change':
                        $rules = ValidateController::getRules($db, $requestData['gate']);
                        if ($valid = ValidateController::gate($requestData['data'], $db, $requestData['gate'], $requestData['id'] ?? 0, $rules)) {
                            return $valid;
                        }
                        if ($user->can('update-' . $db . '-' . $requestData['gate'])) {
                            return FapiController::update($requestData, $table);
                        }
                        break;

                    case 'delById':
                    case 'deleteById':
                    case 'removeById':
                        if ($user->can('delete-' . $db . '-' . $requestData['gate']) . '-id') {
                            return FapiController::delete($requestData, $table);
                        }
                        break;

                    case 'del':
                    case 'delete':
                    case 'remove':
                        if ($user->can('delete-' . $db . '-' . $requestData['gate'])) {
                            return FapiController::delete($requestData, $table);
                        }
                        break;

                    case 'sp':
                        if ($user->can('access-' . $db . '-' . $gate)) {
                            return FapiController::sp($db, $requestData);
                        }
                        break;

                    default:
                        return FapiController::response('error', 'operation not found', null, 404);
                        break;

                }
                // }
            }

            if ($user->email == 'guest') {
                return FapiController::response('error', 'unauthorization', null, 401);
            }

            return FapiController::response('error', 'forbidden action', null, 403);

        } catch (\Throwable $e) {
            if ($e->getMessage() == 'Call to a member function can() on null') {
                return FapiController::response('error', 'unauthorization', null, 401);
            }
            if ($e->getMessage() == 'Call to a member function contains() on array') {
                if ($user->email == 'guest') {
                    return FapiController::response('error', 'unauthorization', null, 401);
                }

                return FapiController::response('error', 'forbidden action', null, 403);
            }
            return FapiController::response('error', 'json data sent is not standard ' . $e->getMessage(), null, 400);
        }

    }
}
