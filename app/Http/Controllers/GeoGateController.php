<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// use Illuminate\Support\Facades\DB;

// use App\Events\MessageSent;

class GeoGateController extends Controller
{

    /**
     * Start Future Application Programming Interface.
     *
     * @param  Request  $request
     * @param  String  $db
     * @return Response
     */

    public function start(Request $request, $params)
    {
        try {
            $requestData = $request->all();
            $params = str_replace('/', '\\', $params);

            $paramArr = explode("\\", $params);
            $db = current($paramArr);
            $operation = end($paramArr);

            $namespace = 'App\Http\Controllers\\';
            $controller = $namespace . str_replace($operation, ucfirst($operation), $params);

            $res = ResponseController::method($request->method(), 'POST', app($controller)->run($db, $requestData));

            $data = (isset($res['res'])) ? $res['res'] : $res;

            return ResponseController::run(isset($data['status']) ? $data['status'] : (isset($data['success']) ? 'success' : 'error'), $data['message'], $data['data'], isset($data['error']) ? 409 : 200);
        } catch (\Throwable $e) {
            if (config('app.debug') == true) {
                return ResponseController::run('error', 'Debug mode ON: ' . $e->getMessage() . ", Line No : " . $e->getLine() . " : File Path : " . $e->getFile(), null, 400);
            }
            return ResponseController::run('error', 'json data sent is not standard ', null, 400);
        }

    }

}
