<?php

namespace App\Http\Controllers\las;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Kelengkapan extends Controller
{

    public static function run($db, $data)
    {
        // $data = [
        //     "pengajuan_id" => 2,
        //     "items" => [
        //         [22,"1"],
        //         [22,"1"],
        //         [22,"1"],
        //         [22,"1"],
        //     ]
        // ];

        $kelengkapan = "kelengkapan";
        $kelengkapan_jaminan = "kelengkapan_jaminan";
        $survey = "survey";

        try {
            $data['kelengkapan_id'] = DB::table($db . '.' . $kelengkapan)->where('pengajuan_id', $data['pengajuan_id'])->first()->id;
        } catch (\Throwable $th) {
            $data['kelengkapan_id'] = DB::table($db . '.' . $kelengkapan)->insertGetId(["pengajuan_id"=> $data['pengajuan_id'], "done" => "0"]);
            // return ['status' => 'error', 'message' => 'Tidak ada Data Pengajuan', 'data' => []];
        }

        foreach ($data['items'] as $item) {
            DB::table($db . '.' . $kelengkapan_jaminan)->updateOrInsert(
                ['kelengkapan_id' => $data['kelengkapan_id'], 'jaminan_list_id' => $item[0]],
                ['value' => $item[1]]
            );
        }

        $qdone = DB::select('CALL ' . $db . ".`sp_kelengkapan`('" . $data['pengajuan_id'] . "')");
        $cdone = 0;
        foreach ($qdone as $key => $item) {
            if (!empty($item->value)) {
                $cdone++;
            }
        }

        if ($cdone == count($qdone)) {
            DB::table($db . '.' . $kelengkapan)->where(['id' => $data['kelengkapan_id']])->update(['done' => 1, 'done_time' => date("Y-m-d H:i:s", Carbon::now()->timestamp)]);
            DB::table($db . '.' . $survey)->updateOrInsert(['pengajuan_id' => $data['pengajuan_id']], ['done' => 0]);
        }

        return ['status' => 'success', 'message' => 'Data Kelengkapan Terupdate', 'data' => []];

    }

}
