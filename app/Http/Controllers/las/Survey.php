<?php

namespace App\Http\Controllers\las;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Survey extends Controller
{

    public static function run($db, $data)
    {
        // $data = [
        //     "pengajuan_id" => 2,
        //     "character" => 20,
        //     "capacity" => 20,
        //     "capital" => 20,
        //     "collaterral" => 20,
        //     "condition" => 20,
        //     "items" => [
        //         [22,"1"],
        //         [22,"1"],
        //         [22,"1"],
        //         [22,"1"],
        //     ]
        // ];

        $survey = "survey";
        $survey_jaminan = "survey_jaminan";
        $komite_kredit = "komite_kredit";

        try {
            $data['survey_id'] = DB::table($db . '.' . $survey)->where('pengajuan_id', $data['pengajuan_id'])->first()->id;
        } catch (\Throwable $th) {
            return ['status' => 'error', 'message' => 'Tidak ada Data Pengajuan', 'data' => []];
        }

        foreach ($data['items'] as $item) {
            DB::table($db . '.' . $survey_jaminan)->updateOrInsert(
                ['survey_id' => $data['survey_id'], 'jaminan_list_id' => $item[0]],
                ['value' => $item[1]]
            );
        }

        $qdone = DB::select('CALL ' . $db . ".`sp_survey`('" . $data['pengajuan_id'] . "')");
        $cdone = 0;
        foreach ($qdone as $key => $item) {
            if (!empty($item->value)) {
                $cdone++;
            }
        }

        DB::table($db . '.' . $survey)->where(['id' => $data['survey_id']])->update([
            "character" => $data['character'] ?? 0,
            "capacity" => $data['capacity'] ?? 0,
            "capital" => $data['capital'] ?? 0,
            "collaterral" => $data['collaterral'] ?? 0,
            "condition" => $data['condition'] ?? 0]);

        if ($cdone == count($qdone)) {
            DB::table($db . '.' . $survey)->where(['id' => $data['survey_id']])->update([
                'done' => 1,
                'done_time' => date("Y-m-d H:i:s", Carbon::now()->timestamp)]);
            DB::table($db . '.' . $komite_kredit)->updateOrInsert(['pengajuan_id' => $data['pengajuan_id']], ['done' => 0]);
        }

        return ['status' => 'success', 'message' => 'Data Survey Terupdate', 'data' => []];

    }

}
