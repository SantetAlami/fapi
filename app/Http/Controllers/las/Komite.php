<?php

namespace App\Http\Controllers\las;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Komite extends Controller
{

    public static function run($db, $data)
    {
        // $data = [
        //     "pengajuan_id" => 2,
        //     "keputusan" => 1,
        // ];

        $komite_kredit = "komite_kredit";

        if ($data['keputusan'] == 1 || $data['keputusan'] == 0) {
            DB::table($db . '.' . $komite_kredit)->updateOrInsert(['pengajuan_id' => $data['pengajuan_id']], ['keputusan' => $data['keputusan'], 'done' => 1, 'done_time' => date("Y-m-d H:i:s", Carbon::now()->timestamp)]);
            return ['status' => 'success', 'message' => 'Data Komite_kredit Terupdate', 'data' => []];
        } else {
            return ['status' => 'error', 'message' => 'Data apa yang di kirim ?', 'data' => []];
        }

    }

}
