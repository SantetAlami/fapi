<?php

namespace App\Http\Controllers\las;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FapiController;

class GateController extends Controller
{

    public function operation($operation, $data, $db)
    {
        switch ($operation) {
            case 'kelengkapan':
                $res = Kelengkapan::run($db, $data);
                break;
            case 'survey':
                $res = Survey::run($db, $data);
                break;
            case 'komite':
                $res = Komite::run($db, $data);
                break;

            default:
                return FapiController::response('error', "operation $operation not found", [], 400);
                break;
        }

        return FapiController::response($res['status'], $res['message'], $res['data'], isset($res['error']) ? 409 : 200);

    }

}
