<?php

namespace App\Http\Controllers\ukmsys\multiple;

use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\ukmsys\users;
use Illuminate\Support\Facades\DB;

// use Carbon\Carbon;

class Penjualan extends Controller
{

    public static function run($db, $data)
    {
        $res = [];
        //get barang
        $barang = "v_barang";

        $toko_id = users::get($db, 'toko_id');
        $table = DB::table($db . '.' . $barang)->where(["toko_id" => $toko_id])->orderBy('kategori')->orderBy('nama');

        $requestData = ["data" => $data["barang"]];

        $res["barang"] = FapiController::read($requestData, $table)["res"]["data"];

        //get carabayar
        $carabayar = "carabayar";

        $toko_id = users::get($db, 'toko_id');
        $table = DB::table($db . '.' . $carabayar)->where(["toko_id" => $toko_id]);

        $requestData = ["data"=> $data["carabayar"]];

        $res["carabayar"] = FapiController::read($requestData, $table)["res"]["data"];

        //get diskon
        $diskon = "diskon";

        $toko_id = users::get($db, 'toko_id');
        $table = DB::table($db . '.' . $diskon)->where(["toko_id" => $toko_id]);

        $requestData = ["data"=> $data["diskon"]];

        $res["diskon"] = FapiController::read($requestData, $table)["res"]["data"];

        //get kategori
        $kategori = "v_kategori";

        $toko_id = users::get($db, 'toko_id');
        $table = DB::table($db . '.' . $kategori)->where(["toko_id" => $toko_id]);

        $requestData = ["data"=> $data["kategori"]];

        $res["kategori"] = FapiController::read($requestData, $table)["res"]["data"];

        //get pelanggan
        $pelanggan = "pelanggan";

        $toko_id = users::get($db, 'toko_id');
        $table = DB::table($db . '.' . $pelanggan)->where(["toko_id" => $toko_id]);

        $requestData = ["data"=> $data["pelanggan"]];

        $res["pelanggan"] = FapiController::read($requestData, $table)["res"]["data"];

        return ['status' => 'success', 'message' => 'Get multiple', 'data' => $res];

    }

}
