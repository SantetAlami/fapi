<?php

namespace App\Http\Controllers\ukmsys\transaksi;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ukmsys\users;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Pengeluaran extends Controller
{

    public static function run($db, $data)
    {
        // {
        //     "pokok_id": 23,
        //     "nominal": 150000,
        //     "tgl": "2020-01-01"
        // }

        $pokok = "pokok";
        $pengeluaran = "pengeluaran";

        $user_id = Auth::user()->id;
        $toko_id = users::get($db, 'toko_id');

        $pFaktur = "TP";

        $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;
        $_pokok = DB::table($db . '.' . $pokok)->where(["id" => $data["pokok_id"], "toko_id" => $toko_id])->first();

        DB::table($db . '.' . $pengeluaran)->insert([
            "tgl" => date("Ymd", Carbon::now()->timestamp),
            "faktur" => $faktur,
            "user_id" => $user_id,
            "toko_id" => $toko_id,
            "pokok_id" => $_pokok->id,
            "nominal" => $data["nominal"],
            "keterangan" => $data["keterangan"] ?? "",
        ]);

        return ['status' => 'success', 'message' => 'Transaksi baru telah di tersimpan', 'data' => []];

    }

}
