<?php

namespace App\Http\Controllers\ukmsys\transaksi\bahan;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\ukmsys\users;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Stokopname extends Controller
{

    public static function run($db, $data)
    {
        // {
        //     "bahan_id": 8,
        //     "tgl": "2021-01-01",
        //     "sisa": 300,
        //     "catatan": ""
        // }
        $stok_bahan = "stok_bahan";
        $bahan = "bahan";

        $pFaktur = "OB";
        $user_id = Auth::user()->id;
        $toko_id = users::get($db, 'toko_id');

        $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

        if (!DB::table($db . "." . $bahan)->where(["id" => $data["bahan_id"], "toko_id" => $toko_id])->exists()) {
            return FapiController::response('error', 'tidak ada data bahan', null, 404);
        }

        $sisa = DB::table($db . "." . $stok_bahan)->selectRaw("sum(masuk - keluar) as sisa")->where(["bahan_id" => $data["bahan_id"]])->where("tgl", "<=", $data["tgl"])->first()->sisa;

        DB::table($db . "." . $stok_bahan)->insert([
            "tgl" => $data["tgl"],
            "user_id" => $user_id,
            "bahan_id" => $data["bahan_id"],
            "faktur" => $faktur,
            "masuk" => ($sisa - $data["sisa"]) < 0 ? ($sisa - $data["sisa"]) * -1 : 0,
            "keluar" => ($sisa - $data["sisa"]) >= 0 ? $sisa - $data["sisa"] : 0,
            "sisa" => $data["sisa"],
            "catatan" => $data["catatan"],
        ]);

        $res = DB::table($db . "." . $stok_bahan)->selectRaw("tgl,max(sisa) as sisa,catatan")->where(["bahan_id" => $data["bahan_id"]])->where("tgl", "<=", date("Ymd", Carbon::now()->timestamp))->groupBy("tgl")->get();

        return FapiController::response('success', 'transaksi telah tersimpan', $res, 200);

    }

}
