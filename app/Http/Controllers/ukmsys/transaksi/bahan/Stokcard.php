<?php

namespace App\Http\Controllers\ukmsys\transaksi\bahan;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\ukmsys\users;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Stokcard extends Controller
{

    public static function run($db, $data)
    {

        $stok_bahan = "stok_bahan";
        $bahan = "bahan";

        $toko_id = users::get($db, 'toko_id');

        if (!DB::table($db . "." . $bahan)->where(["id" => $data["bahan_id"], "toko_id" => $toko_id])->exists()) {
            return FapiController::response('error', 'tidak ada data bahan', null, 404);
        }

        $res = DB::table($db . "." . $stok_bahan)->selectRaw("tgl,max(sisa) as sisa,catatan")->where(["bahan_id" => $data["bahan_id"]])->where("tgl", "<=", date("Ymd", Carbon::now()->timestamp))->groupBy("tgl")->get();

        return FapiController::response('success', 'get kartu stok', $res, 200);

    }

}
