<?php

namespace App\Http\Controllers\ukmsys\transaksi\bahan;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ukmsys\users;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class beli extends Controller
{

    public static function run($db, $data)
    {
        // {
        //     "items": [
        //         {
        //             "id": 1,
        //             "harga": 5000,
        //             "jumlah": 3
        //         },
        //         {
        //             "id": 2,
        //             "harga": 5000,
        //             "jumlah": 2
        //         }
        //     ],
        //     "catatan": ""
        // }

        $beli = "beli";
        $detail_beli = "detail_beli";
        $stok_bahan = "stok_bahan";

        $bahan = "bahan";

        $user_id = Auth::user()->id;
        $toko_id = users::get($db, 'toko_id');

        $pFaktur = "TB";

        $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

        //hitung total harga
        $total_harga = 0;
        $jumlah_bahan = 0;
        foreach ($data["items"] as $item) {
            if (DB::table($db . '.' . $bahan)->where(["id" => $item["id"], "toko_id" => $toko_id])->exists()) {
                $total_harga = $total_harga + $item["harga"];
                $jumlah_bahan++;
            } else {
                return ['status' => 'error', 'message' => 'Tidak ada data bahan', 'data' => []];
            }
        }

        // insert transaksi beli
        $trx_id = DB::table($db . '.' . $beli)->insertGetId([
            "tgl" => $data["tgl"] ?? date("Ymd", Carbon::now()->timestamp),
            "faktur" => $faktur,

            "harga" => $total_harga,
            "jumlah_item" => $jumlah_bahan,

            "user_id" => $user_id,
            "toko_id" => $toko_id,
            "catatan" => $item["catatan"] ?? "",
        ]);

        // insert detail trx
        foreach ($data["items"] as $item) {
            $bahan_res = DB::table($db . '.' . $bahan)->where(["id" => $item["id"], "toko_id" => $toko_id])->first();

            DB::table($db . '.' . $detail_beli)->insert([
                "beli_id" => $trx_id,
                "bahan_id" => $bahan_res->id,
                "nama" => $bahan_res->title,
                "faktur" => $faktur,
                "beli" => $item["harga"],
                "jumlah" => $item["jumlah"],
            ]);

            // update stok bahan

            $sisa = DB::table($db . "." . $stok_bahan)->selectRaw("sum(masuk - keluar) as sisa")->where(["bahan_id" => $item["id"]])->where("tgl", "<=", $data["tgl"])->first()->sisa;
            DB::table($db . '.' . $stok_bahan)->insert(["tgl" => $data["tgl"], "bahan_id" => $item["id"], "faktur" => $faktur, "masuk" => $item["jumlah"], "keluar" => 0, "sisa" => $sisa + $item["jumlah"], "catatan" => $data["catatan"] ?? ""]);
        }

        return ['status' => 'success', 'message' => 'Transaksi baru telah di tersimpan', 'data' => []];

    }

}
