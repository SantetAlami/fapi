<?php

namespace App\Http\Controllers\ukmsys\transaksi;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ukmsys\users;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Refund extends Controller
{

    public static function run($db, $data)
    {
        // {
        //     "faktur": "",
        //     "items": [
        //         {
        //             "id": 1,
        //             "harga": 5000,
        //             "jumlah":3
        //         },
        //         {
        //             "id":2,
        //             "jumlah":2
        //         }
        //     ],
        //     "kembali": [
        //         {
        //             "carabayar_id": 1,
        //             "nominal": 20000
        //         },
        //         {
        //             "carabayar_id": 2,
        //             "nominal": 5000
        //         }
        //     ],
        //     "catatan": ""
        // }
        $jual = "jual";
        $detail_jual = "detail_jual";
        // $barang = "barang";
        $refund_jual = "refund_jual";
        $detail_refund_jual = "detail_refund_jual";
        $bayar_refund_jual = "bayar_refund_jual";

        $trx_id = DB::table($db . '.' . $jual)->where(["faktur" => $data["faktur"]])->first()->id;

        $user_id = Auth::user()->id;
        $toko_id = users::get($db, 'toko_id');

        $pFaktur = "RJ";

        $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

        //hitung total harga
        $total_harga = 0;
        $jumlah_barang = 0;
        if ($toko_id) {
            //jika bukan dari toko
        }
        foreach ($data["items"] as $item) {
            try {
                $barangs = collect(DB::select('CALL ' . $db . '.sp_detail_jual(\'' . $trx_id . '\');'))->firstWhere("barang_id", $item["id"]);

                // dd([
                //     DB::select('CALL ' . $db . '.sp_detail_jual(\'' . $trx_id . '\');'), 
                //     $item["id"], 
                //     collect(DB::select('CALL ' . $db . '.sp_detail_jual(\'' . $trx_id . '\');'))
                // ]);
                $total_harga = $total_harga + (($item["harga"] ?? $barangs->jual) * (($item["jumlah"] >= ($barangs->jumlah - $barangs->refunded)) ? ($barangs->jumlah - $barangs->refunded) : $item["jumlah"]));
                $jumlah_barang++;
            } catch (\Throwable $th) {
                return ['status' => 'error', 'message' => 'Tidak ada data barang', 'data' => []];
            }
        }

        $kembali = 0;
        foreach ($data["kembali"] as $key => $value) {
            $kembali += $value["nominal"];
        }
        
        if($kembali < $total_harga){
            return ['status' => 'error', 'message' => 'Total yg di bayarkan kurang dari harga total', 'data' => []];
        }


        $refund_id = DB::table($db . '.' . $refund_jual)->insertGetId([
            "trx_id" => $trx_id,
            "faktur" => $faktur,
            "faktur_jual" => $data["faktur"],
            "nominal" => $total_harga,
            "jumlah_item" => $jumlah_barang,

            "user_id" => $user_id,
            "toko_id" => $toko_id,
            "catatan" => $item["catatan"] ?? "",
        ]);

        // insert detail trx
        foreach ($data["items"] as $item) {
            $barangs = collect(DB::select('CALL ' . $db . '.sp_detail_jual(\'' . $trx_id . '\');'))->firstWhere("barang_id", $item["id"]);

            DB::table($db . '.' . $detail_refund_jual)->insert([
                "refund_id" => $refund_id,
                "nama" => $barangs->nama,
                "faktur" => $faktur,
                "faktur_jual" => $data["faktur"],
                "beli" => $barangs->beli,
                "jual" => $item["harga"] ?? $barangs->jual,
                "jumlah" => ($item["jumlah"] > ($barangs->jumlah - $barangs->refunded)) ? ($barangs->jumlah - $barangs->refunded) : $item["jumlah"],
                "total" => ($item["harga"] ?? $barangs->jual) * (($item["jumlah"] >= ($barangs->jumlah - $barangs->refunded)) ? ($barangs->jumlah - $barangs->refunded) : $item["jumlah"]),
            ]);

            // update stok

            // DB::table($db . '.' . $stok)->insert(["tgl" => $data["tgl"], "barang_id" => $item["id"], "faktur" => $faktur, "masuk" => 0, "keluar" => $item["jumlah"]]);
        }

        foreach ($data["kembali"] as $key => $value) {
            DB::table($db . '.' . $bayar_refund_jual)->insert([
                "refund_id" => $refund_id,
                "faktur" => $faktur,
                "faktur_jual" => $data["faktur"],
                "carabayar_id" => $value["carabayar_id"],
                "nominal" => $value["nominal"],
            ]);
        }

        return ['status' => 'success', 'message' => 'Transaksi baru telah di tersimpan', 'data' => []];

    }

}
