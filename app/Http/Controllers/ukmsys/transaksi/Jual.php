<?php

namespace App\Http\Controllers\ukmsys\transaksi;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ukmsys\users;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Jual extends Controller
{

    public static function run($db, $data)
    {
        // {
        //     "pelanggan_id": 1,
        //     "pelanggan": {
        //         "nama": "Bukan Penipu",
        //         "email": "bukanpenipu@gmail.com",
        //         "no_tlpn": "0823123123",
        //         "alamat": "Jl apa aja, Mergan, Sukun, Malang, Jawa Timur, Indonesia, 65122",
        //         "barcode": ""
        //     },
        //     "items": [
        //         {
        //             "id": 1,
        //             "harga": 5000,
        //             "jumlah":3
        //         },
        //         {
        //             "id":2,
        //             "jumlah":2
        //         }
        //     ],
        //     "diskon": "20000",
        //     "bayar": [
        //         {
        //             "id": 1,
        //             "nominal": 20000
        //         },
        //         {
        //             "id": 2,
        //             "nominal": 5000
        //         }
        //     ],
        //     "catatan": ""
        // }

        $jual = "jual";
        $detail_jual = "detail_jual";
        $bayar_jual = "bayar_jual";
        // $project_has_jual = "project_has_jual";
        // $stok = "stok";
        $diskon_jual = "diskon_jual";

        $pelanggan = "pelanggan";
        $barang = "barang";
        // $project = "project";

        $user_id = Auth::user()->id;
        $toko_id = users::get($db, 'toko_id');

        $pFaktur = "TJ";

        $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

        //hitung total harga
        $total_harga = 0;
        $jumlah_barang = 0;
        foreach ($data["items"] as $item) {
            try {
                $barangs = DB::table($db . '.' . $barang)->where(["id" => $item["id"], "toko_id" => $toko_id])->first();

                $total_harga = $total_harga + (($item["harga"] ?? $barangs->jual) * $item["jumlah"]);
                $jumlah_barang++;
            } catch (\Throwable $th) {
                return ['status' => 'error', 'message' => 'Tidak ada data barang', 'data' => []];
            }
        }

        $total_diskon = 0;
        foreach ($data["diskon"] as $key => $item) {
            if ($item["persentase"] == 1) {
                $total_diskon = $data["diskon"][$key]["nominal"] = $total_diskon + $total_harga * $item["nominal"] / 100;
            } else {
                $total_diskon = $total_diskon + $item["nominal"];
            }
        }

        $bayar = 0;
        foreach ($data["bayar"] as $key => $value) {
            $bayar += (int) $value["nominal"];
        }

        if ($bayar < $total_harga - $total_diskon) {
            return ['status' => 'error', 'message' => 'Total yg di bayarkan kurang dari harga total', 'data' => []];
        }

        //cek belanggan
        if (!isset($data["pelanggan_id"]) && isset($data["pelanggan"])) {
            $data["pelanggan"]["toko_id"] = $toko_id;
            $pId = DB::table($db . '.' . $pelanggan)->insertGetId($data["pelanggan"]);
            $data["pelanggan_id"] = $pId;
        }

        // insert transaksi jual
        $trx_id = DB::table($db . '.' . $jual)->insertGetId([
            "tgl" => $data["tgl"] ?? date("Ymd", Carbon::now()->timestamp),
            "faktur" => $faktur,
            "pelanggan_id" => $data["pelanggan_id"] ?? null,
            // "status" => (($data["bayar"] - $total_harga) >= 0) ? 1 : 0, //lunas atau belum

            "diskon" => $total_diskon,
            "harga" => $total_harga,
            "jumlah_item" => $jumlah_barang,

            "user_id" => $user_id,
            "toko_id" => $toko_id,
            "catatan" => $item["catatan"] ?? "",
        ]);

        // insert diskon

        foreach ($data["diskon"] as $key => $item) {
            DB::table($db . '.' . $diskon_jual)->insert([
                "jual_id" => $trx_id,
                "nama" => $item["nama"],
                "nominal" => $item["nominal"],
                "persentase" => $item["persentase"],
            ]);
        }

        // insert detail trx
        foreach ($data["items"] as $item) {
            $barang_res = DB::table($db . '.' . $barang)->where(["id" => $item["id"], "toko_id" => $toko_id])->first();

            DB::table($db . '.' . $detail_jual)->insert([
                "jual_id" => $trx_id,
                "barang_id" => $barang_res->id,
                "nama" => $barang_res->nama,
                "faktur" => $faktur,
                "beli" => $barang_res->beli,
                "jual" => $item["harga"] ?? $barang_res->jual,
                "jumlah" => $item["jumlah"],
                "total" => ($item["harga"] ?? $barang_res->jual) * $item["jumlah"],
            ]);

            // update stok

            // DB::table($db . '.' . $stok)->insert(["tgl" => $data["tgl"], "barang_id" => $item["id"], "faktur" => $faktur, "masuk" => 0, "keluar" => $item["jumlah"]]);
        }

        // insert project
        // if (isset($data["project"])) {
        //     foreach ($data["project"] as $item) {
        //         DB::table($db . '.' . $project)->updateOrInsert([
        //             "nama" => $item,
        //             "toko_id" => $toko_id,
        //         ], []);

        //         $project_id = DB::table($db . '.' . $project)->where([
        //             "nama" => $item,
        //             "toko_id" => $toko_id,
        //         ])->first()->id;

        //         DB::table($db . '.' . $project_has_jual)->insert(["project_id" => $project_id, "penjualan_id" => $trx_id]);
        //     }
        // }

        foreach ($data["bayar"] as $key => $value) {
            DB::table($db . '.' . $bayar_jual)->insert([
                "jual_id" => $trx_id,
                "faktur" => $faktur,
                "carabayar_id" => $value["carabayar_id"],
                "nominal" => $value["nominal"],
            ]);
        }

        $kembali = $bayar - ($total_harga - $total_diskon);
        DB::table($db . '.' . $jual)->where(["id" => $trx_id])->update([
            "bayar" => $bayar,
            "kembali" => $kembali,
        ]);

        return ['status' => 'success', 'message' => 'Transaksi baru telah di tersimpan', 'data' => []];

    }

}
