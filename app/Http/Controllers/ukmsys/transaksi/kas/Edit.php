<?php

namespace App\Http\Controllers\ukmsys\transaksi\kas;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\ukmsys\users;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Edit extends Controller
{

    public static function run($db, $data)
    {
        $kas = "kas";

        $toko_id = users::get($db, 'toko_id');
        $user_id = Auth::user()->id;

        $table = DB::table($db . '.' . $kas);

        unset($data["id"]);
        unset($data["faktur"]);
        unset($data["toko_id"]);
        unset($data["user_id"]);
        unset($data["created_at"]);
        unset($data["updated_at"]);
        unset($data["deleted_at"]);
        $data["user_id"] = $user_id;
        $data["where"]["toko_id"] = $toko_id;

        $requestData = ["data" => $data, "id" => $data["id"]];

        return FapiController::update($requestData, $table);

    }

}
