<?php

namespace App\Http\Controllers\ukmsys\transaksi\kas;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\ukmsys\users;

// use Carbon\Carbon;

class Get extends Controller
{

    public static function run($db, $data)
    {
        $kas = "kas";

        $toko_id = users::get($db, 'toko_id');
        $table = DB::table($db . '.' . $kas);

        $data["where"]["toko_id"] = $toko_id;
        $requestData = ["data"=> $data];

        return FapiController::read($requestData, $table);

    }

}
