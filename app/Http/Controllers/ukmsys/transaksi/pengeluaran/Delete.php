<?php

namespace App\Http\Controllers\ukmsys\transaksi\pengeluaran;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\ukmsys\users;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Delete extends Controller
{

    public static function run($db, $data)
    {
        $pengeluaran = "pengeluaran";

        $toko_id = users::get($db, 'toko_id');
        $table = DB::table($db . '.' . $pengeluaran)->where(["toko_id" => $toko_id]);

        // $data["where"]["toko_id"] = $toko_id;
        $res = $table->where('id', $data['id'])->first();

        if (empty($res)) {
            return FapiController::response('error', 'data not found, so it cannot be delete', null, 400);
        }
        $table->where('id', $data['id'])->update(["deleted_at" => date("Ymd", Carbon::now()->timestamp)]);

        return FapiController::response('success', 'data has successfully delete', null, 200);

    }

}
