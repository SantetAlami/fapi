<?php

namespace App\Http\Controllers\ukmsys\master\toko;

use App\Http\Controllers\Controller;
// use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\FapiController;
// use App\Http\Controllers\retailpos\users;
use Illuminate\Support\Facades\DB;

class Create extends Controller
{

    public static function run($db, $data)
    {
        $toko = "toko";

        // $toko = "toko";

        $user_id = Auth::user()->id;
        $table = DB::table($db . '.' . $toko);

        $data["user_id"] = $user_id;
        $requestData = ["data" => $data];

        return FapiController::insert($requestData, $table);

    }

}
