<?php

namespace App\Http\Controllers\ukmsys\master\toko;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\FapiController;
// use App\Http\Controllers\retailpos\users;

// use Carbon\Carbon;

class Get extends Controller
{

    public static function run($db, $data)
    {
        $toko = "toko";

        $user_id = Auth::user()->id;
        $table = DB::table($db . '.' . $toko)->where(["user_id" => $user_id]);

        $requestData = ["data"=> $data];

        return FapiController::read($requestData, $table);

    }

}
