<?php

namespace App\Http\Controllers\ukmsys\master\diskon;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\ukmsys\users;

// use Carbon\Carbon;

class Edit extends Controller
{

    public static function run($db, $data)
    {
        $diskon = "diskon";

        $toko_id = users::get($db, 'toko_id');
        $table = DB::table($db . '.' . $diskon)->where(["toko_id" => $toko_id]);

        $requestData = ["data"=> $data, "id" => $data["id"]];

        return FapiController::update($requestData, $table);

    }

}
