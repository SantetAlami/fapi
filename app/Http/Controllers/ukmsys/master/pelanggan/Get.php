<?php

namespace App\Http\Controllers\ukmsys\master\pelanggan;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\ukmsys\users;

// use Carbon\Carbon;

class Get extends Controller
{

    public static function run($db, $data)
    {
        $pelanggan = "v_pelanggan";

        $toko_id = users::get($db, 'toko_id');
        $table = DB::table($db . '.' . $pelanggan)->where(["toko_id" => $toko_id]);

        $requestData = ["data"=> $data];

        return FapiController::read($requestData, $table);

    }

}
