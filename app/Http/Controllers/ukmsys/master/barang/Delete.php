<?php

namespace App\Http\Controllers\ukmsys\master\barang;

use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\ukmsys\users;

use Carbon\Carbon;

class Delete extends Controller
{

    public static function run($db, $data)
    {
        $barang = "barang";

        // $store = "store";

        // $user_id = Auth::user()->id;
        $toko_id = users::get($db, 'toko_id');
        $table = DB::table($db . '.' . $barang)->where(["toko_id" => $toko_id]);

        $data["toko_id"] = $toko_id;
        $requestData = ["data"=> ['deleted_at' => date("Y-m-d", Carbon::now()->timestamp)], "id" => $data["id"]];

        return FapiController::update($requestData, $table);

    }

}
