<?php

namespace App\Http\Controllers\ukmsys\master\barang;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ukmsys\users;
use App\Model\ukmsys\Barang;
// use App\Http\Controllers\FapiController;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Kategori extends Controller
{

    public static function run($db, $data)
    {
        $kategori = "kategori";

        $toko_id = users::get($db, 'toko_id');

        DB::table($db . '.' . $kategori)->insertOrIgnore(["keterangan" => $data['kategori'], "toko_id" => $toko_id]);
        $kategori_id = DB::table($db . '.' . $kategori)->where(["keterangan" => $data['kategori'], "toko_id" => $toko_id])->first()->id;

        foreach ($data["items"] as $key => $item) {
            Barang::where(["id" => $item["id"] ?? $item, "toko_id" => $toko_id])->update(["kategori_id" => $kategori_id]);
        }

        return ['status' => 'success', 'message' => 'Data barang telah di perbaharui', 'data' => []];

    }

}
