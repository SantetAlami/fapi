<?php

namespace App\Http\Controllers\ukmsys\master\barang;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ukmsys\users;
use App\Model\ukmsys\Barang;
// use App\Http\Controllers\FapiController;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Edit extends Controller
{

    public static function run($db, $data)
    {
        $kategori = "kategori";
        // $barang = "barang";
        $stok = "stok";

        // $store = "store";

        // $user_id = Auth::user()->id;

        $toko_id = users::get($db, 'toko_id');

        DB::table($db . '.' . $kategori)->insertOrIgnore(["keterangan" => $data['kategori'], "toko_id" => $toko_id]);
        $kategori_id = DB::table($db . '.' . $kategori)->where(["keterangan" => $data['kategori'], "toko_id" => $toko_id])->first()->id;

        $data["kategori_id"] = $kategori_id;
        $data["toko_id"] = $toko_id;
        unset($data['kategori']);

        $dataStok = $data['stok'] ?? null;
        unset($data['stok']);

        $barang = Barang::where(['id' => $data['id'], 'toko_id' => $toko_id]);

        $res = $barang->update($data);

        if ($barang->count() != 0) {
            $barang = $barang->first();
            // $barang_id = DB::table($db . '.' . $barang)->where(["barcode" => $data['barcode'], "toko_id" => $toko_id])->first()->id;

            $pFaktur = "EB";
            $faktur = DB::select('CALL ' . $db . '.sp_create_faktur(\'' . $pFaktur . date("Ymd", Carbon::now()->timestamp) . '\');')[0]->FAKTUR;

            if (isset($dataStok) && $barang->track_stok == 1) {
                $sum = DB::table($db . '.' . $stok)->selectRaw("(sum(masuk) - sum(keluar)) as tot")->where(["barang_id" => $data['id']])->first()->tot;
                DB::table($db . '.' . $stok)->insert(["tgl" => date("Y-m-d", Carbon::now()->timestamp), "barang_id" => $data['id'], "faktur" => $faktur, "masuk" => $dataStok - $sum, "keluar" => 0]);
            }

        }

        return ['status' => 'success', 'message' => 'Data barang telah di perbaharui', 'data' => []];

    }

}
