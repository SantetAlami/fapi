<?php

namespace App\Http\Controllers\ukmsys\master\barang;

use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\ukmsys\users;
use Illuminate\Support\Facades\DB;

// use Carbon\Carbon;

class Get extends Controller
{

    public static function run($db, $data)
    {
        $barang = "v_barang";

        // $store = "store";

        // $user_id = Auth::user()->id;
        $toko_id = users::get($db, 'toko_id');
        $table = DB::table($db . '.' . $barang);

        $data["where"]["toko_id"] = $toko_id;
        
        $requestData = ["data" => $data];

        return FapiController::read($requestData, $table);

    }

}
