<?php

namespace App\Http\Controllers\ukmsys\master\kategori;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\ukmsys\users;

// use Carbon\Carbon;

class Get extends Controller
{

    public static function run($db, $data)
    {
        $kategori = "v_kategori";

        $toko_id = users::get($db, 'toko_id');
        $table = DB::table($db . '.' . $kategori)->where(["toko_id" => $toko_id]);

        $requestData = ["data"=> $data];

        return FapiController::read($requestData, $table);

    }

}
