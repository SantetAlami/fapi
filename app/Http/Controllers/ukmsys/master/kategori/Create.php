<?php

namespace App\Http\Controllers\ukmsys\master\kategori;

use App\Http\Controllers\Controller;
// use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\ukmsys\users;
use Illuminate\Support\Facades\DB;

class Create extends Controller
{

    public static function run($db, $data)
    {
        $kategori = "kategori";

        $toko_id = users::get($db, 'toko_id');
        $table = DB::table($db . '.' . $kategori);

        $data["toko_id"] = $toko_id;
        $requestData = ["data" => $data];

        return FapiController::insert($requestData, $table);

    }

}
