<?php

namespace App\Http\Controllers\ukmsys\master\users;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ukmsys\users;

class Delete extends Controller
{

    public static function run($db, $data)
    {
        $toko = "toko";
        $users = "users";
        $user = Auth::user();

        $id = $data["id"];

        $toko_id = users::get($db, 'toko_id');

        $target_toko_id = DB::table($db . '.' . $users)->where(["id" => $id])->first()->toko_id ?? 0;

        $owner_id = DB::table($db . '.' . $toko)->where(["id" => $toko_id])->first()->user_id;
        $stores = DB::table($db . '.' . $toko)->where(["user_id" => $owner_id])->pluck("id")->toArray();

        if (!in_array($target_toko_id, $stores)) {
            return FapiController::response('error', 'Tidak ada data user', null, 401);
        }

        DB::table($db . '.' . $users)->where(["id" => $id])->delete();
        return UserController::destroy($id, ["level" => $user->level(), "roleGroup" => $user->roleGroup()]);

    }

}
