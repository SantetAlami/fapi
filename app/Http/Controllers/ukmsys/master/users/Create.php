<?php

namespace App\Http\Controllers\ukmsys\master\users;

use App\Http\Controllers\Controller;
use App\Http\Controllers\UserController;
use App\Http\Controllers\FapiController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ukmsys\users;

class Create extends Controller
{

    public static function run($db, $data)
    {
        $toko = "toko";

        $user = Auth::user();
        // $user_id = Auth::user()->id;
        $toko_id = users::get($db, 'toko_id');

        $owner_id = DB::table($db . '.' . $toko)->where(["id" => $toko_id])->first()->user_id;
        $stores = DB::table($db . '.' . $toko)->where(["user_id" => $owner_id])->pluck("id")->toArray();

        if (!in_array($data[$db]["toko_id"], $stores)) {
            return FapiController::response('error', 'Tidak ada data toko', null, 401);
        }

        return UserController::store($data, ["level" => $user->level(), "roleGroup" => $user->roleGroup()]);

    }

}
