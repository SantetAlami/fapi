<?php

namespace App\Http\Controllers\ukmsys\master\users;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\FapiController;
// use App\Http\Controllers\retailpos\users;

// use Carbon\Carbon;

class Get extends Controller
{

    public static function run($db, $data)
    {
        $user_id = Auth::user()->id;

        $data["gate"] = "sp_users";
        $data["data"] = [$user_id];

        return FapiController::sp($db, $data);

    }

}
