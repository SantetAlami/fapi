<?php

namespace App\Http\Controllers\ukmsys\master\bahan;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\ukmsys\users;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

class Get extends Controller
{

    public static function run($db, $data)
    {
        $bahan = "bahan";
        $stok_bahan = "stok_bahan";

        $toko_id = users::get($db, 'toko_id');
        $table = DB::table($db . '.' . $bahan);

        $data["where"]["toko_id"] = $toko_id;
        $requestData = ["data" => $data];

        $read = FapiController::read($requestData, $table);
        foreach ($read["res"]["data"] as $key => $item) {
            $read["res"]["data"][$key]->stok = DB::table($db . "." . $stok_bahan)->selectRaw("sum(masuk - keluar) as sisa")->where(["bahan_id" => $item->id])->where("tgl", "<=", $data["where"]["tgl"] ?? date("Ymd", Carbon::now()->timestamp))->first()->sisa ?? 0;
        }

        return $read;
    }

}
