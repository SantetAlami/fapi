<?php

namespace App\Http\Controllers\ukmsys\master\bahan;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\ukmsys\users;
use Illuminate\Support\Facades\DB;

// use Carbon\Carbon;

class Edit extends Controller
{

    public static function run($db, $data)
    {
        $bahan = "bahan";

        $toko_id = users::get($db, 'toko_id');
        $table = DB::table($db . '.' . $bahan);

        $data["where"]["toko_id"] = $toko_id;
        $requestData = ["data" => $data, "id" => $data["id"]];

        $update = FapiController::update($requestData, $table);

        $update["res"]["data"] = Get::run($db, [])["res"]["data"];

        return $update;
    }

}
