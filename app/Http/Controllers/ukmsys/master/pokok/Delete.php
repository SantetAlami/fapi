<?php

namespace App\Http\Controllers\ukmsys\master\pokok;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\ukmsys\users;

// use Carbon\Carbon;

class Delete extends Controller
{

    public static function run($db, $data)
    {
        $pokok = "pokok";

        $toko_id = users::get($db, 'toko_id');
        $table = DB::table($db . '.' . $pokok);

        $data["where"]["toko_id"] = $toko_id;
        $requestData = ["data"=> $data, "id" => $data["id"]];

        return FapiController::delete($requestData, $table);

    }

}
