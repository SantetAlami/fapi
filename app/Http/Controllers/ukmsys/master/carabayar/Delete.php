<?php

namespace App\Http\Controllers\ukmsys\master\carabayar;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\ukmsys\users;

// use Carbon\Carbon;

class Delete extends Controller
{

    public static function run($db, $data)
    {
        $carabayar = "carabayar";

        $toko_id = users::get($db, 'toko_id');
        $table = DB::table($db . '.' . $carabayar)->where(["toko_id" => $toko_id]);
        if($table->where("id", $data["id"])->first()->deletable == 0){
            return ['status' => 'error', 'message' => 'Data tidak boleh di hapus', 'data' => []];
        }

        $requestData = ["data"=> $data, "id" => $data["id"]];

        return FapiController::delete($requestData, $table);

    }

}
