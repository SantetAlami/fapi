<?php

namespace App\Http\Controllers\ukmsys\master\carabayar;

use App\Http\Controllers\Controller;
// use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\ukmsys\users;
use Illuminate\Support\Facades\DB;

class Create extends Controller
{

    public static function run($db, $data)
    {
        $carabayar = "carabayar";

        $toko_id = users::get($db, 'toko_id');
        $table = DB::table($db . '.' . $carabayar);

        $data["toko_id"] = $toko_id;
        $requestData = ["data" => $data];

        return FapiController::insert($requestData, $table);

    }

}
