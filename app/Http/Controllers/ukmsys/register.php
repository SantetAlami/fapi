<?php

namespace App\Http\Controllers\ukmsys;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ResponseController;
// use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\UserController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

// use Carbon\Carbon;

class register extends Controller
{

    public static function run(Request $data)
    {
        try {
            $db = "ukmsys";

            $user = new User;
            $user->name = $data['name'];
            $user->username = $data['username'];
            $user->email = $data['email'];
            $hashedPassword = app('hash')->make($data['password']);
            $user->password = $hashedPassword;
            $user->assignRole('Owner');
            $user->save();

            $credentials = ["username" => $data['username'], "password" => $data['password']];

            if ($token = Auth::attempt($credentials)) {
                $res = Auth::user();
                $toko_id = DB::table($db . '.toko')->insertGetId(["user_id" => $res->id, "nama" => $data["bussines_name"]]);
                DB::table($db . '.users')->insertOrIgnore(["id" => $res->id, "toko_id" => $toko_id, "pin" => $data["pin"]]);
                DB::table($db . '.carabayar')->insert([
                    ["keterangan" => "Cash", "icon" => "", "jeniscb_id" => 0, "toko_id" => $toko_id, "deletable" => 0],
                    ["keterangan" => "Card", "icon" => "", "jeniscb_id" => 1, "toko_id" => $toko_id, "deletable" => 1],
                ]);
                DB::table($db . '.struk')->insertOrIgnore([
                    "logo" => "https://",
                    "header" => "",
                    "footer" => "",
                    "info_pelanggan" => 1,
                    "catatan" => 0,
                    "toko_id" => $toko_id,
                ]);
                $res = UserController::profile([$db], true);
                $res->token = $token;
            }

            //return successful response
            // return response()->json(['user' => $user, 'message' => 'User Registration'], 201);
            return ResponseController::run('success', 'the user has successfully registered', $res, 201);

        } catch (\Throwable $e) {
            //return error message
            // return response()->json(['message' => 'User Registration Failed!'], 409);
            return ResponseController::run('error', 'user registration failed ' . $e->getMessage(), null, 409);
        }

    }

}
