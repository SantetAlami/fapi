<?php

namespace App\Http\Controllers\ukmsys\setting\feature;

use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ukmsys\users;
// use App\Http\Controllers\FapiController;
use Illuminate\Support\Facades\DB;

// use Carbon\Carbon;

class Update extends Controller
{

    public static function run($db, $data)
    {
        $toko_settings = "toko_settings";

        $toko_id = users::get($db, 'toko_id');

        foreach ($data as $key => $item) {
            DB::table($db . "." . $toko_settings)->updateOrInsert([
                "toko_id" => $toko_id,
                "settings_id" => $item["id"],
            ], [
                "value" => $item["value"],
            ]);
        }

        return ['status' => 'success', 'message' => 'Toko settings updated', 'data' => []];

    }

}
