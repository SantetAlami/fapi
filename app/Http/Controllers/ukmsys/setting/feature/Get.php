<?php

namespace App\Http\Controllers\ukmsys\setting\feature;

use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\ukmsys\users;

// use Carbon\Carbon;

class Get extends Controller
{

    public static function run($db, $data)
    {
        $sp_settings = "sp_settings";

        // $store = "store";

        // $user_id = Auth::user()->id;
        $toko_id = users::get($db, 'toko_id');
        $requestData = [
            "gate" => $sp_settings,
            "data" => [
                $toko_id,
            ],
        ];

        return FapiController::sp($db, $requestData);

    }

}
