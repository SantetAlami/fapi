<?php

namespace App\Http\Controllers\ukmsys\setting;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\UserController;
// use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

// use Carbon\Carbon;

class Editprofil extends Controller
{

    /**
     * Update the specified resource in storage.
     *
     * @param  Int $id
     * @param  Array  $data
     * @param  Array  $currentUser
     * @param  Bool $isMe
     * [level,roleGroup]
     * @return \Illuminate\Http\Response
     */
    public static function run($db, $data)
    {
        try {
            if (!empty($data['password'])) {
                if ($data['password'] !== $data['password_confirmation']) {
                    return FapiController::response('error', 'password and password confirmation must same', null, 400);
                }
                $plainPassword = $data['password'];
                $data['password'] = app('hash')->make($plainPassword);
            }

            $user_id = Auth::User()->id;
            $res = User::find($user_id);

            if (app('hash')->check($data['old_password'], $res->password)) {

                $additional = $data;
                try {
                    if ($data['photo']->getClientOriginalName() != "") {
                        if (File::exists('main/users/' . $res->photo)) {
                            File::delete('main/users/' . $res->photo);
                        }
                        $data['photo']->move('main/users', (string) Str::uuid() . $data['photo']->getClientOriginalName());
                        $res->photo = (string) Str::uuid() . $data['photo']->getClientOriginalName();
                        unset($additional['photo']);
                    }
                } catch (\Throwable $th) {}

                $res->name = $data['name'];
                $res->username = $data['username'];
                $res->email = $data['email'];
                $res->password = $data['password'];
                $res->save();

                unset($additional['name']);
                unset($additional['username']);
                unset($additional['email']);
                unset($additional['old_password']);
                unset($additional['password']);
                unset($additional['password_confirmation']);
                unset($additional['role']);
                if ($additional) {
                    $send = [];
                    foreach ($additional as $key => $value) {
                        $send[$key] = $value;
                        if ($key == "toko_id" && !DB::table($db . '.toko')->where(["user_id" => Auth::user()->id, "id" => $value])->exists()) {
                            unset($send[$key]);
                        }
                    }
                    DB::table($db . '.users')->updateOrInsert(['id' => Auth::user()->id], $send);
                }
                $res = UserController::profile(['ukmsys'], true);

                return FapiController::response('success', 'profile has successfully updated', $res, 200);
            }

            return FapiController::response('error', 'old password wrong', [], 406);

        } catch (\Throwable $th) {
            return FapiController::response('error', 'unauthorization ' . $th->getMessage(), null, 401);
        }

    }

}
