<?php

namespace App\Http\Controllers\ukmsys\setting\struk;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ukmsys\users;

// use Carbon\Carbon;

class Get extends Controller
{

    public static function run($db, $data)
    {
        $struk = "struk";

        $toko_id = users::get($db, 'toko_id');
        $res = DB::table($db . "." . $struk)->where(["toko_id" => $toko_id])->first();

        return ['status' => 'success', 'message' => 'Get data struk', 'data' => $res];

    }

}
