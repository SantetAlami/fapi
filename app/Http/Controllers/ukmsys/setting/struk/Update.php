<?php

namespace App\Http\Controllers\ukmsys\setting\struk;

use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ukmsys\users;
// use App\Http\Controllers\FapiController;
use Illuminate\Support\Facades\DB;

// use Carbon\Carbon;

class Update extends Controller
{

    public static function run($db, $data)
    {
        $struk = "struk";

        $toko_id = users::get($db, 'toko_id');

        DB::table($db . "." . $struk)->updateOrInsert(
            ["toko_id" => $toko_id],
            [
                "logo" => $data["logo"],
                "header" => $data["header"],
                "footer" => $data["footer"],
                "info_pelanggan" => $data["info_pelanggan"],
                "catatan" => $data["catatan"],
            ]);

        return ['status' => 'success', 'message' => 'Setting struk updated', 'data' => []];

    }

}
