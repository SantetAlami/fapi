<?php

namespace App\Http\Controllers\ukmsys\laporan;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FapiController;
// use Carbon\Carbon;
use App\Http\Controllers\ukmsys\users;
use Illuminate\Support\Facades\DB;

class Barang extends Controller
{

    public static function run($db, $data)
    {
        $toko = "toko";

        $toko_id = users::get($db, 'toko_id');

        $owner_id = DB::table($db . '.' . $toko)->where(["id" => $toko_id])->first()->user_id;
        $stores = DB::table($db . '.' . $toko)->where(["user_id" => $owner_id])->pluck("id")->toArray();

        $res = DB::select('CALL ' . $db . '.sp_laporan_jual_barang(\'' . implode(",", array_intersect($data["toko"], $stores)) . '\', \'' . implode(",", $data["user"]) . '\', \'' . $data["dari"] . '\', \'' . $data["sampai"] . '\');');

        $top5 = [];
        for ($i = 0; $i < 5; $i++) {
            try {
                $top5[] = [
                    "barang" => $res[$i]->barang,
                    "total_penjualan" => $res[$i]->total_penjualan,
                ];
            } catch (\Throwable $th) {
            }
        }

        $diagram = DB::select('CALL ' . $db . '.sp_laporan_jual_barang_diagram(\'' . implode(",", array_intersect($data["toko"], $stores)) . '\', \'' . implode(",", $data["user"]) . '\', \'' . $data["dari"] . '\', \'' . $data["sampai"] . '\');');

        $date1 = date_create($data["dari"]);
        $date2 = date_create($data["sampai"]);
        $diff = date_diff($date1, $date2);
        
        $labelsdiagram = [];
        for ($i = 0; $i <= $diff->format("%a"); $i++) {
            $plus = "+" . $i . " days";
            $labelsdiagram[] = date('Y-m-d', strtotime($data["dari"] . $plus));
        }
        
        $newdiagram = [];
        foreach ($diagram as $key => $item) {
            $newdiagram[$key]["nama"] = $item->nama;
            unset($item->nama);
            foreach ($labelsdiagram as $label) {
                $newdiagram[$key]["data"][$label] = $item->$label ?? "0";
            }
            $newdiagram[$key]["data"] = array_values($newdiagram[$key]["data"]);
        }

        $diagram = [];
        $diagram["labels"] = $labelsdiagram;
        $diagram["datasheet"] = $newdiagram;
        // sort($diagram);

        return ['status' => 'success', 'message' => 'Penjualan by barang', 'data' => ["table" => $res, "top5" => $top5, "diagram" => $diagram]];
    }

}
