<?php

namespace App\Http\Controllers\ukmsys\laporan;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FapiController;
use App\Http\Controllers\ukmsys\users;
use Illuminate\Support\Facades\DB;

class Kartustok extends Controller
{

    public static function run($db, $data)
    {

        $stok_bahan = "stok_bahan";
        $bahan = "bahan";

        $toko_id = users::get($db, 'toko_id');

        if (!DB::table($db . "." . $bahan)->where(["id" => $data["bahan_id"], "toko_id" => $toko_id])->exists()) {
            return FapiController::response('error', 'tidak ada data bahan', null, 404);
        }

        $res = DB::table(DB::raw($db . "." . $stok_bahan) . " as sb")
            ->selectRaw("
                tgl,
                sum(masuk) masuk,
                sum(keluar) keluar,
                sum(masuk - keluar) + ifnull((select sum(masuk - keluar) from ukmsys.stok_bahan s where s.tgl < sb.tgl and s.bahan_id = sb.bahan_id), 0) as sisa,
                catatan")
        // (select s.sisa from ukmsys.stok_bahan s where s.id = max(sb.id)) sisa,
            ->where(["bahan_id" => $data["bahan_id"]])
            ->where("tgl", ">=", $data["dari"])
            ->where("tgl", "<=", $data["sampai"])
            ->groupBy("tgl")
            ->orderBy("tgl")
            ->get();

        $chart = ["datasheet" => []];
        foreach ($res as $key => $item) {
            if (!in_array($item->tgl, $chart["labels"] ?? [])) {
                $chart["labels"][] = $item->tgl;
            }
            $chart["datasheet"]["masuk"]["name"] = "masuk";
            $chart["datasheet"]["masuk"]["data"][] = $item->masuk;
            $chart["datasheet"]["masuk"]["total"] = array_sum($chart["datasheet"]["masuk"]["data"] ?? []);
            // $chart["datasheet"]["kotor"]["color"] = $item->color;

            $chart["datasheet"]["keluar"]["name"] = "keluar";
            $chart["datasheet"]["keluar"]["data"][] = $item->keluar;
            $chart["datasheet"]["keluar"]["total"] = array_sum($chart["datasheet"]["keluar"]["data"] ?? []);

            $chart["datasheet"]["keluar"]["name"] = "keluar";
            $chart["datasheet"]["keluar"]["data"][] = $item->keluar;
            $chart["datasheet"]["keluar"]["total"] = array_sum($chart["datasheet"]["keluar"]["data"] ?? []);

            $chart["datasheet"]["sisa"]["name"] = "sisa";
            $chart["datasheet"]["sisa"]["data"][] = $item->sisa;
            $chart["datasheet"]["sisa"]["total"] = array_sum($chart["datasheet"]["sisa"]["data"] ?? []);
        }
        $chart["datasheet"] = array_values($chart["datasheet"]);

        return FapiController::response('success', 'get kartu stok', ["table" => $res, "chart" => $chart], 200);

    }

}
