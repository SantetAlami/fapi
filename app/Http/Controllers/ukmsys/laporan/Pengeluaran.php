<?php

namespace App\Http\Controllers\ukmsys\laporan;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FapiController;
// use Carbon\Carbon;
use App\Http\Controllers\ukmsys\users;
use Illuminate\Support\Facades\DB;

class Pengeluaran extends Controller
{

    public static function run($db, $data)
    {
        $pengeluaran = "v_pengeluaran";

        $toko_id = users::get($db, 'toko_id');
        $table = DB::table($db . '.' . $pengeluaran);

        $data["where"]["toko_id"] = $toko_id;
        $requestData = ["data" => $data];

        return FapiController::read($requestData, $table);

    }

}
