<?php

namespace App\Http\Controllers\ukmsys\laporan;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FapiController;
// use Carbon\Carbon;
use App\Http\Controllers\ukmsys\users;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Receips extends Controller
{

    public static function run($db, $data)
    {
        $jual = "v_jual";

        $toko_id = users::get($db, 'toko_id');
        $table = DB::table($db . '.' . $jual);

        if (!isset($data["where"])) {
            $data["where"] = [];
        }
        $data["where"]["toko_id"] = $toko_id;
        $requestData = ["data" => $data];

        return Receips::read($requestData, $table);
    }

    public static function read($requestData, $table)
    {
        $sg = explode(" ", str_replace("`", "", $table->toSql()));
        $sg = explode(".", end($sg));

        if (isset($requestData['id'])) {
            $table->where('id', $requestData['id']);
        }

        if (isset($requestData['data']['select'])) {
            if (is_array($requestData['data']['select'])) {
                $select = $requestData['data']['select'];
            } else {
                $select = [];
                foreach (explode(',', $requestData['data']['select']) as $key => $value) {
                    $select[] = ltrim(rtrim($value));
                }
            }

            $table->select($select);
        }

        $table = FapiController::where($requestData['data'], $table);

        if (isset($requestData['data']['and'])) {
            $and = $requestData['data']['and'];
            $table->where(function ($q) use ($and) {
                $q = FapiController::where($and, $q);
            });
        }

        if (isset($requestData['data']['or'])) {
            $or = $requestData['data']['or'];
            $table->orWhere(function ($q) use ($or) {
                $q = FapiController::where($or, $q);
            });
        }

        $res = $table->get();

        if ($res->isEmpty() && isset($requestData['data']['show_column'])) {
            $cols = DB::select("SELECT `COLUMN_NAME`
                    FROM `INFORMATION_SCHEMA`.`COLUMNS`
                    WHERE `TABLE_SCHEMA`='$sg[0]'
                    AND `TABLE_NAME`='$sg[1]';");

            $res = [[]];
            foreach ($cols as $key => $col) {
                $res[0][$col->COLUMN_NAME] = "";
            }
        }
        if (count($res) == 0) {
            return FapiController::response('success', 'data not found', $res, 200);
        }

        $total = 0;
        $count_sales = 0;
        $count_refund = 0;
        foreach ($res as $key => $item) {
            $res[$key]->nama_pelanggan = DB::table($sg[0] . ".pelanggan")->where("id", $item->pelanggan_id)->first()->nama ?? "";
            $res[$key]->detail = DB::select('CALL ' . $sg[0] . '.sp_detail_jual(' . $item->id . ');');
            if (isset($res[$key]->faktur_refund)) {
                $res[$key]->bayar = DB::table($sg[0] . ".bayar_refund_jual")->where("refund_id", $item->id)->get();
            } else {
                $res[$key]->bayar = DB::table($sg[0] . ".bayar_jual")->where("jual_id", $item->id)->get();
            }

            if (empty($item->faktur_refund)) {
                $count_sales++;
            } else {
                $count_refund++;
            }

            $total += $item->total_bayar;
        }
        if (isset($res[0])) {
            $res[0]->count_sales = $count_sales;
            $res[0]->count_refund = $count_refund;
            $res[0]->total = $total;
        }

        return FapiController::response('success', 'showing all data', $res, 200);

    }

}
