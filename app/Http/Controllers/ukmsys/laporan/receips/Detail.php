<?php

namespace App\Http\Controllers\ukmsys\laporan\receips;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FapiController;
// use Carbon\Carbon;
use App\Http\Controllers\ukmsys\users;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Detail extends Controller
{

    public static function run($db, $data)
    {
        $jual = "jual";

        $toko_id = users::get($db, 'toko_id');

        $table = DB::table($db . '.' . $jual)->where(["toko_id" => $toko_id, "id" => $data["id"]])->first();

        if (!empty($table)) {
            $items = DB::select('CALL ' . $db . '.sp_detail_jual(' . $data["id"] . ');');
            $table->items = $items;
        }

        return ['status' => 'success', 'message' => 'Detail Recieps', 'data' => $table];
    }

}
