<?php

namespace App\Http\Controllers\ukmsys\laporan;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FapiController;
// use Carbon\Carbon;
use App\Http\Controllers\ukmsys\users;
use Illuminate\Support\Facades\DB;

class Ringkasan extends Controller
{

    public static function run($db, $data)
    {
        $toko = "toko";

        $toko_id = users::get($db, 'toko_id');

        $owner_id = DB::table($db . '.' . $toko)->where(["id" => $toko_id])->first()->user_id;
        $stores = DB::table($db . '.' . $toko)->where(["user_id" => $owner_id])->pluck("id")->toArray();

        $res = DB::select('CALL ' . $db . '.sp_laporan_jual_ringkasan(\'' . implode(",",array_intersect($data["toko"], $stores)) . '\', \'' . implode(",",$data["user"]) . '\', \'' . $data["dari"] . '\', \'' . $data["sampai"] . '\');');

        $jarak = abs(strtotime($data["sampai"]) - strtotime($data["dari"])) / 86400;
        $sampai_sebelum = date('Y-m-d', strtotime($data["dari"] . "-1 days"));

        $minus = "-" . $jarak + 1 . " days";
        $dari_sebelum = date('Y-m-d', strtotime($data["dari"] . $minus));
        $sebelum = DB::select('CALL ' . $db . '.sp_laporan_jual_ringkasan(\'' . implode(",",array_intersect($data["toko"], $stores)) . '\', \'' . implode(",",$data["user"]) . '\', \'' . $dari_sebelum . '\', \'' . $sampai_sebelum . '\');');

        $date1 = date_create($data["dari"]);
        $date2 = date_create($data["sampai"]);
        $diff = date_diff($date1, $date2);

        for ($i = 0; $i <= $diff->format("%a"); $i++) {
            $plus = "+" . $i . " days";
            if (!(new self)->in_object($res, "tgl", date('Y-m-d', strtotime($data["dari"] . $plus)))) {
                $cek = (object) null;
                $cek->tgl = date('Y-m-d', strtotime($data["dari"] . $plus));
                $cek->kotor = "0";
                $cek->pengembalian = "0";
                $cek->diskon = "0";
                $cek->bersih = "0";
                $cek->pokok = "0";
                $cek->laba_kotor = "0";
                $cek->margin = "0";
                $res[] = $cek;
            }
        }
        usort($res, function ($item1, $item2) {
            return $item1->tgl <=> $item2->tgl;
        });

        $chart = [];
        foreach ($res as $key => $item) {
            if (!in_array($item->tgl, $chart["labels"] ?? [])) {
                $chart["labels"][] = $item->tgl;
            }
            $chart["datasheet"]["kotor"]["name"] = "kotor";
            $chart["datasheet"]["kotor"]["data"][] = $item->kotor;
            $chart["datasheet"]["kotor"]["total"] = array_sum($chart["datasheet"]["kotor"]["data"] ?? []);
            // $chart["datasheet"]["kotor"]["color"] = $item->color;
            
            $chart["datasheet"]["pengembalian"]["name"] = "pengembalian";
            $chart["datasheet"]["pengembalian"]["data"][] = $item->pengembalian;
            $chart["datasheet"]["pengembalian"]["total"] = array_sum($chart["datasheet"]["pengembalian"]["data"] ?? []);

            $chart["datasheet"]["diskon"]["name"] = "diskon";
            $chart["datasheet"]["diskon"]["data"][] = $item->diskon;
            $chart["datasheet"]["diskon"]["total"] = array_sum($chart["datasheet"]["diskon"]["data"] ?? []);

            $chart["datasheet"]["bersih"]["name"] = "bersih";
            $chart["datasheet"]["bersih"]["data"][] = $item->bersih;
            $chart["datasheet"]["bersih"]["total"] = array_sum($chart["datasheet"]["bersih"]["data"] ?? []);

            $chart["datasheet"]["pokok"]["name"] = "pokok";
            $chart["datasheet"]["pokok"]["data"][] = $item->pokok;
            $chart["datasheet"]["pokok"]["total"] = array_sum($chart["datasheet"]["pokok"]["data"] ?? []);

            $chart["datasheet"]["laba_kotor"]["name"] = "laba_kotor";
            $chart["datasheet"]["laba_kotor"]["data"][] = $item->laba_kotor;
            $chart["datasheet"]["laba_kotor"]["total"] = array_sum($chart["datasheet"]["laba_kotor"]["data"] ?? []);

            $chart["datasheet"]["margin"]["name"] = "margin";
            $chart["datasheet"]["margin"]["data"][] = $item->margin;
            $chart["datasheet"]["margin"]["total"] = array_sum($chart["datasheet"]["margin"]["data"] ?? []);

        }

        // "sebelum" => $sebelum, 
        return ['status' => 'success', 'message' => 'Penjualan by ringkasan', 'data' => ["table" => $res, "chart" => $chart]];
    }

    /**
     * Update method.
     *
     * @param  Array  $array
     * @param  String  $key
     * @param  String  $val
     * @return Bool
     */
    public function in_object($array, $key, $val)
    {
        foreach ($array as $item) {
            if (isset($item->$key) && $item->$key == $val) {
                return true;
            }
        }

        return false;
    }

}
