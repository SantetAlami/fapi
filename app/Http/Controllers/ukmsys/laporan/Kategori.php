<?php

namespace App\Http\Controllers\ukmsys\laporan;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FapiController;
// use Carbon\Carbon;
use App\Http\Controllers\ukmsys\users;
use Illuminate\Support\Facades\DB;

class Kategori extends Controller
{

    public static function run($db, $data)
    {
        $toko = "toko";

        $toko_id = users::get($db, 'toko_id');

        $owner_id = DB::table($db . '.' . $toko)->where(["id" => $toko_id])->first()->user_id;
        $stores = DB::table($db . '.' . $toko)->where(["user_id" => $owner_id])->pluck("id")->toArray();

        $res = DB::select('CALL ' . $db . '.sp_laporan_jual_kategori(\'' . implode(",",array_intersect($data["toko"], $stores)) . '\', \'' . implode(",",$data["user"]) . '\', \'' . $data["dari"] . '\', \'' . $data["sampai"] . '\');');

        return ['status' => 'success', 'message' => 'Penjualan by kategori', 'data' => $res];
    }

}
