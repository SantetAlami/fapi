<?php

namespace App\Http\Controllers;

use App\Channel;
use Illuminate\Support\Facades\Auth;

class ChannelsController extends Controller
{

    /**
     * Delete method.
     *
     * @param  Array  $requestData
     * @return Response
     */

    public function read($requestData)
    {
        try {
            $res = [];
            Channel::create($requestData);

            return FapiController::response('success', 'data has successfully created', $res, 201);

        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    /**
     * Delete method.
     *
     * @param  Array  $requestData
     * @return Response
     */

    public function store($requestData)
    {
        try {
            $res = [];
            Channel::create($requestData);

            return FapiController::response('success', 'data has successfully created', $res, 201);

        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    /**
     * Update by id method.
     *
     * @param  Array  $requestData
     * @param  DB::class  $table
     * @return Response
     */

    public static function updateById($requestData)
    {
        try {
            $cek = Channel::where('id', $requestData['id'])->where('user_id', Auth::user()->id)->first();
            if (empty($cek)) {
                return FapiController::response('success', 'data not found, so it cannot be updated', null, 200);
            }
            $cek->update($requestData['data']);

            $res = Channel::where('id', $requestData['id'])->where('user_id', Auth::user()->id)->first();

            return FapiController::response('success', 'data has successfully updated', $res, 200);

        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    /**
     * Update method.
     *
     * @param  Array  $requestData
     * @param  DB::class  $table
     * @return Response
     */

    public static function update($requestData)
    {
        try {
            $cek = Channel::where('id', $requestData['id'])->first();
            if (empty($cek)) {
                return FapiController::response('success', 'data not found, so it cannot be updated', null, 200);
            }
            $cek->update($requestData['data']);

            $res = Channel::where('id', $requestData['id'])->first();

            return FapiController::response('success', 'data has successfully updated', $res, 200);

        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    /**
     * Delete by id method.
     *
     * @param  Array  $requestData
     * @param  DB::class  $table
     * @return Response
     */

    public static function deleteById($requestData)
    {
        try {
            $res = Channel::where('id', $requestData['id'])->where('user_id', Auth::user()->id)->delete();
            if (empty($res)) {
                return FapiController::response('success', 'data not found, so it cannot be updated', null, 200);
            }

            return FapiController::response('success', 'data has successfully deleted', null, 200);

        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }

    /**
     * Delete method.
     *
     * @param  Array  $requestData
     * @param  DB::class  $table
     * @return Response
     */

    public static function delete($requestData)
    {
        try {
            $res = Channel::where('id', $requestData['id'])->delete();
            if (empty($res)) {
                return FapiController::response('success', 'data not found, so it cannot be updated', null, 200);
            }

            return FapiController::response('success', 'data has successfully deleted', null, 200);

        } catch (\Throwable $e) {
            return FapiController::response('error', $e->getMessage(), null, 409);
        }
    }
}
