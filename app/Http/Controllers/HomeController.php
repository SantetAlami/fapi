<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    public function index()
    {
        $pushed = exec('git rev-list --all --count');
        return "Welcome to Fu<del style='text-decoration-color: red;color: #EEE;'>ll</del><sup>ture</sup> Api v2.0.0 ($pushed)";
    }
}
