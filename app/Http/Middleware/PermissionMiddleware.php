<?php

namespace App\Http\Middleware;

use App\Http\Controllers\ResponseController;
use Closure;

class PermissionMiddleware
{
    public function handle($request, Closure $next, $permission)
    {
        if (app('auth')->guest()) {
            return ResponseController::run('error', 'unauthorization', null, 401);

        }

        $permissions = is_array($permission)
        ? $permission
        : explode('|', $permission);

        foreach ($permissions as $permission) {
            if (app('auth')->user()->hasRole('super admin') || app('auth')->user()->can($permission)) {
                return $next($request);
            }
        }

        return ResponseController::run('error', 'forbidden action', null, 403);
    }
}
