<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        Commands\PermitCommand::class,
        Commands\PermitAMCommand::class,
        Commands\PermitAMsysCommand::class,
        Commands\PermitDBCommand::class,
        Commands\PermitSPDBCommand::class,
        Commands\PermitGeoCommand::class,
        Commands\TestCommand::class,
        Commands\SACommand::class,
        Commands\StartCommand::class,
        Commands\ValidateCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
