<?php
namespace App\Console\Commands;

use App\Http\Controllers\AuthController;
use App\Permission;
use App\Role;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class StartCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "permit:start";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Seed db role permission after first migration";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $tablePermit = ['users', 'roles'];
            foreach ($tablePermit as $key => $name) {
                exec('php artisan permission:create-permission access-' . strtolower($name));
                exec('php artisan permission:create-permission manage-' . strtolower($name));
                exec('php artisan permission:create-permission create-' . strtolower($name));
                exec('php artisan permission:create-permission read-' . strtolower($name));
                exec('php artisan permission:create-permission update-' . strtolower($name));
                exec('php artisan permission:create-permission delete-' . strtolower($name));
                DB::table('view_permissions')->insert(
                    [
                        'staff' => 'Auth',
                        'title' => ucfirst($name),
                        'access' => 'access-' . strtolower($name),
                        'manage' => 'manage-' . strtolower($name),
                        'create' => 'create-' . strtolower($name),
                        'read' => 'read-' . strtolower($name),
                        'update' => 'update-' . strtolower($name),
                        'delete' => 'delete-' . strtolower($name),
                    ]
                );

                $this->info("permission " . $name . " generated");
            }

            $defaultRoles = [
                ['name' => 'super admin', 'guard_name' => 'api', 'level' => 0, 'group' => 'su'],
                ['name' => 'guest', 'guard_name' => 'api', 'level' => 1, 'group' => null],
                ['name' => 'admin', 'guard_name' => 'api', 'level' => 10, 'group' => null],
                ['name' => 'user', 'guard_name' => 'api', 'level' => 50, 'group' => null],
                ['name' => 'admin post', 'guard_name' => 'api', 'level' => 10, 'group' => 'post'],
                ['name' => 'user post', 'guard_name' => 'api', 'level' => 50, 'group' => 'post'],
            ];
            Role::insert($defaultRoles);

            $this->info("default roles created");

            foreach ($defaultRoles as $key => $itemRole) {
                $role = Role::where('name', $itemRole['name'])->first();
                if ($role) {
                    switch ($itemRole['name']) {
                        case 'super admin':
                            $role->syncPermissions(Permission::all());
                            break;

                        case 'admin':
                            $permition = [];
                            foreach ($tablePermit as $key => $item) {
                                $permition[] = 'access-' . $item;
                                $permition[] = 'manage-' . $item;
                                $permition[] = 'create-' . $item;
                                $permition[] = 'read-' . $item;
                                $permition[] = 'update-' . $item;
                                $permition[] = 'delete-' . $item;
                            }
                            $role->syncPermissions($permition);
                            break;

                        case 'admin post':
                            $permition = [];
                            foreach ($tablePermit as $key => $item) {
                                $permition[] = 'access-' . $item;
                                $permition[] = 'manage-' . $item;
                                $permition[] = 'create-' . $item;
                                $permition[] = 'read-' . $item;
                                $permition[] = 'update-' . $item;
                                $permition[] = 'delete-' . $item;
                            }
                            $role->syncPermissions($permition);
                            break;

                        default:
                            // $this->error('default user permit');
                            break;
                    }
                }
                $this->info("permission " . $itemRole['name'] . " set!");

            }
            $reg = [
                "name" => "guest",
                "email" => "guest",
                "password" => "1234",
                "password_confirmation" => "1234",
            ];
            AuthController::register($reg);
            $this->info("user guest created!");
        } catch (\Exception $e) {
            $this->error($e);
        }
    }
}
