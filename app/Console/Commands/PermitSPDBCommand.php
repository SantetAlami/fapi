<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PermitSPDBCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "permit:DBam {db}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Create permission Access, Create, Read, Update, Delete for all table in db target";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $db = $this->argument('db');

        $db = explode(' ', $db)[0];
        try {

            $tables = DB::select("SELECT specific_name AS name FROM `information_schema`.`ROUTINES` WHERE routine_schema='$db' AND routine_type='PROCEDURE' AND LEFT(specific_name,3) = 'sp_'");

            foreach ($tables as $key => $item) {
                exec('php artisan permit:am ' . $db . ' ' . $item->name);
            }

            $this->info("All am permission has been created!");
        } catch (\Exception $e) {
            $this->error($e);
        }
    }
}
