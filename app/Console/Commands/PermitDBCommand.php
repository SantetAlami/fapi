<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PermitDBCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "permit:DBamcrud {db}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Create permission Access, Create, Read, Update, Delete for all table in db target";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $db = $this->argument('db');

        $db = explode(' ', $db)[0];
        try {

            $tables = DB::select("SELECT inf.TABLE_NAME as name, if((SELECT COLUMN_NAME FROM information_schema.columns
            where table_schema = '$db' AND COLUMN_NAME = 'user_id' AND TABLE_NAME = inf.TABLE_NAME )='user_id',1,0) AS user_id from information_schema.columns inf
            where inf.table_schema = '$db' AND LEFT(inf.TABLE_NAME,2) <> 'v_'
            GROUP BY inf.TABLE_NAME");

            foreach ($tables as $key => $item) {
                if ($item->user_id == 0) {
                    exec('php artisan permit:amcrud ' . $db . ' ' . $item->name);
                } else {
                    exec('php artisan permit:amcrud ' . $db . ' ' . $item->name . ' 1');
                }
                $this->info("Amcrud $db $item->name permission has been created!");
            }

            $this->info("All amcrud permission has been created!");
        } catch (\Exception $e) {
            $this->error($e);
        }
    }
}
