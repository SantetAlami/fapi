<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PermitAMCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "permit:am {db} {names}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Create permission Access, Manage (this permission for real backend api)";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $db = $this->argument('db');
        $names = $this->argument('names');

        try {
            foreach (explode(',',$names) as $key => $name) {
                exec('php artisan permission:create-permission access-' . strtolower($db) . '-' . strtolower($name));
                exec('php artisan permission:create-permission manage-' . strtolower($db) . '-' . strtolower($name));

                $view_permission = [
                    'staff' => ucfirst($db),
                    'title' => ucfirst($name),
                    'access' => 'access-' . strtolower($db) . '-' . strtolower($name),
                    'manage' => 'manage-' . strtolower($db) . '-' . strtolower($name),
                ];

                $check = DB::table('view_permissions')->where('staff', ucfirst($db))->where('title', ucfirst($name))->first();
                if (!empty($check)) {
                    DB::table('view_permissions')->where('staff', ucfirst($db))->where('title', ucfirst($name))->update($view_permission);
                } else {
                    DB::table('view_permissions')->insert($view_permission);
                }
                $this->info("$name am permission has been created!");
            }
            $this->info("All am permission has been created!");
        } catch (\Exception $e) {
            $this->error($e);
        }
    }
}
