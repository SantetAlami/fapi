<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PermitAMRCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "permit:amr {db} {name}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Create permission Access, Manage, Read (this permission for view)";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $db = $this->argument('db');
        $name = $this->argument('name');

        try {
            exec('php artisan permission:create-permission access-' . strtolower($db) . '-' . strtolower($name));
            exec('php artisan permission:create-permission manage-' . strtolower($db) . '-' . strtolower($name));
            exec('php artisan permission:create-permission read-' . strtolower($db) . '-' . strtolower($name));

            $view_permission = [
                'staff' => ucfirst($db),
                'title' => ucfirst($name),
                'access' => 'access-' . strtolower($db) . '-' . strtolower($name),
                'manage' => 'manage-' . strtolower($db) . '-' . strtolower($name),
                'read' => 'read-' . strtolower($db) . '-' . strtolower($name),
            ];


            $check = DB::table('view_permissions')->where('staff', ucfirst($db))->where('title', ucfirst($name))->first();
            if(!empty($check)){
                $check->update($view_permission);
            }else {
                DB::table('view_permissions')->insert($view_permission);
            }
            $this->info("All amcrud permission has been created!");
        } catch (\Exception $e) {
            $this->error($e);
        }
    }
}
