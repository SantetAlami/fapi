<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

// use Config\perpus;

class ValidateCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "fapi:setValidate {db} {names}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Set validation of table with default value";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $db = $this->argument('db');
        $names = $this->argument('names');

        try {
            foreach (explode(',', $names) as $key => $name) {
                $cols = DB::select("SELECT * FROM (
                    SELECT `COLUMN_NAME` AS col
                    FROM `INFORMATION_SCHEMA`.`COLUMNS`
                    WHERE `TABLE_SCHEMA`='$db' AND `TABLE_NAME`='$name'
                ) a WHERE col NOT IN ('id', 'created_at', 'updated_at')");

                $ins = [];
                foreach ($cols as $key => $item) {
                    $ins[] = [
                        'staff' => $db,
                        'gate' => $name,
                        'pillar' => $item->col,
                    ];
                }

                DB::table('gate_validation')->insert($ins);
                $this->info("$name validation has been created!");
            }
            $this->info("All validaion has been created with default value!");
        } catch (\Exception $e) {
            $this->error($e);
        }
    }
}
