<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
// use Illuminate\Support\Facades\DB;
use App\User;
use App\Role;
use App\Permission;

class SACommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "permit:SARole {idUser}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Give SA role to user via id user";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $idRole = $this->argument('idRole');
        $idUser = $this->argument('idUser');

        try {
            $role = Role::findOrFail(1);
            if ($role) {
                $role->syncPermissions(Permission::all());
                User::findOrFail($idUser)->syncRoles([$role]);
            }
            $this->info("permission added!");
        } catch (\Exception $e) {
            $this->error($e);
        }
    }
}
