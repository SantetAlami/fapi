<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
// use Config\perpus;

class TestCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "test {string}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Test something!";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $arr = $this->argument('arr');
        $string = $this->argument('string');

        try {
            $arr = ["ada gajah di sini", "ada singa di sana", "tidak ada apa di sana"];
            $res = array_search_like($arr, $string);
            dd($res);
            // $this->info($res);
        } catch (\Exception $e) {
            $this->error($e);
        }
    }
}
