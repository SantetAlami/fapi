<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PermitAMsysCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "permit:amsys {db} {names}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Create permission Access, Manage (this permission for real backend api)";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $db = $this->argument('db');
        $names = $this->argument('names');

        try {
            foreach (explode(',',$names) as $key => $name) {
                exec('php artisan permission:create-permission access-sys-' . strtolower($db) . '-' . strtolower($name));
                exec('php artisan permission:create-permission manage-sys-' . strtolower($db) . '-' . strtolower($name));

                $view_permission = [
                    'staff' => ucfirst($db),
                    'title' => 'sys ' . ucfirst($name),
                    'access' => 'access-sys-' . strtolower($db) . '-' . strtolower($name),
                    'manage' => 'manage-sys-' . strtolower($db) . '-' . strtolower($name),
                ];

                $check = DB::table('view_permissions')->where('staff', ucfirst($db))->where('title', 'sys ' . ucfirst($name))->first();
                if (!empty($check)) {
                    DB::table('view_permissions')->where('staff', ucfirst($db))->where('title', 'sys ' . ucfirst($name))->update($view_permission);
                } else {
                    DB::table('view_permissions')->insert($view_permission);
                }
                $this->info("$name am sys permission has been created!");
            }
            $this->info("All am sys permission has been created!");
        } catch (\Exception $e) {
            $this->error($e);
        }
    }
}
