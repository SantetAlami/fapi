<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PermitCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "permit:amcrud {db} {name} {wIdUser=0}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Create permission Access, Manage, Create, Read, Update, Delete (this permission for table)";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $db = $this->argument('db');
        $name = $this->argument('name');
        // try {
        $wIdUser = $this->argument('wIdUser');
        // } catch (\Throwable $th) {
        //     $wIdUser = 0;
        // }

        // dd($wIdUser);
        try {
            exec('php artisan permission:create-permission access-' . strtolower($db) . '-' . strtolower($name));
            exec('php artisan permission:create-permission manage-' . strtolower($db) . '-' . strtolower($name));
            exec('php artisan permission:create-permission create-' . strtolower($db) . '-' . strtolower($name));
            exec('php artisan permission:create-permission read-' . strtolower($db) . '-' . strtolower($name));
            exec('php artisan permission:create-permission update-' . strtolower($db) . '-' . strtolower($name));
            exec('php artisan permission:create-permission delete-' . strtolower($db) . '-' . strtolower($name));

            $view_permission = [
                'staff' => ucfirst($db),
                'title' => ucfirst($name),
                'access' => 'access-' . strtolower($db) . '-' . strtolower($name),
                'manage' => 'manage-' . strtolower($db) . '-' . strtolower($name),
                'create' => 'create-' . strtolower($db) . '-' . strtolower($name),
                'read' => 'read-' . strtolower($db) . '-' . strtolower($name),
                'update' => 'update-' . strtolower($db) . '-' . strtolower($name),
                'delete' => 'delete-' . strtolower($db) . '-' . strtolower($name),
            ];

            if ($wIdUser != 0) {
                exec('php artisan permission:create-permission read-' . strtolower($db) . '-' . strtolower($name) . '-id');
                exec('php artisan permission:create-permission update-' . strtolower($db) . '-' . strtolower($name) . '-id');
                exec('php artisan permission:create-permission delete-' . strtolower($db) . '-' . strtolower($name) . '-id');

                $view_permission['readById'] = 'read-' . strtolower($db) . '-' . strtolower($name) . '-id';
                $view_permission['updateById'] = 'update-' . strtolower($db) . '-' . strtolower($name) . '-id';
                $view_permission['deleteById'] = 'delete-' . strtolower($db) . '-' . strtolower($name) . '-id';

            }

            DB::table('view_permissions')->insert($view_permission);
            $this->info("All amcrud permission has been created!");
        } catch (\Exception $e) {
            $this->error($e);
        }
    }
}
