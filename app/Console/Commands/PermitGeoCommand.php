<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;

class PermitGeoCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "permit:DBgeoam {db}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Create permission Access, Create, Read, Update, Delete for all table in db target";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $db = $this->argument('db');

        $db = explode(' ', $db)[0];
        try {

            $res = [];

            $res = $this->dig('Http/Controllers/' . $db, $db);

            $tables = array_flatten($res);

            foreach ($tables as $key => $item) {
                exec('php artisan permit:am ' . $db . ' ' . $item);
                $this->info($item . " permit has been created!");
            }

            $this->info("All am permission has been created!");
        } catch (\Exception $e) {
            $this->error($e);
        }
    }

    public function dig($path, $db)
    {
        $res = [];
        $dir = array_diff(scandir(app_path($path)), array('.', '..'));
        foreach ($dir as $key => $item) {
            $arr = explode('.', $item);
            if ($arr[count($arr) - 1] != 'php') {
                $res[] = $this->dig($path . '/' . $item, $db);
            } else {
                if (strlen(preg_replace('![^A-Z]+!', '', $item)) == 1) {
                    $res[] = str_replace("/", "-", str_replace("Http/Controllers/" . $db . "/", "", $path) . '/' . str_replace(".php", "", strtolower($item)));
                }
            }
        }

        return $res;
    }

}
