<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Gate_validation extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'gate_validation';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'staff', 'gate', 'pillar', 'required', 'email', 'unique', 'required_if', 'must_if', 'math',
    ];

}
