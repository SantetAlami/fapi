<?php

namespace App\Model\ukmsys;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    /**
     * The database table enable or disable timestamp in this the model.
     *
     * @var bool
     */

    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ukmsys.barang';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'barcode', 'nama', 'kategori_id', 'beli', 'jual', 'satuan', 'gambar', 'toko_id', 'detail', 'track_stok', 'deleted_at'
    ];
}
