<?php

namespace App;

use App\Role;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, HasRoles;

    protected $guard_name = 'api';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //this is new

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Get all permission atribute of user.
     *
     * @return array
     */
    public function getAllPermissionsAttribute($staff = [])
    {
        $res = [];
        $allPermissions = $this->getAllPermissions();
        foreach ($allPermissions as $p) {
            if ($staff == []) {
                $res[] = $p->name;
            } else {
                foreach ($staff as $key => $value) {
                    if (is_numeric(strpos(substr_count($p->name, "-") != 1 ? $p->name : $value, $value))) {
                        $res[] = $p->name;
                    }
                }
            }
        }
        return $res;
    }

    /**
     * Get all manage of permission atribute of user.
     *
     * @return array
     */
    public function getManagePermit()
    {
        $res = [];
        $allPermissions = $this->getAllPermissions();
        foreach ($allPermissions as $p) {
            $res[] = $p->name;
        }
        return $res;
    }

    /**
     * Get role level.
     *
     * @return String
     */
    public function idRole()
    {
        $res = [];
        $allRoles = $this->getRoleNames();
        foreach ($allRoles as $name) {
            // $res[] = $name;
            $res[] = Role::where('name', $name)->pluck('id')[0];
        }

        return min($res);
    }

    /**
     * Get role level.
     *
     * @return String
     */
    public function level()
    {
        $res = [];
        $allRoles = $this->getRoleNames();
        foreach ($allRoles as $name) {
            // $res[] = $name;
            $res[] = Role::where('name', $name)->pluck('level')[0];
        }

        return min($res);
    }

    /**
     * Get role group.
     *
     * @return array
     */
    public function roleGroup()
    {
        $res = [];
        $allRoles = $this->getRoleNames();
        foreach ($allRoles as $name) {
            // $res[] = $name;
            $res[] = Role::where('name', $name)->pluck('group')[0];
        }

        return $res;
    }

}
