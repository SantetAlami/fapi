<?php

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

$v1x = "fapi/nyan1";
$v2x = "eva";
$v2_1x = "kle";

Route::post($v1x, 'ApiGateController@start');
Route::post($v1x . '/{db}', 'ApiGateController@start');
Route::post($v1x . '/{db}/{staff}', 'ApiGateController@start');
// Route::post($v1x . '/{db}/{staff}', function ($db, $staff) {
//     return $db . " " . $staff;
// });

// v2
Route::get($v2x, 'ApiGate2Controller@start');
Route::post($v2x, 'ApiGate2Controller@start');
Route::put($v2x, 'ApiGate2Controller@start');
Route::patch($v2x, 'ApiGate2Controller@start');
Route::delete($v2x, 'ApiGate2Controller@start');

//2.1
Route::get($v2_1x . '/sys/{params}', 'ApiGate2_1Controller@start')->where('params', '.*');
Route::post($v2_1x . '/sys/{params}', 'ApiGate2_1Controller@start')->where('params', '.*');
Route::put($v2_1x . '/sys/{params}', 'ApiGate2_1Controller@start')->where('params', '.*');
Route::patch($v2_1x . '/sys/{params}', 'ApiGate2_1Controller@start')->where('params', '.*');
Route::delete($v2_1x . '/sys/{params}', 'ApiGate2_1Controller@start')->where('params', '.*');

Route::get($v2x . '/{db}', 'ApiGate2Controller@start');
Route::post($v2x . '/{db}', 'ApiGate2Controller@start');

Route::get($v2x . '/{db}/{gate}', 'ApiGate2Controller@start');
Route::post($v2x . '/{db}/{gate}', 'ApiGate2Controller@start');
Route::post($v2x . '/{db}/{gate}/{operation}', 'ApiGate2Controller@start');
Route::put($v2x . '/{db}/{gate}/{operation}', 'ApiGate2Controller@start');
Route::patch($v2x . '/{db}/{gate}/{operation}', 'ApiGate2Controller@start');
Route::delete($v2x . '/{db}/{gate}/{operation}', 'ApiGate2Controller@start');

Route::get($v2x . '/{db}/{gate}/{operation}', 'ApiGate2Controller@start');
Route::post($v2x . '/{db}/{gate}/{operation}', 'ApiGate2Controller@start');
Route::post($v2x . '/{db}/{gate}/{operation}/{id}', 'ApiGate2Controller@start');
Route::put($v2x . '/{db}/{gate}/{operation}/{id}', 'ApiGate2Controller@start');
Route::patch($v2x . '/{db}/{gate}/{operation}/{id}', 'ApiGate2Controller@start');
Route::delete($v2x . '/{db}/{gate}/{operation}/{id}', 'ApiGate2Controller@start');

$permission = str_replace('/', '-', str_replace('api/geo/', 'access-', Request::path()));
Route::get('geo/{params}', 'GeoGateController@start')->where('params', '.*')->middleware(['permission:' . $permission]);
Route::post('geo/{params}', 'GeoGateController@start')->where('params', '.*')->middleware(['permission:' . $permission]);
Route::put('geo/{params}', 'GeoGateController@start')->where('params', '.*')->middleware(['permission:' . $permission]);
Route::patch('geo/{params}', 'GeoGateController@start')->where('params', '.*')->middleware(['permission:' . $permission]);
Route::delete('geo/{params}', 'GeoGateController@start')->where('params', '.*')->middleware(['permission:' . $permission]);

Route::post('/ukmsys/register', 'ukmsys\register@run');
